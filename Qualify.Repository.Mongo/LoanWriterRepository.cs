﻿using MongoDB.Driver;
using Qualify.Domain.Models.Identity;
using System;
using System.Threading.Tasks;

namespace Qualify.Repository.Mongo
{
    public class LoanWriterRepository : ILoanWriterRepository
    {
        private readonly MongoUnitOfWork _userUnitOfWork;

        public LoanWriterRepository(MongoUnitOfWork userUnitOfWork)
        {
            _userUnitOfWork = userUnitOfWork;
        }

        public IMongoCollection<LoanWriter> BaseQuery()
        {
            return _userUnitOfWork.LoanWriters;
        }

        public async Task CreateAsync(LoanWriter loanWriter)
        {
            await _userUnitOfWork.LoanWriters.InsertOneAsync(loanWriter);
        }

        public async Task<LoanWriter> GetByUniqueIdAsync(string id)
        {
            return await _userUnitOfWork.LoanWriters.Find(r => r.UniqueID == id).FirstOrDefaultAsync();
        }
        public async Task<LoanWriter> GetFirstUserAsync()
        {

            return await _userUnitOfWork.LoanWriters.Find(LoanWriter => true).FirstOrDefaultAsync();
        }

        public async Task<LoanWriter> GetByEmailAsync(string email)
        {
            return await _userUnitOfWork.LoanWriters.Find(r => r.LoanWriterEmail == email).FirstOrDefaultAsync();
        }

        public async Task<LoanWriter> UpdateByIdAsync(string uniqueId, LoanWriter loanWriter)
        {
            await _userUnitOfWork.LoanWriters.ReplaceOneAsync(r => r.UniqueID == uniqueId, loanWriter);

            return loanWriter;
        }

        public async Task DeleteByIdAsync(string uniqueId)
        {
            await _userUnitOfWork.LoanWriters.DeleteOneAsync(r => r.UniqueID == uniqueId);
        }
        public async Task<LoanWriter> GetUserByOneTimeUseTokenAsync(string oneTimeUseUserToken)
        {
            return await _userUnitOfWork.LoanWriters.Find(r => r.OneTimeUseToken == oneTimeUseUserToken).FirstOrDefaultAsync();
        }
    }
}
