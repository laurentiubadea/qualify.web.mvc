﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Qualify.Domain;
using Qualify.Domain.Models;
using Qualify.Domain.Models.Identity;
using System;
using System.Linq;

namespace Qualify.Repository.Mongo
{
    public class MongoUnitOfWork
    {
        public readonly IMongoCollection<Record> Records;
        public readonly IMongoCollection<LoanWriter> LoanWriters;
        private readonly ConnectionStrings _connectionStrings;

        public MongoUnitOfWork(IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionStrings = connectionStrings.Value;

            var types = AppDomain.CurrentDomain.GetAssemblies()
                            .SelectMany(s => s.GetTypes())
                            .Where(typeof(IMongoPropertiesInterface).IsAssignableFrom).ToList();
            types.Remove(typeof(IMongoPropertiesInterface));

            foreach (Type type in types)
            {
                var classMapDefinition = typeof(BsonClassMap<>);
                var classMapType = classMapDefinition.MakeGenericType(type);
                var classMap = (BsonClassMap)Activator.CreateInstance(classMapType);

                // Do custom initialization here, e.g. classMap.SetDiscriminator, AutoMap etc
                classMap.AutoMap();
                classMap.SetIgnoreExtraElements(true);

                if (type.GetProperties().ToList().Any(p => p.Name == "Id"))
                {
                    classMap.MapProperty("Id")
                    .SetIdGenerator(StringObjectIdGenerator.Instance)
                    .SetSerializer(new StringSerializer(BsonType.ObjectId));
                }

                BsonClassMap.RegisterClassMap(classMap);
            }

            MongoClient client = new MongoClient(_connectionStrings.ANZDB);
            IMongoDatabase database = client.GetDatabase("anzqualify");

            Records = database.GetCollection<Record>("records");
            LoanWriters = database.GetCollection<LoanWriter>("users");
        }
    }
}
