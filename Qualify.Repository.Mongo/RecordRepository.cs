﻿using MongoDB.Driver;
using Qualify.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qualify.Repository.Mongo
{
    public class RecordRepository : IRecordRepository
    {
        private readonly MongoUnitOfWork _unitOfWork;

        public RecordRepository(MongoUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMongoCollection<Record> BaseQuery()
        {
            return _unitOfWork.Records;
        }

        public async Task<IEnumerable<Record>> GetAllAsync()
        {
            return await _unitOfWork.Records.Find(Record => true).ToListAsync();
        }

        public async Task<Record> GetByIdAsync(string id)
        {
            return await _unitOfWork.Records.Find(r => r.Id == id).SingleOrDefaultAsync();
        }

        public async Task<Record> GetByUniqueIdAsync(string uniqueId)
        {
            return await _unitOfWork.Records.Find(r => r.UniqueID == uniqueId).SingleOrDefaultAsync();
        }

        //public async Task<IEnumerable<Record>> GetByUserIdAsync(string userId)
        //{
        //    return await _unitOfWork.Records.Find(r => r.User.LoanWriterAccreditationNumber == userId).ToListAsync();
        //}

        public async Task<Record> UpdateByIdAsync(string id, Record record)
        {
            await _unitOfWork.Records.ReplaceOneAsync(r => r.Id == id, record);

            return await GetByIdAsync(record.Id);
        }

        public async Task DeleteByIdAsync(string id)
        {
            await _unitOfWork.Records.DeleteOneAsync(r => r.Id == id);
        }

        public async Task DeleteManyAsync(IEnumerable<string> ids)
        {
            FilterDefinition<Record> filter = Builders<Record>.Filter.In(r => r.Id, ids);

            await _unitOfWork.Records.DeleteManyAsync(filter);
        }

        IMongoCollection<Record> IRecordRepository.BaseQuery()
        {
            throw new NotImplementedException();
        }

        //public async Task CreateAsync(Record record) {
        //    await _unitOfWork.Records.InsertOneAsync(record);
        //}

        public async Task CreateAsync(Record record)
        {
            await _unitOfWork.Records
                    .WithWriteConcern(WriteConcern.Acknowledged)
                    .InsertOneAsync(
                        record,
                        new InsertOneOptions
                        {
                            BypassDocumentValidation = true
                        });
        }
    }
}
