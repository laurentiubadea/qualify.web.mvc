﻿/// <binding AfterBuild='build' Clean='clean' />
var gulp = require('gulp');
var gulpClean = require('gulp-clean');
var source = require('vinyl-source-stream');

gulp.task('clean', function () {
    return gulp.src('wwwroot/vendor', { read: false })
        .pipe(gulpClean());
});

gulp.task('build', gulp.series(
    jquery, requirejs, jsonForms, slideModal, expressionEngine, kendoGrid, bootstrap, material, knockout, knockoutMapping, formatNumber, robotoFontface, addREADME
));

function copyFilesToDestination(filesUrls, destinationUrl) {
    return gulp.src(filesUrls).pipe(gulp.dest(destinationUrl));
}

function jquery() {
    return copyFilesToDestination(
        [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/jquery/dist/jquery.min.js'
        ],
        'wwwroot/vendor/jquery'
    );
}

function requirejs() {
    return copyFilesToDestination(
        [
            'node_modules/requirejs/require.js',
        ],
        'wwwroot/vendor/requirejs'
    );
}

function jsonForms() {
    return copyFilesToDestination(
        [
            'node_modules/jsonformsst7avs/**/*.{css,js}'
        ],
        'wwwroot/vendor/JsonForms'
    );
}

function slideModal() {
    return copyFilesToDestination(
        [
            'node_modules/slidemodalst7avs/**/*.{css,js}'
        ],
        'wwwroot/vendor/SlideModal'
    );
}

function expressionEngine() {
    return copyFilesToDestination(
        [
            'node_modules/expressionenginest7avs/**/*.{css,js}'
        ],
        'wwwroot/vendor/ExpressionEngine'
    );
}

function kendoGrid() {
    return copyFilesToDestination(
        [
            'node_modules/kendost7avs/**'
        ],
        'wwwroot/vendor/KendoUI'
    );
}

function bootstrap() {
    return copyFilesToDestination(
        [
            'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map',
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/bootstrap/dist/css/bootstrap.min.css.map'
        ],
        'wwwroot/vendor/bootstrap'
    );
}

function material() {
    return copyFilesToDestination(
        [
            'node_modules/material-components-web/dist/material-components-web.min.js',
            'node_modules/material-components-web/dist/material-components-web.min.css',
            'node_modules/material-components-web/dist/material-components-web.min.css.map'
        ],
        'wwwroot/vendor/material-components-web'
    );
}

function knockout() {
    return copyFilesToDestination(
        [
            'node_modules/knockout/build/output/knockout-latest.js',
        ],
        'wwwroot/vendor/knockout'
    );
}

function knockoutMapping() {
    return copyFilesToDestination(
        [
            'node_modules/knockout-mapping/dist/knockout.mapping.min.js',
        ],
        'wwwroot/vendor/knockout-mapping'
    );
}

function formatNumber() {
    return copyFilesToDestination(
        [
            'node_modules/jquery-number/jquery.number.js',
        ],
        'wwwroot/vendor/jquery-number'
    );
}

function robotoFontface() {
    return copyFilesToDestination(
        [
            'node_modules/roboto-fontface/**/*.{css,woff,woff2}',
        ],
        'wwwroot/vendor/roboto-fontface'
    );
}

function addREADME() {
    var stream = source('README.md');

    stream.end('This vendor folder include the dependencies that the application needs in order to run. Here should be only the files you need which are taken from ex: "node modules" using the GulpFile.js in the root');
    return stream.pipe(gulp.dest('wwwroot/vendor'));
}