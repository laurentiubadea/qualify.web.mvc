﻿using Amazon;

namespace Qualify.Web.Mvc.Helpers.AWS
{
    public class AwsConfiguration
    {
        public RegionEndpoint Region { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
    }
}
