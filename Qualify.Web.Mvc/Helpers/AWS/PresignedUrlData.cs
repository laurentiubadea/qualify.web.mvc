﻿namespace Qualify.Web.Mvc.Helpers.AWS
{
    public class PresignedUrlData
    {
        public string KeyName { get; set; }
    }
}
