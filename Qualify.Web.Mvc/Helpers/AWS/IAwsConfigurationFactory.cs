﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Helpers.AWS
{
    public interface IAwsConfigurationFactory
    {
        AwsConfiguration Get();
    }
}
