﻿using Amazon;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Helpers.AWS
{
    public class AwsConfigurationFactory : IAwsConfigurationFactory
    {
        public readonly IConfiguration _configuration;
        public AwsConfigurationFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public AwsConfiguration Get()
        {
            return new AwsConfiguration
            {
                AccessKey = AccessKey,
                Region = Region,
                SecretKey = SecretKey
            };
        }
        private RegionEndpoint Region
        {
            get
            {
                if (_configuration["AWS:Region"] == null)
                    throw new KeyNotFoundException("AWS:Region not found");

                return RegionEndpoint.GetBySystemName(_configuration["AWS:Region"]);
            }
        }
        private string AccessKey
        {
            get
            {
                if (_configuration["AWS:AccessKey"] == null)
                    throw new KeyNotFoundException("AWS:AccessKey not found");

                return _configuration["AWS:AccessKey"];
            }
        }
        private string SecretKey
        {
            get
            {
                if (_configuration["AWS:SecretKey"] == null)
                    throw new KeyNotFoundException("AWS:SecretKey not found");

                return _configuration["AWS:SecretKey"];
            }
        }
    }
}
