﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Helpers.PartialViewsLoader
{
    public class PartialViewsLoader : IPartialViewsLoader
    {
        private static ICompositeViewEngine _viewEngine;
        public PartialViewsLoader(ICompositeViewEngine viewEngine)
        {
            _viewEngine = viewEngine;
        }

        public async Task<string> RenderRazorViewToString(Controller controller, string partialView, object model, object additionalInfo = null)
        {
            controller.ViewData.Model = model;
            controller.ViewData["AdditionalInfo"] = additionalInfo;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = _viewEngine.FindView(controller.ControllerContext, partialView, false);
                ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw, new HtmlHelperOptions());

                await viewResult.View.RenderAsync(viewContext);

                return sw.GetStringBuilder().ToString();
            }
        }

        public async Task<string> GetViewPageHtml(Controller controller, string viewName, object model)
        {
            ViewEngineResult result = _viewEngine.FindView(controller.ControllerContext, viewName, false);

            if (result.View == null)
                throw new Exception(string.Format("View Page {0} was not found", viewName));

            controller.ViewData.Model = model;
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                ViewContext viewContext = new ViewContext(controller.ControllerContext, result.View, controller.ViewData, controller.TempData, sw, new HtmlHelperOptions());
                await result.View.RenderAsync(viewContext);
            }

            return sb.ToString();
        }
    }
}
