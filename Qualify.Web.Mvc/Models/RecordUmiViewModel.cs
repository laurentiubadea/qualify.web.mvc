﻿using System.Collections.Generic;

namespace Qualify.Web.Mvc.Models
{
    public class RecordUmiViewModel
    {
        public string DataJson { get; set; }
        public bool IsUmiCalculationFinished { get; set; }
        public bool IsUmiSubmittedAtLeastOnce { get; set; }
        public string CDPResultJson { get; set; }
        public IEnumerable<Domain.Models.Temporary.Indicator> UmiIndicators { get; internal set; }
    }
}
