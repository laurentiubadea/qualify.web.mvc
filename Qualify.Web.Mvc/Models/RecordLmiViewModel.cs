﻿using System.Collections.Generic;

namespace Qualify.Web.Mvc.Models
{
    public class RecordLmiViewModel
    {
        public string DataJson { get; set; }
        public bool IsCalculationFinished { get; set; }
        public bool IsSubmittedAtLeastOnce { get; set; }
        public string ResultJson { get; set; }
        public IEnumerable<Domain.Models.Temporary.Indicator> Indicators { get; internal set; }
    }
}
