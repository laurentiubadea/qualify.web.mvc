﻿using System.ComponentModel.DataAnnotations;

namespace Qualify.Web.Mvc.Models.Profile
{
    public class ExternalLoanWriterModel
    {
        [Required]
        public string UniqueID { get; set; } = "";
        [Required]
        public string FirstName { get; set; } = "";
        [Required]
        public string Surname { get; set; } = "";
        [Required]
        public string Email { get; set; } = "";
        public string Role { get; set; } = "User";
        [Required]
        public string AO { get; set; } = "";
        [Required]
        public string SAO { get; set; } = "";
        [Required]
        public string Mobile { get; set; } = "";
        public string Address { get; set; } = "";
        public string Suburb { get; set; } = "";
        public string State { get; set; } = "";
        public string Postcode { get; set; } = "";
        public string Country { get; set; } = "";
    }
}
