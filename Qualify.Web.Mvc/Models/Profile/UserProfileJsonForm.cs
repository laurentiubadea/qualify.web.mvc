﻿namespace Qualify.Web.Mvc.Models.Profile
{
    public class UserProfileJsonForm
    {
        public ExternalLoanWriterModel ExternalLoanWriterModel { get; set; }
        public string[] LockedFields { get; set; }
    }
}
