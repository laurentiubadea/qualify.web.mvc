﻿using System.Collections.Generic;

namespace Qualify.Web.Mvc.Models
{
    public class RecordPricingViewModel
    {
        public string DataJson { get; set; }
        public bool IsCalculationFinished { get; set; }
        public bool IsSubmittedAtLeastOnce { get; set; }
        public string QualifyResult { get; set; }
        public IEnumerable<Domain.Models.Temporary.Indicator> Indicators { get; internal set; }
    }
}
