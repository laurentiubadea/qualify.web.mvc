﻿using Newtonsoft.Json;

namespace Qualify.Web.Mvc.Models.IntercomChat
{
    public class IntercomUser
    {
        [JsonProperty("app_id")]
        public string AppId
        {
            get
            {
                return "hhl8by6g";
            }
        }
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("name")]
        public string FullName { get; set; }
        public string UniqueID { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        public string Role { get; set; }
        public string AO { get; set; }
        public string SAO { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string ReturnURL { get; set; }

    }
}
