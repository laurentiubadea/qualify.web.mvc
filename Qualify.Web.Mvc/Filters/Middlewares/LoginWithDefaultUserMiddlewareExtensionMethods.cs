﻿using Microsoft.AspNetCore.Builder;

namespace Qualify.Web.Mvc.Filters.Middlewares
{
    public static class LoginWithDefaultUserMiddlewareExtensionMethods
    {
        public static IApplicationBuilder UseDefaultUser(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.UseMiddleware<LoginWithDefaultUserMiddleware>();
        }
    }
}
