﻿using Microsoft.AspNetCore.Http;
using Qualify.Domain.Models.Identity;
using Qualify.Repository;
using Qualify.Web.Mvc.Services;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Filters.Middlewares
{
    public class LoginWithDefaultUserMiddleware
    {
        private readonly RequestDelegate _next;
        public LoginWithDefaultUserMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ILoanWriterRepository loanWriterRepository)
        {
            if (context.User.Identity.IsAuthenticated == false)
            {
                string defaultUserUniqueId = AppSettings.DefaultLoginUserUniqueID;

                LoanWriter loanWriter = await loanWriterRepository.GetByUniqueIdAsync(defaultUserUniqueId);
                await SignInService.SignInAsync(context, loanWriter);
                context.Response.Redirect(context.Request.Path + context.Request.QueryString);
                return;
            }
            await _next(context);
        }
    }
}
