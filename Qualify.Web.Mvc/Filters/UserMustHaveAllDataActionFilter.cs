﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Qualify.Domain.Models.Identity;
using Qualify.Repository;
using Qualify.Web.Mvc.ExtensionMethods;
using Qualify.Web.Mvc.Models.Profile;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace Qualify.Web.Mvc.Filters
{
    public class UserMustHaveAllDataActionFilter : ActionFilterAttribute
    {
        public bool Disable { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (Disable)
                return;

            object loanWriterRepository = context.HttpContext.RequestServices.GetService<ILoanWriterRepository>();

            LoanWriter loggedInLoanwriter = context.HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);

            ExternalLoanWriterModel externalLoanWriterModel = loggedInLoanwriter.ToExternalLoanWriterModel();

            ValidationContext validateContext = new ValidationContext(externalLoanWriterModel, serviceProvider: null, items: null);
            List<ValidationResult> results = new List<ValidationResult>();
            bool isExternalProfileFull = Validator.TryValidateObject(externalLoanWriterModel, validateContext, results);

            if (isExternalProfileFull == false)
            {
                //redirect to a page to fill all the user data
                context.Result = new RedirectToActionResult("Update", "UserProfile", new { });
            }
        }
    }
}
