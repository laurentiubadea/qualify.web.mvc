﻿using Amazon.Lambda;
using Amazon.S3;
using Elmah.Io.AspNetCore;
using Jdenticon.AspNetCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Qualify.Domain;
using Qualify.Repository;
using Qualify.Repository.Mongo;
using Qualify.Web.Mvc.Business_Service;
using Qualify.Web.Mvc.Configuration;
using Qualify.Web.Mvc.Filters;
using Qualify.Web.Mvc.Filters.Middlewares;
using Qualify.Web.Mvc.Helpers.AWS;
using Qualify.Web.Mvc.Helpers.PartialViewsLoader;
using Qualify.Web.Mvc.Services.Jwt;
using System;
using ToolKitGrid.JWTApi.Helpers;

namespace Qualify.Web.Mvc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AppSettings.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
            services.Configure<ConnectionStrings>(Configuration.GetSection("ConnectionStrings"));

            services.AddScoped(p => new UriApiMenthodsConfiguration { RecordQualifyUrl = this.Configuration.GetValue<string>("RecordQualifyUrl") });
            services.AddSingleton<MongoUnitOfWork>();
            services.AddScoped<IRecordRepository, RecordRepository>();
            services.AddScoped<ILoanWriterRepository, LoanWriterRepository>();

            services.AddScoped<IAddToRecordService, AddToRecordService>();
            services.AddScoped<IRemoveFromRecordService, RemoveFromRecordService>();
            services.AddScoped<IRecordUpdateService, RecordUpdateService>();
            services.AddScoped<IAwsConfigurationFactory, AwsConfigurationFactory>();
            services.AddScoped<IPartialViewsLoader, PartialViewsLoader>();
            services.AddScoped<UserMustHaveAllDataActionFilter>();
            services.AddScoped<IJwtService, JwtService>();
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services
                .AddDefaultAWSOptions(Configuration.GetAWSOptions())
                .AddAWSService<IAmazonLambda>()
                .AddAWSService<IAmazonS3>();


            services.Configure<ElmahIoOptions>(Configuration.GetSection("ElmahIo"));
            services.AddElmahIo();
            services.AddCors();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //.AddJsonOptions(options =>
            //                    options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.Cookie.Name = "User.Cookie";
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
                options.LoginPath = "/account/login";
                options.LogoutPath = "/account/logout";
            });
            services.AddHttpClient();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                string awsAccessKey = this.Configuration[Constants.AwsAccessKey];
                string awsSecretKey = this.Configuration[Constants.AwsSecretKey];
                Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", awsAccessKey);
                Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", awsSecretKey);
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseStatusCodePages();
                app.UseHsts();
            }

            //elmah
            app.UseSession();
            app.UseElmahIo();
            app.UseHttpsRedirection();
            app.UseJdenticon();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            if (AppSettings.SecurityEnabled == false)
            {
                app.UseDefaultUser();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}");
            });


        }
    }
}
