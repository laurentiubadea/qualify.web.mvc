﻿using Newtonsoft.Json;
using System.Linq;
using System.Security.Claims;

namespace Qualify.Web.Mvc.ExtensionMethods
{
    public static class ClaimsPrincipalExtensionMethods
    {
        public static T GetClaim<T>(this ClaimsPrincipal claimsPrincipal, string type)
        {
            string stringValue = claimsPrincipal.GetClaimValue(type);
            if (string.IsNullOrEmpty(stringValue))
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(claimsPrincipal.GetClaimValue(type));
        }

        public static string GetClaimValue(this ClaimsPrincipal claimsPrincipal, string type)
        {
            return ((ClaimsIdentity)claimsPrincipal.Identity).Claims.FirstOrDefault(p => p.Type == type)?.Value;
        }

        public static Claim GetClaim(this ClaimsPrincipal claimsPrincipal, string type)
        {
            return ((ClaimsIdentity)claimsPrincipal.Identity).Claims.FirstOrDefault(p => p.Type == type);
        }

        public static void RemoveClaim(this ClaimsPrincipal claimsPrincipal, string type)
        {
            ((ClaimsIdentity)claimsPrincipal.Identity).RemoveClaim(claimsPrincipal.GetClaim(ClaimTypes.UserData));
        }

        public static void AddClaim<T>(this ClaimsPrincipal claimsPrincipal, string type, T claimValue)
        {
            ((ClaimsIdentity)claimsPrincipal.Identity).AddClaim<T>(type, claimValue);
        }
    }
}
