﻿using Qualify.Domain.Models.Identity;
using Qualify.Web.Mvc.Models.IntercomChat;
using Qualify.Web.Mvc.Models.Profile;

namespace Qualify.Web.Mvc.ExtensionMethods
{
    public static class LoanWriterExtensionMethods
    {
        public static ExternalLoanWriterModel ToExternalLoanWriterModel(this LoanWriter loanWriter)
        {
            ExternalLoanWriterModel externalLoanWriterModel = new ExternalLoanWriterModel
            {
                UniqueID = loanWriter.UniqueID,
                FirstName = loanWriter.LoanWriterFirstName,
                Surname = loanWriter.LoanWriterSurname,
                Email = loanWriter.LoanWriterEmail,
                Role = loanWriter.Role,
                AO = loanWriter.AO,
                SAO = loanWriter.SAO,
                Mobile = loanWriter.LoanWriterMobile?.Number,
                Country = loanWriter.LoanWriterAddress?.Country,
                Postcode = loanWriter.LoanWriterAddress?.AustralianPostCode,
                State = loanWriter.LoanWriterAddress?.AustralianState,
                Suburb = loanWriter.LoanWriterAddress?.Suburb
            };

            return externalLoanWriterModel;
        }

        public static IntercomUser ToIntercomUser(this LoanWriter loanWriter)
        {
            IntercomUser intercomUser = new IntercomUser
            {
                UserId = loanWriter.UniqueID,
                FullName = loanWriter.FullName,
                UniqueID = loanWriter.UniqueID,
                FirstName = loanWriter.LoanWriterFirstName,
                Surname = loanWriter.LoanWriterSurname,
                Email = loanWriter.LoanWriterEmail,
                Role = loanWriter.Role,
                AO = loanWriter.AO,
                SAO = loanWriter.SAO,
                Mobile = loanWriter.LoanWriterMobile?.Number,
                Suburb = loanWriter.LoanWriterAddress?.Suburb,
                State = loanWriter.LoanWriterAddress?.AustralianState,
                Postcode = loanWriter.LoanWriterAddress?.AustralianPostCode,
                Country = loanWriter.LoanWriterAddress?.Country,
                ReturnURL = loanWriter.ReturnUrl
            };

            return intercomUser;
        }
    }
}
