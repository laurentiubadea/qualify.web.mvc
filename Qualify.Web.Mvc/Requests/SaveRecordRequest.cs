﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Requests
{
    public class SaveRecordRequest
    {
        public string RecordDataJson { get; set; }
    }
}
