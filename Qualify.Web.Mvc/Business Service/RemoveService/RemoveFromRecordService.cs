﻿using Qualify.Repository;

namespace Qualify.Web.Mvc.Business_Service
{
    public class RemoveFromRecordService : IRemoveFromRecordService
    {
        private IRecordRepository _recordRepository;

        public RemoveFromRecordService(IRecordRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }

        //public async Task<Record> RemoveHouseholdAsync(string recordId, Household toRemoveHousehold)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Household household = record.Household.FirstOrDefault(h => h.Id == toRemoveHousehold.Id);

        //    //nu e neaparat nevoie sa cauti household si sa verifici dar am pus asta aici just in case.
        //    if (household != null)
        //    {
        //        List<Household> households = record.Household.ToList();
        //        households.Remove(households.First(h => h.Id == toRemoveHousehold.Id));

        //        record.Household = households.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveApplicantAsync(string recordId, Person toRemoveApplicant)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Person applicant = record.Person.FirstOrDefault(a => a.Id == toRemoveApplicant.Id);

        //    if (applicant != null)
        //    {
        //        List<Person> persons = record.Person.ToList();
        //        persons.Remove(persons.First(p => p.Id == toRemoveApplicant.Id));

        //        record.Person = persons.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemovePAYGAsync(string recordId, PAYG toRemovePAYG)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    PAYG payg = record.Income.First().PAYG.FirstOrDefault(p => p.Id == toRemovePAYG.Id);

        //    if (payg != null)
        //    {
        //        List<PAYG> paygs = record.Income.First().PAYG.ToList();
        //        paygs.Remove(paygs.First(p => p.Id == toRemovePAYG.Id));

        //        record.Income.First().PAYG = paygs.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveSelfEmployedAsync(string recordId, SelfEmployed selfEmployedToRemove)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    SelfEmployed selfEmployed = record.Income.First().SelfEmployed.FirstOrDefault(p => p.Id == selfEmployedToRemove.Id);

        //    if (selfEmployed != null)
        //    {
        //        List<SelfEmployed> selfEmployeds = record.Income.First().SelfEmployed.ToList();
        //        selfEmployeds.Remove(selfEmployeds.First(p => p.Id == selfEmployedToRemove.Id));

        //        record.Income.First().SelfEmployed = selfEmployeds.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveOtherIncomeAsync(string recordId, OtherIncome otherIncomeToRemove)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    OtherIncome otherIncome = record.Income.First().OtherIncome.FirstOrDefault(p => p.Id == otherIncomeToRemove.Id);

        //    if (otherIncome != null)
        //    {
        //        List<OtherIncome> otherIncomes = record.Income.First().OtherIncome.ToList();
        //        otherIncomes.Remove(otherIncomes.First(p => p.Id == otherIncomeToRemove.Id));

        //        record.Income.First().OtherIncome = otherIncomes.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveNotEmployedAsync(string recordId, NotEmployed notEmployedToRemove)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    NotEmployed notEmployed = record.Income.First().NotEmployed.FirstOrDefault(p => p.Id == notEmployedToRemove.Id);

        //    if (notEmployed != null)
        //    {
        //        List<NotEmployed> notEmployeds = record.Income.First().NotEmployed.ToList();
        //        notEmployeds.Remove(notEmployeds.First(p => p.Id == notEmployedToRemove.Id));

        //        record.Income.First().NotEmployed = notEmployeds.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveRentalIncomedAsync(string recordId, RentalIncome rentalIncomeToRemove)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    RentalIncome rentalIncome = record.Income.First().RentalIncome.FirstOrDefault(p => p.Id == rentalIncomeToRemove.Id);

        //    if (rentalIncome != null)
        //    {
        //        List<RentalIncome> rentalIncomes = record.Income.First().RentalIncome.ToList();
        //        rentalIncomes.Remove(rentalIncomes.First(p => p.Id == rentalIncomeToRemove.Id));

        //        record.Income.First().RentalIncome = rentalIncomes.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveLiabilityAsync(string recordId, Liability toRemoveLiability)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Liability liability = record.Liability.FirstOrDefault(h => h.Id == toRemoveLiability.Id);

        //    if (liability != null)
        //    {
        //        List<Liability> liabilities = record.Liability.ToList();
        //        liabilities.Remove(liabilities.First(h => h.Id == toRemoveLiability.Id));

        //        record.Liability = liabilities.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveExpenseAsync(string recordId, Expense toRemoveExpense)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Expense expense = record.Expenses.FirstOrDefault(h => h.Id == toRemoveExpense.Id);

        //    if (expense != null)
        //    {
        //        List<Expense> expenses = record.Expenses.ToList();
        //        expenses.Remove(expenses.First(h => h.Id == toRemoveExpense.Id));

        //        record.Expenses = expenses.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> RemoveNewLoanAsync(string recordId, NewLoan toRemoveNewLoan)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    NewLoan newLoan = record.NewLoan.FirstOrDefault(h => h.Id == toRemoveNewLoan.Id);

        //    if (newLoan != null)
        //    {
        //        List<NewLoan> newLoans = record.NewLoan.ToList();
        //        newLoans.Remove(newLoans.First(h => h.Id == toRemoveNewLoan.Id));

        //        record.NewLoan = newLoans.AsEnumerable();
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}
    }
}
