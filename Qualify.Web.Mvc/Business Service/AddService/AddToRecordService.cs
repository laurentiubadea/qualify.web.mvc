﻿using Qualify.Repository;

namespace Qualify.Web.Mvc.Business_Service
{
    public class AddToRecordService : IAddToRecordService
    {
        private readonly IRecordRepository _recordRepository;

        public AddToRecordService(IRecordRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }

        //public async Task<Record> AddHouseholdAsync(string recordId, Household household)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (household != null)
        //    {
        //        if (record.Household != null)
        //        {
        //            List<Household> households = record.Household.ToList();
        //            households.Add(household);
        //            record.Household = households.AsEnumerable();
        //        }
        //        else
        //        {
        //            List<Household> households = new List<Household>();
        //            households.Add(household);
        //            record.Household = households.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddApplicantAsync(string recordId, Person applicant)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (applicant != null)
        //    {
        //        if (record.Person != null)
        //        {
        //            List<Person> applicants = record.Person.ToList();
        //            applicants.Add(applicant);
        //            record.Person = applicants.AsEnumerable();
        //        }
        //        else
        //        {
        //            List<Person> applicants = new List<Person>();
        //            applicants.Add(applicant);
        //            record.Person = applicants.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddPAYGIncomeAsync(string recordId, PAYG payg)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (record != null)
        //    {
        //        if (record.Income != null)
        //        {
        //            if (record.Income.First().PAYG != null)
        //            {
        //                List<PAYG> paygs = record.Income.First().PAYG.ToList();
        //                paygs.Add(payg);
        //                record.Income.First().PAYG = paygs.AsEnumerable();
        //            }
        //            else
        //            {
        //                List<PAYG> paygs = new List<PAYG>();
        //                paygs.Add(payg);
        //                record.Income.First().PAYG = paygs.AsEnumerable();
        //            }
        //        }
        //        else
        //        {
        //            List<Income> incomes = new List<Income>();
        //            incomes.Add(new Income());
        //            record.Income = incomes;
        //            record.Income.AsEnumerable();

        //            List<PAYG> paygs = new List<PAYG>();
        //            paygs.Add(payg);
        //            record.Income.First().PAYG = paygs.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddSelEmployedIncomeAsync(string recordId, SelfEmployed selfEmployed)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (record != null)
        //    {
        //        if (record.Income != null)
        //        {
        //            if (record.Income.First().SelfEmployed != null)
        //            {
        //                List<SelfEmployed> selfEmployeds = record.Income.First().SelfEmployed.ToList();
        //                selfEmployeds.Add(selfEmployed);
        //                record.Income.First().SelfEmployed = selfEmployeds.AsEnumerable();
        //            }
        //            else
        //            {
        //                List<SelfEmployed> selfEmployeds = new List<SelfEmployed>();
        //                selfEmployeds.Add(selfEmployed);
        //                record.Income.First().SelfEmployed = selfEmployeds.AsEnumerable();
        //            }
        //        }
        //        else
        //        {
        //            List<Income> incomes = new List<Income>();
        //            incomes.Add(new Income());
        //            record.Income = incomes;
        //            record.Income.AsEnumerable();

        //            List<SelfEmployed> selfEmployds = new List<SelfEmployed>();
        //            selfEmployds.Add(selfEmployed);
        //            record.Income.First().SelfEmployed = selfEmployds.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddNotEmployedIncomeAsync(string recordId, NotEmployed notEmployed)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (record != null)
        //    {
        //        if (record.Income != null)
        //        {
        //            if (record.Income.First().NotEmployed != null)
        //            {
        //                List<NotEmployed> notEmployeds = record.Income.First().NotEmployed.ToList();
        //                notEmployeds.Add(notEmployed);
        //                record.Income.First().NotEmployed = notEmployeds.AsEnumerable();
        //            }
        //            else
        //            {
        //                List<NotEmployed> notEmployeds = new List<NotEmployed>();
        //                notEmployeds.Add(notEmployed);
        //                record.Income.First().NotEmployed = notEmployeds.AsEnumerable();
        //            }
        //        }
        //        else
        //        {
        //            List<Income> incomes = new List<Income>();
        //            incomes.Add(new Income());
        //            record.Income = incomes;
        //            record.Income.AsEnumerable();

        //            List<NotEmployed> notEmployeds = new List<NotEmployed>();
        //            notEmployeds.Add(notEmployed);
        //            record.Income.First().NotEmployed = notEmployeds.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddOtherIncomeAsync(string recordId, OtherIncome otherIncome)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (record != null)
        //    {
        //        if (record.Income != null)
        //        {
        //            if (record.Income.First().OtherIncome != null)
        //            {
        //                List<OtherIncome> otherIncomes = record.Income.First().OtherIncome.ToList();
        //                otherIncomes.Add(otherIncome);
        //                record.Income.First().OtherIncome = otherIncomes.AsEnumerable();
        //            }
        //            else
        //            {
        //                List<OtherIncome> otherIncomes = new List<OtherIncome>();
        //                otherIncomes.Add(otherIncome);
        //                record.Income.First().OtherIncome = otherIncomes.AsEnumerable();
        //            }
        //        }
        //        else
        //        {
        //            List<Income> incomes = new List<Income>();
        //            incomes.Add(new Income());
        //            record.Income = incomes;
        //            record.Income.AsEnumerable();

        //            List<OtherIncome> otherIncomes = new List<OtherIncome>();
        //            otherIncomes.Add(otherIncome);
        //            record.Income.First().OtherIncome = otherIncomes.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddRentalIncomeAsync(string recordId, RentalIncome rentalIncome)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (record != null)
        //    {
        //        if (record.Income != null)
        //        {
        //            if (record.Income.First().RentalIncome != null)
        //            {
        //                List<RentalIncome> rentalIncomes = record.Income.First().RentalIncome.ToList();
        //                rentalIncomes.Add(rentalIncome);
        //                record.Income.First().RentalIncome = rentalIncomes.AsEnumerable();
        //            }
        //            else
        //            {
        //                List<RentalIncome> rentalIncomes = new List<RentalIncome>();
        //                rentalIncomes.Add(rentalIncome);
        //                record.Income.First().RentalIncome = rentalIncomes.AsEnumerable();
        //            }
        //        }
        //        else
        //        {
        //            List<Income> incomes = new List<Income>();
        //            incomes.Add(new Income());
        //            record.Income = incomes;
        //            record.Income.AsEnumerable();

        //            List<RentalIncome> rentalIncomes = new List<RentalIncome>();
        //            rentalIncomes.Add(rentalIncome);
        //            record.Income.First().RentalIncome = rentalIncomes.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddLiabilityAsync(string recordId, Liability liability)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (liability != null)
        //    {
        //        if (record.Liability != null)
        //        {
        //            List<Liability> liabilities = record.Liability.ToList();
        //            liabilities.Add(liability);
        //            record.Liability = liabilities.AsEnumerable();
        //        }
        //        else
        //        {
        //            List<Liability> liabilities = new List<Liability>();
        //            liabilities.Add(liability);
        //            record.Liability = liabilities.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddExpenseAsync(string recordId, Expense expense)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (expense != null)
        //    {
        //        if (record.Expenses != null)
        //        {
        //            List<Expense> expenses = record.Expenses.ToList();
        //            expenses.Add(expense);
        //            record.Expenses = expenses.AsEnumerable();
        //        }
        //        else
        //        {
        //            List<Expense> expenses = new List<Expense>();
        //            expenses.Add(expense);
        //            record.Expenses = expenses.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> AddNewLoanAsync(string recordId, NewLoan newLoan)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    if (newLoan != null)
        //    {
        //        if (record.NewLoan != null)
        //        {
        //            List<NewLoan> newLoans = record.NewLoan.ToList();
        //            newLoans.Add(newLoan);
        //            record.NewLoan = newLoans.AsEnumerable();
        //        }
        //        else
        //        {
        //            List<NewLoan> newLoans = new List<NewLoan>();
        //            newLoans.Add(newLoan);
        //            record.NewLoan = newLoans.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}
    }
}