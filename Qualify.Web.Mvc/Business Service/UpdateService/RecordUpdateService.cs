﻿using Qualify.Domain.HelperModels.IncomeTypes;
using Qualify.Domain.Models;
using Qualify.Domain.Models.Embedded;
using Qualify.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Business_Service
{
    public class RecordUpdateService : IRecordUpdateService
    {
        private IRecordRepository _recordRepository;

        public RecordUpdateService(IRecordRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }

        //public async Task<Record> UpdateHouseholdAsync(string recordId, Household toUpdateHousehold)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Household household = record.Household.FirstOrDefault(h => h.Id == toUpdateHousehold.Id);

        //    //nu e neaparat nevoie sa cauti household si sa verifici dar am pus asta aici just in case.
        //    if (household != null)
        //    {
        //        int index = record.Household.ToList().IndexOf(household);

        //        if (index != -1)
        //        {
        //            List<Household> houses = record.Household.ToList();
        //            houses[index] = toUpdateHousehold;
        //            record.Household = houses.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateApplicantAsync(string recordId, Person toUpdateApplicant)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Person applicant = record.Person.FirstOrDefault(a => a.Id == toUpdateApplicant.Id);

        //    if (applicant != null)
        //    {
        //        int index = record.Person.ToList().IndexOf(applicant);

        //        if (index != -1)
        //        {
        //            List<Person> personApplicants = record.Person.ToList();
        //            personApplicants[index] = toUpdateApplicant;
        //            record.Person = personApplicants.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdatePAYGAsync(string recordId, PAYG toUpdatePAYG)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    PAYG payg = record.Income.First().PAYG.FirstOrDefault(p => p.Id == toUpdatePAYG.Id);

        //    if (payg != null)
        //    {
        //        int index = record.Income.First().PAYG.ToList().IndexOf(payg);

        //        if (index != -1)
        //        {
        //            List<PAYG> paygs = record.Income.First().PAYG.ToList();
        //            paygs[index] = toUpdatePAYG;
        //            record.Income.First().PAYG = paygs.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateSelfEmployedAsync(string recordId, SelfEmployed selfEmployedToUpdate)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    SelfEmployed selfEmployed = record.Income.First().SelfEmployed.FirstOrDefault(p => p.Id == selfEmployedToUpdate.Id);

        //    if (selfEmployed != null)
        //    {
        //        int index = record.Income.First().SelfEmployed.ToList().IndexOf(selfEmployed);

        //        if (index != -1)
        //        {
        //            List<SelfEmployed> selfEmployeds = record.Income.First().SelfEmployed.ToList();
        //            selfEmployeds[index] = selfEmployedToUpdate;
        //            record.Income.First().SelfEmployed = selfEmployeds.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateOtherIncomeAsync(string recordId, OtherIncome otherIncomeToUpdate)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    OtherIncome otherIncome = record.Income.First().OtherIncome.FirstOrDefault(p => p.Id == otherIncomeToUpdate.Id);

        //    if (otherIncome != null)
        //    {
        //        int index = record.Income.First().OtherIncome.ToList().IndexOf(otherIncome);

        //        if (index != -1)
        //        {
        //            List<OtherIncome> otherIncomes = record.Income.First().OtherIncome.ToList();
        //            otherIncomes[index] = otherIncomeToUpdate;
        //            record.Income.First().OtherIncome = otherIncomes.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateNotEmployedAsync(string recordId, NotEmployed notEmployedToUpdate)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    NotEmployed notEmployed = record.Income.First().NotEmployed.FirstOrDefault(p => p.Id == notEmployedToUpdate.Id);

        //    if (notEmployed != null)
        //    {
        //        int index = record.Income.First().NotEmployed.ToList().IndexOf(notEmployed);

        //        if (index != -1)
        //        {
        //            List<NotEmployed> notEmployeds = record.Income.First().NotEmployed.ToList();
        //            notEmployeds[index] = notEmployedToUpdate;
        //            record.Income.First().NotEmployed = notEmployeds.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateRentalIncomedAsync(string recordId, RentalIncome rentalIncomeToUpdate)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    RentalIncome rentalIncome = record.Income.First().RentalIncome.FirstOrDefault(p => p.Id == rentalIncomeToUpdate.Id);

        //    if (rentalIncome != null)
        //    {
        //        int index = record.Income.First().RentalIncome.ToList().IndexOf(rentalIncome);

        //        if (index != -1)
        //        {
        //            List<RentalIncome> rentalIncomes = record.Income.First().RentalIncome.ToList();
        //            rentalIncomes[index] = rentalIncomeToUpdate;
        //            record.Income.First().RentalIncome = rentalIncomes.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateLiabilityAsync(string recordId, Liability toUpdateLiability)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Liability liability = record.Liability.FirstOrDefault(h => h.Id == toUpdateLiability.Id);

        //    if (liability != null)
        //    {
        //        int index = record.Liability.ToList().IndexOf(liability);

        //        if (index != -1)
        //        {
        //            List<Liability> liabilities = record.Liability.ToList();
        //            liabilities[index] = toUpdateLiability;
        //            record.Liability = liabilities.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateExpenseAsync(string recordId, Expense toUpdateExpense)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    Expense expense = record.Expenses.FirstOrDefault(h => h.Id == toUpdateExpense.Id);

        //    if (expense != null)
        //    {
        //        int index = record.Expenses.ToList().IndexOf(expense);

        //        if (index != -1)
        //        {
        //            List<Expense> expenses = record.Expenses.ToList();
        //            expenses[index] = toUpdateExpense;
        //            record.Expenses = expenses.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

        //public async Task<Record> UpdateNewLoanAsync(string recordId, NewLoan toUpdateNewLoan)
        //{
        //    Record record = await _recordRepository.GetByIdAsync(recordId);

        //    NewLoan newLoan = record.NewLoan.FirstOrDefault(h => h.Id == toUpdateNewLoan.Id);

        //    if (newLoan != null)
        //    {
        //        int index = record.NewLoan.ToList().IndexOf(newLoan);

        //        if (index != -1)
        //        {
        //            List<NewLoan> newLoans = record.NewLoan.ToList();
        //            newLoans[index] = toUpdateNewLoan;
        //            record.NewLoan = newLoans.AsEnumerable();
        //        }
        //    }

        //    await _recordRepository.UpdateByIdAsync(recordId, record);

        //    return record;
        //}

    }
}
