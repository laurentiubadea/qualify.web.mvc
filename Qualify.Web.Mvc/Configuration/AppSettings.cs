﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Qualify.Web.Mvc.Configuration
{
    public static class AppSettings
    {
        public static IConfiguration Configuration { get; set; }
        public static IHostingEnvironment Environment { get; set; }
        public static bool SecurityEnabled => bool.Parse(Configuration[Constants.ApplySecurityConfigKey]);
        public static string DefaultLoginUserUniqueID => Configuration[Constants.DefaultLoginUserUniqueIDCoinfigKey];
    }
}
