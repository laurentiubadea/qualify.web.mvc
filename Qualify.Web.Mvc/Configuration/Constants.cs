﻿namespace Qualify.Web.Mvc.Configuration
{
    public static class Constants
    {
        internal static string SubmitUmiOrchestratorLambdaFunctionArn = "Submit:Umi:SubmitOrchestratorLambdaFunctionArn"; //OLD, get rid of this at some point
        internal static string SubmitUmiOrchestratorS3DestinationBucket = "Submit:Umi:DestinationBucket";
        internal static string SubmitUmiOrchestratorS3DestinationKeyPrefix = "Submit:Umi:DestinationKeyPrefix";
        internal static string SubmitUmiRequestType = "Submit:Umi:RequestType";

        internal static string SubmitPricingOrchestratorLambdaFunctionArn = "Submit:Pricing:SubmitOrchestratorLambdaFunctionArn";
        internal static string SubmitPricingOrchestratorS3DestinationBucket = "Submit:Pricing:DestinationBucket";
        internal static string SubmitPricingOrchestratorS3DestinationKeyPrefix = "Submit:Pricing:DestinationKeyPrefix";
        internal static string SubmitPricingRequestType = "Submit:Pricing:RequestType";

        internal static string SubmitLmiOrchestratorLambdaFunctionArn = "Submit:Lmi:SubmitOrchestratorLambdaFunctionArn";
        internal static string SubmitLmiOrchestratorS3DestinationBucket = "Submit:Lmi:DestinationBucket";
        internal static string SubmitLmiOrchestratorS3DestinationKeyPrefix = "Submit:Lmi:DestinationKeyPrefix";
        internal static string SubmitLmiRequestType = "Submit:Lmi:RequestType";

        internal static string AwsAccessKey = "AWS:AccessKey";
        internal static string AwsSecretKey = "AWS:SecretKey";

        internal static string HardCodedUserId = "HardCodedUserId";

        internal static string PublicJsonS3Versions = "PublicJsonS3Versions";
        internal static string EnvironmentBucket = "EnvironmentBucket";

        internal static string OneTimeUseUserTokenPayloadJwtKey = "OneTimeUseUserTokenPayloadJwtKey";
        internal static string AuthorizeLambdaFunctionNameConfigKey = "AuthorizeLambdaFunctionName";
        internal static string ApplySecurityConfigKey = "SecurityEnabled";
        internal static string DefaultLoginUserUniqueIDCoinfigKey = "DefaultLoginUserUniqueID";
    }
}
