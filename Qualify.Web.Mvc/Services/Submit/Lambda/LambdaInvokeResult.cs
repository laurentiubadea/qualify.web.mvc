﻿using Amazon.Lambda.Model;
using Newtonsoft.Json;
using System;
using System.Text;

namespace Simpology.Aws.Infrastructure.Lambda
{
    public class LambdaInvokeResult<T>
    {
        public T Result { get; set; }
        public bool HasError => LambdaException != null ? true : false;
        public LambdaInvokeException LambdaException { get; set; }

        public static LambdaInvokeResult<T> GenerateResult(InvokeResponse invokeResponse)
        {
            LambdaInvokeResult<T> result = new LambdaInvokeResult<T>();
            string decodedResultString = Encoding.UTF8.GetString(invokeResponse.Payload.ToArray());

            if (decodedResultString.Contains("errorMessage", StringComparison.InvariantCultureIgnoreCase))
            {
                result.LambdaException = JsonConvert.DeserializeObject<LambdaInvokeException>(decodedResultString);
                return result;
            }
            //
            result.Result = JsonConvert.DeserializeObject<T>(decodedResultString);

            return result;
        }
    }
}
