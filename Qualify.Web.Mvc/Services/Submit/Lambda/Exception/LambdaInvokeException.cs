﻿using System.Collections.Generic;

namespace Simpology.Aws.Infrastructure
{
    public class LambdaInvokeException
    {
        public string ErrorType { get; set; }   
        public string ErrorMessage { get; set; }
        public IEnumerable<string> StackTrace { get; set; }
        public LambdaInvokeException Cause { get; set; }
    }
}
