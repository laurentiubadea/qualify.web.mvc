﻿using System.Collections.Generic;

namespace Qualify.Web.Mvc.Services.Submit
{
    public static class SubmitResponseService
    {
        private static List<string> Keys = new List<string>
        {
            "RequestID",
            "AppNbr",
            "SourceMessageID"
        };

        public static IDictionary<string, string> GetResponseInfo(string responseJson)
        {
            var responseDictionary = new Dictionary<string, string>();

            foreach (var key in Keys)
            {
                if (Keys.Contains(key))
                {
                    responseDictionary.Add(key, GetValue(responseJson, key));
                }
            }

            return responseDictionary;
        }

        private static string GetValue(string json, string key)
        {
            string lastPartOfJson = json.Substring(json.IndexOf(key) + key.Length + 4);
            var value =  lastPartOfJson.Substring(0, lastPartOfJson.IndexOf("\","));
            return value;
        }
    }
}
