﻿namespace Qualify.Web.Mvc.Services.Submit.Models
{
    public class SubmitRequestModel
    {
        public object RecordJson { get; internal set; }
        public string UserDetailsJson { get; internal set; }
        public string S3DestinationBucket { get; internal set; }
        public string S3DestinationKeyPrefix { get; internal set; }
        public string RequestType { get; set; }
    }
}
