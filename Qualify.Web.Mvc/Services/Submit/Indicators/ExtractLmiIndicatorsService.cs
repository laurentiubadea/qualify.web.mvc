﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Qualify.Domain.Models.Temporary;
using System.Text;

namespace Qualify.Web.Mvc.Services.Submit
{
    public class ExtractLmiIndicatorsService
    {
        private const string ResultTitle = "Result";
        private const string TotalUmiResultTitle = "TOTAL UMI RESULT";
        private const string MaximumBorrowingPowerTitle = "MAXIMUM BORROWING POWER";
        private const string TotalExpensesTitle = "TOTAL EXPENSES";
        private const string TotalIncomeTitle = "TOTAL INCOME";

        private const string ServicingApplicationUMI = "ServicingApplicationUMI";

        private const string GreenHex = "#d8eaac";
        private const string RedHex = "#e2909f";
        private const string YellowHex = "#ffff00";

        internal static IEnumerable<Indicator> Extract(dynamic lambdaResultJObject)
        {
            List<Indicator> indicators = new List<Indicator>();

            indicators.Add(DefaultDummyResult()); //remove this when real indicators are added

            //indicators.Add(ExtractResult(lambdaResultJObject));
            //indicators.Add(ExtractTotalUmiResult(lambdaResultJObject));
            //indicators.Add(ExtractMaximumBorrowingPower(lambdaResultJObject));
            //indicators.Add(ExtractTotalIncome(lambdaResultJObject));
            //indicators.Add(ExtractTotalExpenses(lambdaResultJObject));

            return indicators;
        }

        private static Indicator ExtractResult(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = ResultTitle,
            };

            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["CdpHostRecommendDecision"] != null)
                    {
                        string value = lambdaResultJObject["ApiCtsDV"]["findec"]["CdpHostRecommendDecision"];

                        if (value == "S")
                        {
                            indicator.Value = "Approve Subject To";
                            indicator.BackgroundColorHex = GreenHex;
                        }
                        else if (value == "I")
                        {
                            indicator.Value = "Approve in Principle";
                            indicator.BackgroundColorHex = GreenHex;
                        }
                        else if (value == "R")
                        {
                            indicator.Value = "Refer";
                            indicator.BackgroundColorHex = YellowHex;
                        }
                        else if (value == "P")
                        {
                            indicator.Value = "Repackage";
                            indicator.BackgroundColorHex = YellowHex;
                        }
                        else if (value == "A")
                        {
                            indicator.Value = "Queue for Approve";
                            indicator.BackgroundColorHex = GreenHex;
                        }
                        else if (value == "B")
                        {
                            indicator.Value = "Refer Risk Based Pricing";
                            indicator.BackgroundColorHex = YellowHex;
                        }
                        else
                        {
                            indicator.Value = "Recommend Decline";
                            indicator.BackgroundColorHex = RedHex;
                        }

                        return indicator;
                    }
            indicator.BackgroundColorHex = RedHex;
            indicator.Value = "None";
            return indicator;
        }

        private static Indicator ExtractTotalUmiResult(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = TotalUmiResultTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["ServicingApplicationUMI"] != null)
                    {
                        indicator.Value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["ServicingApplicationUMI"]}";
                    }
            return indicator;
        }

        private static Indicator ExtractMaximumBorrowingPower(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = MaximumBorrowingPowerTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["MaxLoanwithLMI"] != null)
                    {
                        indicator.Value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["MaxLoanwithLMI"]}";
                    }
            return indicator;
        }

        private static Indicator ExtractTotalExpenses(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = TotalExpensesTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["ServicingExpenseTot"] != null)
                    {
                        indicator.Value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["ServicingExpenseTot"]}";
                        indicator.BorderColorHex = RedHex;
                    }
            return indicator;
        }

        private static Indicator ExtractTotalIncome(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = TotalIncomeTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["ServicingIncomeTot"] != null)
                    {
                        indicator.Value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["ServicingIncomeTot"]}";
                        indicator.BorderColorHex = GreenHex;
                    }
            return indicator;
        }

        private static Indicator DefaultDummyResult()
        {
            return new Indicator
            {
                Title = "Total",
                BackgroundColorHex = RedHex,
                Value = "None"
            };
        }

        public static object GetDeepPropertyValue(object src, string propName)
        {
            if (propName.Contains('.'))
            {
                string[] Split = propName.Split('.');
                string remainingProperty = propName.Substring(propName.IndexOf('.') + 1);
                return GetDeepPropertyValue(src.GetType().GetProperty(Split[0]).GetValue(src, null), remainingProperty);
            }
            else
                return src.GetType().GetProperty(propName).GetValue(src, null);
        }

    }
}
