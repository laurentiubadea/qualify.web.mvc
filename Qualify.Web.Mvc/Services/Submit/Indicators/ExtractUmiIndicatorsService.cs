﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Qualify.Domain.Models.Temporary;
using System.Text;

namespace Qualify.Web.Mvc.Services.Submit
{
    public class ExtractUmiIndicatorsService
    {
        private const string DtiTitle = "DTI";
        private const string TotalUmiResultTitle = "UMI";
        private const string MaximumBorrowingPowerTitle = "MAXIMUM BORROWING POWER";
        private const string TotalExpensesTitle = "TOTAL EXPENSES";
        private const string TotalIncomeTitle = "TOTAL INCOME";

        private const string ServicingApplicationUMI = "ServicingApplicationUMI";

        private const string GreenHex = "#d8eaac";
        private const string RedHex = "#e2909f";
        private const string YellowHex = "#ffff00";

        internal static IEnumerable<Indicator> Extract(dynamic lambdaResultJObject)
        {
            List<Indicator> indicators = new List<Indicator>();

            indicators.Add(ExtractTotalUmiResult(lambdaResultJObject));
            indicators.Add(ExtractDtiResult(lambdaResultJObject));
            indicators.Add(ExtractMaximumBorrowingPower(lambdaResultJObject));
            indicators.Add(ExtractTotalIncome(lambdaResultJObject));
            indicators.Add(ExtractTotalExpenses(lambdaResultJObject));

            return indicators;
        }

        private static Indicator ExtractDtiResult(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = DtiTitle,
            };

            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["AppnDTIRatio"] != null)
                    {
                        string value = lambdaResultJObject["ApiCtsDV"]["findec"]["AppnDTIRatio"];
                        indicator.Value = value;
                    }
            return indicator;
        }

        private static Indicator ExtractTotalUmiResult(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = TotalUmiResultTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["BankApplicationUmi"] != null)
                    {
                        string value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["BankApplicationUmi"]}";
                        //if (int.Parse(value ?? "0") > 0)
                        //    indicator.BackgroundColorHex = GreenHex;
                        //else
                        //    indicator.BackgroundColorHex = RedHex;

                        indicator.Value = value;
                    }
            return indicator;
        }

        private static Indicator ExtractMaximumBorrowingPower(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = MaximumBorrowingPowerTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["MaxBorrowingLimit"] != null)
                    {
                        indicator.Value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["MaxBorrowingLimit"]}";
                    }
            return indicator;
        }

        private static Indicator ExtractTotalExpenses(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = TotalExpensesTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["BankTotalExp"] != null)
                    {
                        indicator.Value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["BankTotalExp"]}";
                        indicator.BorderColorHex = RedHex;
                    }
            return indicator;
        }

        private static Indicator ExtractTotalIncome(dynamic lambdaResultJObject)
        {
            Indicator indicator = new Indicator
            {
                Title = TotalIncomeTitle
            };
            if (lambdaResultJObject["ApiCtsDV"] != null)
                if (lambdaResultJObject["ApiCtsDV"]["findec"] != null)
                    if (lambdaResultJObject["ApiCtsDV"]["findec"]["BankTotalIncome"] != null)
                    {
                        indicator.Value = $"${lambdaResultJObject["ApiCtsDV"]["findec"]["BankTotalIncome"]}";
                        indicator.BorderColorHex = GreenHex;
                    }
            return indicator;
        }

        public static object GetDeepPropertyValue(object src, string propName)
        {
            if (propName.Contains('.'))
            {
                string[] Split = propName.Split('.');
                string remainingProperty = propName.Substring(propName.IndexOf('.') + 1);
                return GetDeepPropertyValue(src.GetType().GetProperty(Split[0]).GetValue(src, null), remainingProperty);
            }
            else
                return src.GetType().GetProperty(propName).GetValue(src, null);
        }

    }
}
