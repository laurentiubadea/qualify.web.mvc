﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Services.Submit
{
    public static class SubmitS3DestinationKeyPrefixBuilder
    {
        public static string Build(string origin, string recordId, DateTime timeStamp)
        {
            string prefix = $"{origin}/{recordId}/{timeStamp.ToString("yyyy-MM-dd hh:mm:ss")}";
            return prefix;
        }
    }
}
