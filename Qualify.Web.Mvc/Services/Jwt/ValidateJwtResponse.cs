﻿namespace Qualify.Web.Mvc.Services.Jwt
{
    public class ValidateJwtResponse
    {
        public bool  Valid { get; set; }
        public string ErrorMessage { get; set; }
    }
}
