﻿using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Model;
using JWT;
using JWT.Serializers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Qualify.Domain.Models.Identity;
using Qualify.Repository;
using Qualify.Web.Mvc.Configuration;
using Simpology.Aws.Infrastructure.Lambda;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Services.Jwt
{
    public class JwtService : IJwtService
    {
        private readonly IConfiguration _configuration;
        private readonly ILoanWriterRepository _loanWriterRepository;

        public JwtService(ILoanWriterRepository loanWriterRepository,
             IConfiguration configuration)
        {
            _configuration = configuration;
            _loanWriterRepository = loanWriterRepository;
        }

        public async Task<ValidateJwtResponse> ValidateAsync(string jwt)
        {
            ValidateJwtResponse response = new ValidateJwtResponse
            {
                Valid = true
            };

            //1. call lambda
            bool isJwtValid = await ValidateJwtWithAwsLambdaAsync(jwt);
            if (!isJwtValid)
            {
                response.Valid = false;
                response.ErrorMessage = "JWT is not valid";
                return response;
            };
            
            //2. check the one time use guid from jwt payload
            string oneTimeUseUserToken = default(string);
                IDictionary<string, object> jwtKeyValuePairs = GetJwtPayload(jwt);
                oneTimeUseUserToken = jwtKeyValuePairs[_configuration[Constants.OneTimeUseUserTokenPayloadJwtKey]].ToString();

            //3. check the one time use token in DB and identify the user
            LoanWriter loanWriter = await _loanWriterRepository.GetUserByOneTimeUseTokenAsync(oneTimeUseUserToken);

            if (loanWriter == null)
            {
                response.Valid = false;
                response.ErrorMessage = "User not found by one time use token";
                return response;
            }

            return response;
        }

        public async Task<bool> ValidateJwtWithAwsLambdaAsync(string jwt)
        {
            APIGatewayCustomAuthorizerRequest aPIGatewayCustomAuthorizerRequest = new APIGatewayCustomAuthorizerRequest
            {
                AuthorizationToken = $"Bearer {jwt}"
            };

            string awsAccessKey = _configuration[Constants.AwsAccessKey];
            string awsSecretKey = _configuration[Constants.AwsSecretKey];
            AmazonLambdaClient amazonLambdaClient = new AmazonLambdaClient(awsAccessKey, awsSecretKey, RegionEndpoint.APSoutheast2);

            InvokeResponse invokeResponseLambda = await amazonLambdaClient.InvokeAsync(new InvokeRequest
            {
                FunctionName = _configuration[Constants.AuthorizeLambdaFunctionNameConfigKey],
                Payload = JsonConvert.SerializeObject(aPIGatewayCustomAuthorizerRequest),
                InvocationType = InvocationType.RequestResponse
            });

            LambdaInvokeResult<APIGatewayCustomAuthorizerResponse> lambdaResult = LambdaInvokeResult<APIGatewayCustomAuthorizerResponse>.GenerateResult(invokeResponseLambda);

            if (lambdaResult.HasError)
            {
                throw new Exception(lambdaResult.LambdaException.ErrorMessage);
            }

            return lambdaResult.Result.PolicyDocument.Statement[0].Effect == "Allow";
        }

        public IDictionary<string, object> GetJwtPayload(string jwt)
        {
            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);
            IDictionary<string, object> payloadJson = decoder.DecodeToObject<IDictionary<string, object>>(jwt);
            return payloadJson;
        }
    }
}
