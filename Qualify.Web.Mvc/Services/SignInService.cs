﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Qualify.Domain.Models.Identity;
using Qualify.Web.Mvc.ExtensionMethods;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Services
{
    public static class SignInService
    {
        public static async Task SignInAsync(HttpContext httpContext, LoanWriter loanWriter)
        {
            //create Identity
            ClaimsIdentity identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(ClaimTypes.Name, loanWriter.LoanWriterEmail ?? ""));
            identity.AddClaim(new Claim("ReturnURL", loanWriter.ReturnUrl));
            identity.AddClaim(ClaimTypes.UserData, loanWriter);

            //sign the identity
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties
            {
                IsPersistent = true,
                AllowRefresh = true
            });
        }
    }
}
