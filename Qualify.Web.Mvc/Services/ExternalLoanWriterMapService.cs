﻿using Qualify.Domain.Models.Identity;
using Qualify.Web.Mvc.Models.Profile;
using System.Collections.Generic;
using System.Reflection;

namespace Qualify.Web.Mvc.Services
{
    public static class ExternalLoanWriterMapService
    {
        public static IList<string> GetLockedFields(ExternalLoanWriterModel externalLoanWriterModel)
        {
            List<string> lockedFields = new List<string>();
            foreach (PropertyInfo propertyInfo in externalLoanWriterModel.GetType().GetProperties())
            {
                if (propertyInfo.GetValue(externalLoanWriterModel) != null
                    && !string.IsNullOrEmpty(propertyInfo.GetValue(externalLoanWriterModel).ToString()))
                {
                    lockedFields.Add(propertyInfo.Name);
                }
            }
            return lockedFields;
        }

        public static LoanWriter MapExternalOnCurrent(ExternalLoanWriterModel externalLoanWriterModel, LoanWriter loanWriter)
        {
            loanWriter.AO = externalLoanWriterModel.AO;
            loanWriter.SAO = externalLoanWriterModel.SAO;
            loanWriter.LoanWriterFirstName = externalLoanWriterModel.FirstName;
            loanWriter.LoanWriterSurname = externalLoanWriterModel.Surname;
            loanWriter.LoanWriterEmail = externalLoanWriterModel.Email;
            loanWriter.Role = externalLoanWriterModel.Role;
            loanWriter.LoanWriterMobile.Number = externalLoanWriterModel.Mobile;
            loanWriter.LoanWriterAddress.Suburb = externalLoanWriterModel.Suburb;
            loanWriter.LoanWriterAddress.AustralianState = externalLoanWriterModel.State;
            loanWriter.LoanWriterAddress.Country = externalLoanWriterModel.Country;
            loanWriter.LoanWriterAddress.AustralianPostCode = externalLoanWriterModel.Postcode;
            return loanWriter;
        }
    }
}
