﻿using Microsoft.Extensions.Configuration;
using Qualify.Web.Mvc.Configuration;

namespace Qualify.Web.Mvc
{
    public class AppSettings
    {
        public string JWT_Secret { get; set; }
        public string Client_URL { get; set; }
        public string Site { get; set; }
        public string ExpiryInMinutes { get; set; }
        public static IConfiguration Configuration { get; set; }
        public static bool SecurityEnabled => bool.Parse(Configuration[Constants.ApplySecurityConfigKey]);
        public static string DefaultLoginUserUniqueID => Configuration[Constants.DefaultLoginUserUniqueIDCoinfigKey];
        public static string PublicJsonS3Versions => Configuration[Constants.PublicJsonS3Versions];
        public static string EnvironmentBucket => Configuration[Constants.EnvironmentBucket];
    }
}
