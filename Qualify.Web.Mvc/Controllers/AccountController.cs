﻿using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Qualify.Domain.Models.Identity;
using Qualify.Repository;
using Qualify.Web.Mvc.Configuration;
using Qualify.Web.Mvc.ExtensionMethods;
using Qualify.Web.Mvc.Filters;
using Qualify.Web.Mvc.Helpers.PartialViewsLoader;
using Qualify.Web.Mvc.Services;
using Qualify.Web.Mvc.Services.Jwt;
using System;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Controllers
{
    [AllowAnonymous]
    [UserMustHaveAllDataActionFilter(Disable = true)]
    public class AccountController : Controller
    {
        private readonly IRecordRepository _recordRepository;
        private readonly IPartialViewsLoader _partialViewsLoader;
        private readonly ILoanWriterRepository _loanWriterRepository;
        private readonly IJwtService _jwtService;
        private readonly IConfiguration _configuration;

        public AccountController(IRecordRepository recordRepository,
            IPartialViewsLoader partialViewsLoader,
            IJwtService jwtService,
            IConfiguration configuration,
            ILoanWriterRepository loanWriterRepository)
        {
            _recordRepository = recordRepository;
            _partialViewsLoader = partialViewsLoader;
            _jwtService = jwtService;
            _configuration = configuration;
            _loanWriterRepository = loanWriterRepository;
        }

        [HttpGet("view/{id}")]
        public async Task<IActionResult> Index(string id, string token)
        {
            string jwt = token;

            if (string.IsNullOrEmpty(token))
            {

                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Index", "Record", new { id });
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                await HttpContext.SignOutAsync();
            }

            //validate jwt
            ValidateJwtResponse validateJwtResponse = null;
            try
            {
                validateJwtResponse = await _jwtService.ValidateAsync(jwt);
            }
            catch (Exception ex)
            {
                ex.Ship(HttpContext);
                return Unauthorized();
            }


            if (validateJwtResponse.Valid == true)
            {
                //Get session data for the user mentioned in the JWT payload. 
                string oneTimeUseToken = _jwtService.GetJwtPayload(jwt)[_configuration[Constants.OneTimeUseUserTokenPayloadJwtKey]].ToString();
                LoanWriter loanWriter = await _loanWriterRepository.GetUserByOneTimeUseTokenAsync(oneTimeUseToken);

                if (loanWriter == null)
                {
                    return Unauthorized();
                }

                //TODO: uncomment on production
                //loanWriter.OneTimeUseToken = null;
                await _loanWriterRepository.UpdateByIdAsync(loanWriter.UniqueID, loanWriter);

                await SignInService.SignInAsync(HttpContext, loanWriter);

                return RedirectToAction("Index", "Record", new { id });
            }

            return Unauthorized();
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Unauthorized();
        }

        public async Task<IActionResult> BackToOrigin()
        {
            string returnUrl = User.GetClaim("ReturnURL").Value;
            await HttpContext.SignOutAsync();
            return Redirect(returnUrl);
        }

    }
}