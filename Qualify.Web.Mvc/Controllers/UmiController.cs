﻿using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Qualify.Domain.Models;
using Qualify.Domain.Models.Identity;
using Qualify.Domain.Models.Temporary;
using Qualify.Repository;
using Qualify.Web.Mvc.Business_Service;
using Qualify.Web.Mvc.Configuration;
using Qualify.Web.Mvc.Helpers.AWS;
using Qualify.Web.Mvc.Requests;
using Qualify.Web.Mvc.Services.Submit;
using Qualify.Web.Mvc.Services.Submit.Models;
using Simpology.Aws.Infrastructure.Lambda;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using ToolKitGrid.JWTApi.Helpers;

namespace Qualify.Web.Mvc.Controllers
{
    [Authorize]
    //[UserMustHaveAllDataActionFilter]
    [ApiController]
    public class UmiController : Controller
    {
        private readonly IRecordRepository _recordRepository;
        private readonly IAddToRecordService _addToRecordService;
        private readonly IRemoveFromRecordService _removeFromRecordService;
        private readonly IRecordUpdateService _recordUpdateService;
        private readonly IAmazonLambda _amazonLambda;
        private readonly ILoanWriterRepository _loanWriterRepository;
        private readonly IConfiguration _configuration;
        private readonly UriApiMenthodsConfiguration _apiSettingsUrl;
        private readonly IAwsConfigurationFactory _awsConfigurationFactory;

        public UmiController(IRecordRepository recordRepository,
                              IAddToRecordService addToRecordService,
                              IRemoveFromRecordService removeFromRecordService,
                              IRecordUpdateService recordUpdateService,
                              UriApiMenthodsConfiguration apiSettingsUrl,
                              IConfiguration configuration,
                              IAmazonLambda amazonLambda,
                              ILoanWriterRepository loanWriterRepository,
                              IAwsConfigurationFactory awsConfigurationFactory)
        {
            _recordRepository = recordRepository;
            _addToRecordService = addToRecordService;
            _removeFromRecordService = removeFromRecordService;
            _recordUpdateService = recordUpdateService;
            _apiSettingsUrl = apiSettingsUrl;
            _configuration = configuration;
            _amazonLambda = amazonLambda;
            _loanWriterRepository = loanWriterRepository;
            _awsConfigurationFactory = awsConfigurationFactory;
        }
        private new string Url
        {
            get
            {
                return _apiSettingsUrl.RecordQualifyUrl;
            }
        }

        [HttpPost("qualifyRedirect")]
        public async Task<IActionResult> QualifyRedirect([FromBody] string recordId)
        {
            SendJwtResult result = new SendJwtResult();
            result.Url = $"{Url}/view/" + recordId;
            return Ok(result);
        }

        [HttpPost]
        [Route("Umi/SaveRecord")]
        public async Task<IActionResult> SaveRecord([FromForm]SaveRecordRequest saveRecordRequest)
        {
            var document = BsonSerializer.Deserialize<BsonDocument>(saveRecordRequest.RecordDataJson);

            Record recordData = BsonSerializer.Deserialize<Record>(document);

            Record existingRecord = await _recordRepository.GetByUniqueIdAsync(recordData.UniqueID);

            recordData.Id = existingRecord.Id;
            recordData.DateUpdated = DateTime.Now;

            await _recordRepository.UpdateByIdAsync(recordData.Id, recordData);
          
            return Ok();
        }

        //Submit
        [HttpPost]
        [Route("Umi/SubmitRecord")]
        public async Task<IActionResult> CalculateUmi([FromForm]string recordId, [FromForm]string cleanRecordDataJson)
        {
            DateTime timeStamp = DateTime.UtcNow;

            var document = BsonSerializer.Deserialize<BsonDocument>(cleanRecordDataJson);
            CleanRecord cleanRecordData = BsonSerializer.Deserialize<CleanRecord>(document);
            cleanRecordData.CleanEmptyArrays();

            //get the record from DB
            Record recordToUpdateSubmitDate = await _recordRepository.GetByUniqueIdAsync(recordId);
            recordToUpdateSubmitDate.Umi = new Umi
            {
                RequestDate = timeStamp
            };

            await _recordRepository.UpdateByIdAsync(recordToUpdateSubmitDate.Id, recordToUpdateSubmitDate);

            //get the user details from DB (LoanWritter)
            LoanWriter loanWriter = await _loanWriterRepository.GetByUniqueIdAsync(_configuration[Constants.HardCodedUserId]);

            //get from config  destination bucket and key prefix (generate key prefix)
            string s3DestinationBucket = _configuration[Constants.SubmitUmiOrchestratorS3DestinationBucket];
            string s3DestinationKeyPrefix = SubmitS3DestinationKeyPrefixBuilder.Build(_configuration[Constants.SubmitUmiOrchestratorS3DestinationKeyPrefix], recordId, timeStamp);

            //create the lambda request 
            SubmitRequestModel submitRequestModel = new SubmitRequestModel
            {
                RecordJson = JsonConvert.SerializeObject(cleanRecordData, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                }),
                UserDetailsJson = JsonConvert.SerializeObject(loanWriter),
                S3DestinationBucket = s3DestinationBucket,
                S3DestinationKeyPrefix = s3DestinationKeyPrefix,
                RequestType = _configuration[Constants.SubmitUmiRequestType]
            };

            string awsAccessKey = _configuration[Constants.AwsAccessKey];
            string awsSecretKey = _configuration[Constants.AwsSecretKey];
            AmazonLambdaClient amazonLambdaClient = new AmazonLambdaClient(awsAccessKey, awsSecretKey, RegionEndpoint.APSoutheast2);
            InvokeRequest invokeRequest = new InvokeRequest
            {
                InvocationType = InvocationType.RequestResponse,
                Payload = JsonConvert.SerializeObject(submitRequestModel),
                FunctionName = _configuration[Constants.SubmitUmiOrchestratorLambdaFunctionArn]
            };

            //call submit lambda
            string cdpResponseJson = "";
            IEnumerable<Indicator> indicators;
            try
            {
                InvokeResponse invokeResponse = await amazonLambdaClient.InvokeAsync(invokeRequest);
                LambdaInvokeResult<object> lambdaInvokeResult = LambdaInvokeResult<object>.GenerateResult(invokeResponse);
                if (lambdaInvokeResult.HasError)
                {
                    throw new Exception(lambdaInvokeResult.LambdaException.ErrorMessage);
                }
                var lambdaResult = lambdaInvokeResult.Result.ToString();
                JObject lambdaResultJObject = JsonConvert.DeserializeObject<JObject>(lambdaResult);
                //extract UMI indicators
                cdpResponseJson = lambdaResultJObject["Response"].ToString();
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
                throw ex;
            }

            //extract indicators
            try
            {
                indicators = ExtractUmiIndicatorsService.Extract(JsonConvert.DeserializeObject<dynamic>(cdpResponseJson));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }

            //Extract JSON properties
            IDictionary<string, string> responseItems =  SubmitResponseService.GetResponseInfo(JsonConvert.SerializeObject(JsonConvert.DeserializeObject<dynamic>(cdpResponseJson), Formatting.Indented));

            //this last part not tested yet 
            recordToUpdateSubmitDate.Umi.UmiResult = cdpResponseJson;
            recordToUpdateSubmitDate.Umi.Indicators = indicators;

            await _recordRepository.UpdateByIdAsync(recordToUpdateSubmitDate.Id, recordToUpdateSubmitDate);

            return Json(new
            {
                Success = true, 
                Record = JsonConvert.SerializeObject(recordToUpdateSubmitDate)
            });
        }

        [HttpPost]
        [Route("Umi/Delete")]
        public async Task<IActionResult> Delete([FromForm]string recordId)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(recordId);

            record.Umi = null;

            await _recordRepository.UpdateByIdAsync(record.Id, record);

            return new JsonResult(new
            {
                Record = JsonConvert.SerializeObject(record)
            });
        }

        [HttpGet]
        [Route("Umi/DownloadPackage")]
        public async Task<FileResult> DownloadPackage(string recordId)
        {
            AwsConfiguration awsConfiguration = _awsConfigurationFactory.Get();
            byte[] resultData = null;
            Record record = await _recordRepository.GetByUniqueIdAsync(recordId);
            DateTime timestamp = record.Umi.RequestDate ?? DateTime.Now;

            var amazonS3Instance = new AmazonS3Client(awsConfiguration.AccessKey, awsConfiguration.SecretKey, awsConfiguration.Region);

            //test amazonS3
            ListObjectsRequest listObjectsRequest = new ListObjectsRequest
            {
                BucketName = _configuration[Constants.SubmitUmiOrchestratorS3DestinationBucket],
                Delimiter = "/",
                Prefix = $"{SubmitS3DestinationKeyPrefixBuilder.Build(_configuration[Constants.SubmitUmiOrchestratorS3DestinationKeyPrefix], recordId, timestamp)}/",
                MaxKeys = 20
            };

            ListObjectsResponse listObjectsResponse = await amazonS3Instance.ListObjectsAsync(listObjectsRequest);
            using (MemoryStream ms = new MemoryStream())
            {
                using (ZipArchive zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    foreach (S3Object s3Object in listObjectsResponse.S3Objects)
                    {
                        ZipArchiveEntry zipArchiveEntry = zipArchive.CreateEntry(Path.GetFileName(s3Object.Key));
                        GetObjectRequest getObjectRequest = new GetObjectRequest
                        {
                            BucketName = s3Object.BucketName,
                            Key = s3Object.Key,
                        };
                        GetObjectResponse getObjectResponse = await amazonS3Instance.GetObjectAsync(getObjectRequest);
                        using (Stream stream = zipArchiveEntry.Open())
                        {
                            await getObjectResponse.ResponseStream.CopyToAsync(stream);
                        }
                    }
                }
                resultData = ms.ToArray();
            }

            return File(resultData, "application/zip", "DownloadPackage.zip");
        }
    }
}
