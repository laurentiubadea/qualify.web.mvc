﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Qualify.Domain.Models;
using Qualify.Repository;
using Qualify.Web.Mvc.Models;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Controllers
{
    [Authorize]
    //[UserMustHaveAllDataActionFilter]
    public class RecordController : Controller
    {
        private readonly IRecordRepository _recordRepository;
        public RecordController(IRecordRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }

        public async Task<IActionResult> Index(string id)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(id);

            if (record == null)
            {
                return NotFound();
            }

            //collect indicators from the CDPResponseJson 
            RecordViewModel viewModel = new RecordViewModel
            {
                DataJson = JsonConvert.SerializeObject(record)
            };

            return View(viewModel);
        }
    }
}