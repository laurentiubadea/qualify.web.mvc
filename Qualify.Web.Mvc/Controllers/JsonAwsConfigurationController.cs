﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Mvc;
using Qualify.Web.Mvc.Helpers.AWS;
using System;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Controllers
{
    [Route("[controller]/[action]")]
    public class JsonAwsConfigurationController : Controller
    {
        private readonly IAwsConfigurationFactory _awsConfigurationFactory;

        public JsonAwsConfigurationController(IAwsConfigurationFactory awsConfigurationFactory)
        {
            _awsConfigurationFactory = awsConfigurationFactory;
        }

        [HttpGet]
        public async Task<IActionResult> GenerateJsonPresignedUrl(PresignedUrlData presignedUrlData)
        {
            AwsConfiguration awsConfiguration = _awsConfigurationFactory.Get();
            var amazonS3Instance = new AmazonS3Client(awsConfiguration.AccessKey, awsConfiguration.SecretKey, awsConfiguration.Region);

            //generate presigned url
            string presignedUrl = amazonS3Instance.GetPreSignedURL(new GetPreSignedUrlRequest
            {
                BucketName = AppSettings.EnvironmentBucket,
                Key = presignedUrlData.KeyName,
                Expires = DateTime.UtcNow.AddMinutes(2),
                Verb = HttpVerb.GET,
                Protocol = Protocol.HTTPS
            });

            return Json(presignedUrl);
        }

        public async Task<IActionResult> GetJsonEnvSettingsForS3()
        {
            EnvironmentSettingsS3 envSettings = new EnvironmentSettingsS3();
            envSettings.UrlPublicJsonVersions = AppSettings.PublicJsonS3Versions;

            return Json(envSettings);
        }
    }
}
