﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Qualify.Domain.Models;
using Qualify.Repository;
using Qualify.Web.Mvc.Helpers.AWS;
using Qualify.Web.Mvc.Helpers.PartialViewsLoader;
using Qualify.Web.Mvc.Models;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Controllers
{
    [Authorize]
    //[UserMustHaveAllDataActionFilter]
    public class WebController : Controller
    {
        private readonly IRecordRepository _recordRepository;
        private readonly IPartialViewsLoader _partialViewsLoader;
        private readonly IAwsConfigurationFactory _awsConfigurationFactory;
        private readonly IConfiguration _configuration;

        public WebController(IRecordRepository recordRepository,
            IPartialViewsLoader partialViewsLoader,
            IAwsConfigurationFactory awsConfigurationFactory,
            IConfiguration configuration)
        {
            _recordRepository = recordRepository;
            _partialViewsLoader = partialViewsLoader;
            _awsConfigurationFactory = awsConfigurationFactory;
            _configuration = configuration;
        }

        //[HttpGet("view/{id}")]
        //public async Task<IActionResult> ViewRecord(string id, string token)
        //{
        //    Record record = await _recordRepository.GetByUniqueIdAsync(id);

        //    if (record == null)
        //    {
        //        return NotFound();
        //    }

        //    //collect indicators from the CDPResponseJson 
        //    RecordViewModel viewModel = new RecordViewModel
        //    {
        //        DataJson = JsonConvert.SerializeObject(record)
        //    };

        //    return View("Index", viewModel);
        //}

        public async Task<IActionResult> TEMP_LoadData()
        {
            string gigel = System.IO.File.ReadAllText(
                Path.Combine(
                    AppDomain.CurrentDomain.BaseDirectory,
                    "DataModelDbSample.json")
                );
            Record record = JsonConvert.DeserializeObject<Record>(gigel);

            Record newRecord = new Record
            {
                Person = record.Person.ToList(),
                Household = record.Household.ToList(),
                Expenses = record.Expenses.ToList(),
                Liability = record.Liability.ToList(),
                Income = record.Income.ToList(),
                NewLoan = record.NewLoan.ToList(),
                PropertyAsset = record.PropertyAsset.ToList()
            };

            await _recordRepository.CreateAsync(newRecord);

            return Ok();

            //_recordRepository.CreateAsync()
        }

        [HttpPost]
        public async Task<IActionResult> RenderUmiPartialView(string id)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(id);

            if (record == null)
            {
                return NotFound();
            }

            RecordUmiViewModel viewModel = new RecordUmiViewModel
            {
                DataJson = JsonConvert.SerializeObject(record),
                IsUmiSubmittedAtLeastOnce = record.Umi?.RequestDate != null,
                IsUmiCalculationFinished = !string.IsNullOrEmpty(record.Umi?.UmiResult),
                CDPResultJson = record.Umi?.UmiResult,
                UmiIndicators = record.Umi?.Indicators
            };

            if (viewModel.IsUmiCalculationFinished)
            {
                return await RenderResultUmiPartialView(viewModel);
            }
            else
            {
                return await RenderDataUmiPartialView(viewModel);
            }
        }
        private async Task<IActionResult> RenderResultUmiPartialView(RecordUmiViewModel viewModel)
        {
            string viewContent = await _partialViewsLoader.RenderRazorViewToString(this, "WebTemplates/_UmiResult", viewModel);

            return new JsonResult(new
            {
                partialView = viewContent,
                model = viewModel
            });
        }
        private async Task<IActionResult> RenderDataUmiPartialView(RecordUmiViewModel viewModel)
        {
            string viewContent = await _partialViewsLoader.RenderRazorViewToString(this, "WebTemplates/_Umi", viewModel);

            return new JsonResult(new
            {
                partialView = viewContent,
                model = viewModel
            });
        }

        [HttpPost]
        public async Task<IActionResult> RenderPricingPartialView(string id)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(id);

            if (record == null)
            {
                return NotFound();
            }

            RecordPricingViewModel viewModel = new RecordPricingViewModel
            {
                DataJson = JsonConvert.SerializeObject(record),
                IsSubmittedAtLeastOnce = record.Pricing?.RequestDate != null,
                IsCalculationFinished = !string.IsNullOrEmpty(record.Pricing?.Result),
                QualifyResult = record.Pricing?.Result,
                Indicators = record.Pricing?.Indicators
            };

            if (viewModel.IsCalculationFinished)
            {
                return await RenderResultPricingPartialView(viewModel);
            }
            else
            {
                return await RenderDataPricingUmiPartialView(viewModel);
            }
        }
        private async Task<IActionResult> RenderResultPricingPartialView(RecordPricingViewModel viewModel)
        {
            string viewContent = await _partialViewsLoader.RenderRazorViewToString(this, "WebTemplates/_PricingResult", viewModel);

            return new JsonResult(new
            {
                partialView = viewContent,
                model = viewModel
            });
        }
        private async Task<IActionResult> RenderDataPricingUmiPartialView(RecordPricingViewModel viewModel)
        {
            string viewContent = await _partialViewsLoader.RenderRazorViewToString(this, "WebTemplates/_Pricing", viewModel);

            return new JsonResult(new
            {
                partialView = viewContent,
                model = viewModel
            });
        }

        [HttpPost]
        public async Task<IActionResult> RenderLmiPartialView(string id)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(id);

            if (record == null)
            {
                return NotFound();
            }

            RecordLmiViewModel viewModel = new RecordLmiViewModel
            {
                DataJson = JsonConvert.SerializeObject(record),
                IsCalculationFinished = !string.IsNullOrEmpty(record.Lmi?.Result),
                IsSubmittedAtLeastOnce = record.Lmi?.RequestDate != null,
                ResultJson = record.Lmi?.Result,
                Indicators = record.Lmi?.Indicators
            };

            if (viewModel.IsCalculationFinished)
            {
                return await RenderResultLmiPartialView(viewModel);
            }
            else
            {
                return await RenderDataLmiPartialView(viewModel);
            }
        }
        private async Task<IActionResult> RenderResultLmiPartialView(RecordLmiViewModel viewModel)
        {
            string viewContent = await _partialViewsLoader.RenderRazorViewToString(this, "WebTemplates/_LmiResult", viewModel);

            return new JsonResult(new
            {
                partialView = viewContent,
                model = viewModel
            });
        }
        private async Task<IActionResult> RenderDataLmiPartialView(RecordLmiViewModel viewModel)
        {
            string viewContent = await _partialViewsLoader.RenderRazorViewToString(this, "WebTemplates/_Lmi", viewModel);

            return new JsonResult(new
            {
                partialView = viewContent,
                model = viewModel
            });
        }
    }
}
