﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Qualify.Domain.Models.Identity;
using Qualify.Repository;
using Qualify.Web.Mvc.ExtensionMethods;
using Qualify.Web.Mvc.Models.Profile;
using Qualify.Web.Mvc.Services;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Controllers
{
    [Authorize]
    public class UserProfileController: Controller
    {
        private readonly ILoanWriterRepository _loanWriterRepository;

        public UserProfileController(ILoanWriterRepository loanWriterRepository)
        {
            _loanWriterRepository = loanWriterRepository;
        }

        public JsonResult Get()
        {
            LoanWriter loanWriter = HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);

            ExternalLoanWriterModel externalLoanWriterModel = loanWriter.ToExternalLoanWriterModel();

            IList<string> lockedFieldsNames = ExternalLoanWriterMapService.GetLockedFields(externalLoanWriterModel);

            UserProfileJsonForm userProfileJsonForm = new UserProfileJsonForm
            {
                ExternalLoanWriterModel = externalLoanWriterModel,
                LockedFields = lockedFieldsNames.ToArray()
            };

            return new JsonResult(userProfileJsonForm);
        }

        [HttpPost]
        public async Task<IActionResult> Update(ExternalLoanWriterModel externalLoanWriterModel)
        {
            //get current user 
            LoanWriter loanWriter = await _loanWriterRepository.GetByUniqueIdAsync(externalLoanWriterModel.UniqueID);

            if (string.IsNullOrEmpty(externalLoanWriterModel.UniqueID))
            {
                return new JsonResult(new
                {
                    success = true
                });
            }

            //map external userprofile on saved user
            ExternalLoanWriterMapService.MapExternalOnCurrent(externalLoanWriterModel, loanWriter);

            //update db 
            await _loanWriterRepository.UpdateByIdAsync(loanWriter.UniqueID, loanWriter);

            //reset claim
            await HttpContext.SignOutAsync();
            await SignInService.SignInAsync(HttpContext, loanWriter);

            return new JsonResult(new
            {
                success = true
            });
        }

        [HttpGet]
        public async Task<IActionResult> Update()
        {
            return View();
        }
    }
}
