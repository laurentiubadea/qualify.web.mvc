require.config({
    paths: {
        "jquery": "node_modules/jquery/dist/jquery.min",
        "knockout": "node_modules/knockout/build/output/knockout-latest",
        "bootstrap": "node_modules/bootstrap/dist/js/bootstrap.min",
        "expressionsEngine": "./ExpressionsEngine/ExpressionsEngine",
    },
    urlArgs: 'v=1.0.0.0',
    packages: [
        {
            name: "codemirror",
            location: "node_modules/codemirror",
            main: "lib/codemirror"
        },
        {
            name: "jsonForm",
            location: "./",
            main: "JsonForms"
        }
    ]
});