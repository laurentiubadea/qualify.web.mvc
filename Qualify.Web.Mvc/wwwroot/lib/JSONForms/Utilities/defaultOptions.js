define([], function () {
    var defaultOptions = {
        nameTitleOptions: [
            "Mr",
            "Mrs",
            "Dr",
            "Ms"
        ]
    };

    defaultOptions.counter = function (number) {
        var stringOptions = [];

        for (var i = 0; i < number; i++) {
            stringOptions.push((i + 1).toString());
        }

        return stringOptions;
    }

    return defaultOptions;
});