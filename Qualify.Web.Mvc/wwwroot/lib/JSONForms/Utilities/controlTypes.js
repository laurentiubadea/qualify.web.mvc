define([], function() {
    var controlTypes = {
        TextInputControl: "TextInputControl",
        TextareaInputControl: "TextareaInputControl",
        NumberInputControl: "NumberInputControl",
        KendoNumberInputControl: "KendoNumberInputControl",
        DropdownInputControl: "DropdownInputControl",
        DateInputControl: "DateInputControl",
        CheckboxInputControl: "CheckboxInputControl",
        RadioButtonInputControl: "RadioButtonInputControl",
        Row: "Row",
        TitleControl: "TitleControl",
        SubtitleControl: "SubtitleControl",
        ShowHideButtonControl: "ShowHideButtonControl",
        InfoControl: "InfoControl",
        TabControl: "TabControl",
        KendoDropdown: "KendoDropdown",
        LeftLabelInputControl: "LeftLabelInputControl",
        GroupControl: "GroupControl"
    };

    return controlTypes;
});