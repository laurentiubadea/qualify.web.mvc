define([], function () {
    var optionsHelpers = {};

    optionsHelpers.lastInputIdOfTheForm = null;

    optionsHelpers.format = function (options) {
        var mappedOptions = [];

        if (!options) {
            options = [];
        }

        if (!Array.isArray(options)) {
            options = [options];
        }

        for (var i = 0, len = options.length; i < len; i++) {
            if (options[i].hasOwnProperty('text') && options[i].hasOwnProperty('value')) {
                mappedOptions.push({
                    text: options[i].text,
                    value: options[i].value
                });
            } else {
                mappedOptions.push({
                    text: options[i],
                    value: options[i]
                });
            }
        }

        // mappedOptions.unshift({
        //     text: "",
        //     value: ""
        // });

        return mappedOptions;
    };

    //repeatableElement has path = Dependant and index = 0
    //property is Dependant.DateOfBirth
    optionsHelpers.recursivelyParseNestedElements = function (element, property, value, repeatableElement) {
        //if property has dots
        var propertyFirstPart = property;

        //console.log(JSON.stringify(element), property, value, repeatableElement);
        var isItemNewArray = false;
        var isArrayItemFound = false;

        if (property.indexOf(".") > 0) {
            propertyFirstPart = property.substring(0, property.indexOf("."));
            var propertyLastPart = property.replace(propertyFirstPart + ".", "");

            var newNestedObject = {};
            if (repeatableElement &&
                propertyFirstPart === repeatableElement.path) {

                //console.log("REPEATABLE DEPENDANT: ", JSON.stringify(element[propertyFirstPart]));

                if (element[propertyFirstPart]) {
                    if (element[propertyFirstPart][repeatableElement.index]) {

                        //console.log("FOUND SHIT");
                        newNestedObject = element[propertyFirstPart][repeatableElement.index];
                        isArrayItemFound = true;
                    } else {
                        isItemNewArray = true;
                    }
                } else {
                    //console.log("CREATED ARRAY");

                    element[propertyFirstPart] = [];
                    isItemNewArray = true;
                }
            } else {
                if (element[propertyFirstPart]) {
                    newNestedObject = element[propertyFirstPart];
                }
            }

            //propertyWithoutFirstPart
            value = optionsHelpers.recursivelyParseNestedElements(newNestedObject, propertyLastPart, value, repeatableElement);
            //after this iteration -> value is the nested object which was first in the array
            //{ name: "Gigel", "DateOfBirth": date }
        }

        if (isArrayItemFound)
            return element;

        if (isItemNewArray) {
            element[propertyFirstPart].push(value);
        } else {
            element[propertyFirstPart] = value;
        }

        return element;
    };

    optionsHelpers.getNestedRepeatableItemLength = function (element, property, repeatableElement) {
        var propertyFirstPart = property;
        if (property.indexOf(".") > 0) {
            propertyFirstPart = property.substring(0, property.indexOf("."));
            var propertyLastPart = property.replace(propertyFirstPart + ".", "");

            if (propertyFirstPart === repeatableElement.path &&
                element[propertyFirstPart] &&
                element[propertyFirstPart].constructor === Array) {
                return element[propertyFirstPart].length;
            }

            if (element[propertyFirstPart]) {
                return this.getNestedRepeatableItemLength(element[propertyFirstPart], propertyLastPart, repeatableElement);
            }
        }

        if (propertyFirstPart === repeatableElement.path &&
            element[propertyFirstPart] &&
            element[propertyFirstPart].constructor === Array) {
            return element[propertyFirstPart].length;
        }

        return 0;
    };

    //repeatable element -> Path and Index; Path = Dependant; index = 0, 1, 2;    property = Dependant.Name
    optionsHelpers.getNestedElementsPropertyValue = function (element, property, repeatableElement) {
        //if property has dots
        var propertyFirstPart = property;

        if (property.indexOf(".") > 0) {
            propertyFirstPart = property.substring(0, property.indexOf("."));
            var propertyLastPart = property.replace(propertyFirstPart + ".", "");

            if (repeatableElement &&
                propertyFirstPart === repeatableElement.path &&
                element[propertyFirstPart]) {

                //if (element[propertyFirstPart].constructor !== Array) {
                //    return "";
                //}

                if (element[propertyFirstPart][repeatableElement.index]) {
                    return optionsHelpers.getNestedElementsPropertyValue(element[propertyFirstPart][repeatableElement.index], propertyLastPart, repeatableElement);
                }

                return null;
                //the element[propertyFirstPart] is an array - we want the target property at an index
            }

            if (element[propertyFirstPart]) {
                return optionsHelpers.getNestedElementsPropertyValue(element[propertyFirstPart], propertyLastPart, repeatableElement);
            }
            //to implement an error case scenario - if code is here then the nested property's parent doesn't exist!!!!
            //return {
            //    element: null,
            //    value: null
            //};

            return null;
        }

        return element[propertyFirstPart];
        //return {
        //    element: element,
        //    value: element ? element[propertyFirstPart] : ""
        //};
    };

    optionsHelpers.formatReferenced = function (referencedOptions, record, uniqueId) {
        var targetEntities = record[referencedOptions.entity],
            mappedProperties = [];

        var entity = referencedOptions.entity;
        var numberOfEntities = 0;
        if (!targetEntities)
            return mappedProperties;

        //if referencedOptions.excludeSelf === true -> don't include in the referenced options list the current item;
        //current item UNIQUE ID will need to be sent as param at this function

        for (var i = 0, len = targetEntities.length; i < len; i++) {
            var currentEntity = targetEntities[i];
            if (referencedOptions.excludeSelf === true &&
                currentEntity.UniqueID === uniqueId)
                continue;

            var currentText = "";

            for (var j = 0, len2 = referencedOptions.properties.length; j < len2; j++) {
                var propertyValue = optionsHelpers.getNestedElementsPropertyValue(currentEntity, referencedOptions.properties[j]);

                //if (propertyValue.element && propertyValue.value) {
                if (propertyValue) {
                    currentText += propertyValue + " ";
                }
            }
            var textHasNoContent = currentText.replace(/ /g, '') === '';

            if (textHasNoContent) {
                numberOfEntities++;
                entity = entity.replace(/([a-z])([A-Z])/g, '$1 $2');
                currentText = entity + " " + numberOfEntities;
            }

            mappedProperties.push({
                text: currentText,
                value: currentEntity.UniqueID
            });
        }

        if (mappedProperties.length === 0) {
            mappedProperties = [];
        }

        return mappedProperties;
    };

    //BIND EVENT FOR 'TAB' BUTTON
    optionsHelpers.bindTabEvent = function ($jsonFormContainer) {
        setTimeout(function () {

            var self = this;
            var $createRecordButtonFooter = $("#create-record-button");

            if ($createRecordButtonFooter.length !== 0) {
                $createRecordButtonFooter.on("keydown", function (e) {
                    if (e.keyCode == 9) {
                        var firstControlId = $(".control-wrapper:visible:first", $jsonFormContainer).attr("id");
                        var firstInputId = $("#" + firstControlId).find("input").attr("id");
                        $("#" + firstInputId).focus();

                        return false;
                    }
                });
            } else {
                var lastChildId = $(".control-wrapper:visible:last", $jsonFormContainer).attr("id");
                var $lastChild = $("#" + lastChildId);

                self.lastInputIdOfTheForm = lastChildId;

                $lastChild.on("keydown", function (e) {
                    var controlId = $(this).attr("id");
                    if (controlId === self.lastInputIdOfTheForm) {

                        if (e.keyCode == 9) {
                            $("#closeButton").focus();

                            return false;
                        }
                    }
                });
            }
        }, 500);
    };

    return optionsHelpers;
});