define(["jsonForm/Controls/controlsFactory", "jsonForm/StructuralControls/Utilities/structuralControlsFactory", "material", "jsonForm/Utilities/optionsHelpers"], function (controlsFactory, structuralControlsFactory, mdc, optionsHelpers) {
    var createControlHelpers = {};

    createControlHelpers.setControlValue = function (params) {
        var jsonForms = params.jsonForms,
            controlType = params.jsonForms,
            controlOptions = params.controlOptions,
            repeatableElement = params.repeatableElement;

        if (!jsonForms.options.uniqueId ||
            controlType === "SubtitleControl" ||
            controlType === "Row" ||
            controlType === "GroupControl")
            return;

        currentSectionName = jsonForms.options.name;

        var targetValue = jsonForms.options.record[currentSectionName];
        if (!targetValue) {
            return;
        }

        var targetObject = targetValue.find(obj => obj.UniqueID === jsonForms.options.uniqueId);
        //found the target entity in that section

        var namePartToBeReplaced = "record.";
        if (currentSectionName) {
            namePartToBeReplaced = namePartToBeReplaced + currentSectionName + ".";
        }
        var controlName = controlOptions.name.replace(namePartToBeReplaced, "");

        controlOptions.repeatableElement = null;
        if (repeatableElement) {
            repeatableElement.path = repeatableElement.path.replace(namePartToBeReplaced, "");
            controlOptions.repeatableElement = {
                path: repeatableElement.path,
                index: repeatableElement.index
            };
        }

        controlOptions.value = optionsHelpers.getNestedElementsPropertyValue(targetObject, controlName, repeatableElement);//.value;
    };

    return createControlHelpers;
});