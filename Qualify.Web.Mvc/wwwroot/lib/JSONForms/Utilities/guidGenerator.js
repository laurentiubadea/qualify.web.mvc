define([], function () {
    var guidGenerator = {};

    guidGenerator._randomString = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };

    guidGenerator.generateGuid = function () {
        return (this._randomString() + this._randomString() + "-" + this._randomString() + "-4" + this._randomString().substr(0, 3) +
            "-" + this._randomString() + "-" + this._randomString() + this._randomString() + this._randomString()).toLowerCase();
    };

    guidGenerator.generateUidFromMask = function (mask, sectionName) {
        if ((mask === undefined) && (sectionName !== "")) {
            var randomNumber = Math.floor(Math.random() * 1000000),
                alternativeMask = sectionName + "-" + randomNumber.toString();

            return alternativeMask;
        }

        var numbers = mask.split("-")[1],
            prefix = mask.substr(0, mask.indexOf("-")),
            numbersCounter = parseInt("1" + numbers),
            resultedRandomNumber = Math.floor(Math.random() * numbersCounter),
            resultedUid = prefix + "-" + resultedRandomNumber.toString();

        return resultedUid;
    };

    return guidGenerator;
});