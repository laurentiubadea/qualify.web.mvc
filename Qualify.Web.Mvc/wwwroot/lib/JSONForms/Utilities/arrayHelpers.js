define([], function () {
    var helpers = {};

    helpers.findIndex = function (array, callback) {
        for (var i = 0, len = array.length; i < len; i++) {
            var result = callback.apply(this, [array[i]]);

            if (result) {
                return i;
            }
        }

        return -1;
    };

    helpers.remove = function (array, callback) {
        var index = this.findIndex(array, callback);

        if (index > -1 && index < array.length) {
            array.splice(index, 1);
        }
    };

    return helpers;
});