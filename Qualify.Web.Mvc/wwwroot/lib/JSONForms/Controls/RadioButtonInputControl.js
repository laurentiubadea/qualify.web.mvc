define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "jsonForm/Utilities/optionsHelpers"], function ($, Control, templateTypes, optionsHelpers) {
    function RadioButtonInputControl(options) {
        Control.call(this);
        this.options = $.extend(true, {}, RadioButtonInputControl.options, options);

        var formattedOptions = optionsHelpers.format(this.options.options);

        for (var i = 0, len = formattedOptions.length; i < len; i++) {
            formattedOptions[i]["uid"] = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        }

        this.renderCustomTemplate(this.options.$parent, templateTypes.RadioButton, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            labelFontSize: this.options.style.labelFontSize,
            name: this.options.name,
            options: formattedOptions,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        this.getValue = function () {
            var $controlContainer = this.getDomContainer();
            var checked = $controlContainer.find("input:checked").val();

            return checked;
        };

        this.setValue = function (value, isValueChangedEventAvoided) {
            var $controlContainer = this.getDomContainer();
            $controlContainer.find(":radio[value='" + value + "']").prop("checked", "true");

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        this.setValue(this.options.value, true);

        if (this.options.lastFormInput) {
            $("#" + this.inputId).on("keydown", function (e) {
                if (e.keyCode === 9) {
                    e.preventDefault();
                    $(".close-modal-button").first().focus();
                }
            });
        }

        //this.bindEvents();
    };

    RadioButtonInputControl.options = {
        $parent: $({}),
        label: "Radio input",
        name: "",
        type: "radio",
        value: "",
        lastFormInput: false,
        rules: {},
        style: {
            marginTop: "10px",
            labelFontSize: "16px"
        },
        options: [{
            text: "Default Text",
            value: "Default Value"
        }],
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return RadioButtonInputControl;
});