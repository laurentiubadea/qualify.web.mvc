define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "material", "kendo.numerictextbox.min"], function ($, Control, templateTypes, mdc) {
    function KendoNumberInputControl(options) {
        var self = this;
        Control.call(this);
        this.options = $.extend(true, {}, KendoNumberInputControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.KendoNumberInput, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            labelId: this.inputId + "-labelId",
            placeholder: this.options.placeholder,
            name: this.options.name,
            fieldType: this.options.fieldType,
            min: this.options.min,
            max: this.options.maximumValue,
            decimals: this.options.maximumDecimals,
            step: this.options.step,
            width: this.options.style.width,
            widthLabel: this.options.style.widthLabel,
            widthInput: this.options.style.widthInput,
            marginRight: this.options.style.marginRight,
            paddingRight: this.options.style.paddingRight,
            leftLabelVisible: this.options.style.additionalLeftLabel.visible,
            leftLabelText: this.options.style.additionalLeftLabel.text,
            leftLabelMargin: this.options.style.additionalLeftLabel.margin,
            leftLabelFontSize: this.options.style.additionalLeftLabel.fontSize,
            rightLabelVisible: this.options.style.additionalRightLabel.visible,
            rightLabelText: this.options.style.additionalRightLabel.text,
            rightLabelMargin: this.options.style.additionalRightLabel.margin,
            rightLabelFontSize: this.options.style.additionalRightLabel.fontSize,
            iconType: ((this.options.style.icon.leading === true) && (this.options.style.icon.trailing === false)) ? "mdc-text-field--with-leading-icon" : (((this.options.style.icon.leading === false) && (this.options.style.icon.trailing === true)) ? "mdc-text-field--with-trailing-icon" : ""),
            iconVisible: ((this.options.style.icon.leading === true) || (this.options.style.icon.trailing === true)) ? true : false,
            iconClass: this.options.style.icon.class,
            hasTooltip: this.options.tooltip.display,
            tooltipText: this.options.tooltip.text,
            tooltipPlacement: this.options.tooltip.dataPlacement,
            type: this.options.format.type,
            symbol: this.options.format.symbol,
            symbolPlacement: this.options.format.symbolPlacement,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        if (this.options.tooltip.display) {
            $("#" + this.containerId).tooltip({
                trigger: this.options.tooltip.trigger
            });
        }

        const textField = [].map.call($("#" + this.containerId), function (el) {
            mdc.textField.MDCTextField.attachTo(el);
        });

        var kendoFormat = "n2";
        var symbol = this.options.format.symbol;

        if (symbol && symbol !== "") {
            switch (symbol) {
                case "$":
                    kendoFormat = "c";
                    break;

                case "%":
                    kendoFormat = "##.00 \\%";
                    break;

                default:
                    break;
            }
        }

        $("#" + this.inputId).kendoNumericTextBox({
            format: kendoFormat,
            decimals: this.options.maximumDecimals,
            spinners: false,
            max: self.options.max || 999999999
        });

        var $numberInput = $("#" + this.inputId).data("kendoNumericTextBox");
        $numberInput.wrapper.width("100%");

        this.setValue = function (value, isValueChangedEventAvoided) {
            var kendoControl = $("#" + this.inputId).data("kendoNumericTextBox");
            kendoControl.value(value);

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        this.setValue(this.options.value, true);

        if ($numberInput.value()) {
            $("#" + this.inputId + "-labelId").addClass("mdc-floating-label--float-above");
        } else {
            $("#" + this.inputId + "-labelId").remove("mdc-floating-label--float-above");
        }
    };

    KendoNumberInputControl.options = {
        $parent: $({}),
        label: "Number input",
        name: "",
        placeholder: "",
        value: "",
        type: "kendonumberinput",
        fieldType: "tel",
        min: "-999999999",
        max: "999999999",
        step: "1",
        decimals: "",
        lastFormInput: false,
        rules: {},
        tooltip: {
            display: true,
            trigger: "hover",
            dataPlacement: "bottom",
            text: ""
        },
        format: {
            type: "",
            symbol: "",
            symbolPlacement: ""
        },
        style: {
            width: "100%",
            widthLabel: "60%",
            widthInput: "40%",
            marginRight: "0px",
            paddingRight: "",
            marginTop: "10px",
            additionalLeftLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            additionalRightLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            icon: {
                leading: false,
                trailing: false,
                class: ""
            }
        },
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return KendoNumberInputControl;
});