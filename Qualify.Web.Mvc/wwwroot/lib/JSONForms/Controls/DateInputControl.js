define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "material", "kendo.datepicker.min"], function ($, Control, templateTypes, mdc) {
    function DateInputControl(options) {
        Control.call(this);
        this.options = $.extend(true, {}, DateInputControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.DateInput, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            labelId: this.inputId + "-labelId",
            placeholder: this.options.placeholder,
            name: this.options.name,
            min: this.options.min,
            max: this.options.max,
            width: this.options.width,
            type: this.options.type,
            marginTop: this.options.marginTop,
            marginRight: this.options.style.marginRight,
            leftLabelVisible: this.options.style.additionalLeftLabel.visible,
            leftLabelText: this.options.style.additionalLeftLabel.text,
            leftLabelMargin: this.options.style.additionalLeftLabel.margin,
            leftLabelFontSize: this.options.style.additionalLeftLabel.fontSize,
            rightLabelVisible: this.options.style.additionalRightLabel.visible,
            rightLabelText: this.options.style.additionalRightLabel.text,
            rightLabelMargin: this.options.style.additionalRightLabel.margin,
            rightLabelFontSize: this.options.style.additionalRightLabel.fontSize,
            iconType: ((this.options.style.icon.leading === true) && (this.options.style.icon.trailing === false)) ? "mdc-text-field--with-leading-icon" : (((this.options.style.icon.leading === false) && (this.options.style.icon.trailing === true)) ? "mdc-text-field--with-trailing-icon" : ""),
            iconVisible: ((this.options.style.icon.leading === true) || (this.options.style.icon.trailing === true)) ? true : false,
            iconClass: this.options.style.icon.class,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        var wrapper = $("#" + this.inputId).parent().parent().attr("id");

        const textField = [].map.call($("#" + wrapper), function (el) {
            mdc.textField.MDCTextField.attachTo(el);
        });

        control = this;


        var $input = $("#" + this.inputId);
        var $containerId = $("#" + this.containerId);
        $containerId.parent().css("padding-right", this.options.style.paddingRight);

        $input.kendoDatePicker({
            culture: "en-GB",
            format: "dd/MM/yyyy",
            parseFormats: ["dd/MM/yyyy", "dd.MM.yyyy", "dd-MM-yyyy"]
        });

        this.setValue = function (value, isValueChangedEventAvoided) {
            var kendoControl = $("#" + this.inputId).data("kendoDatePicker");
            if (value && value !== '') {
                kendoControl.value(new Date(value));
            }

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        this.setValue(this.options.value, true);

        if ($input.val() !== "") {
            $("#" + this.inputId + "-labelId").addClass("mdc-floating-label--float-above");
        } else {
            $("#" + this.inputId + "-labelId").remove("mdc-floating-label--float-above");
        }

        //if (this.options.lastFormInput) {
        //    $("#" + this.inputId).on("keydown", function (e) {
        //        if (e.keyCode === 9) {
        //            e.preventDefault();
        //            $(".close-modal-button").first().focus();
        //        }
        //    });
        //}

        this.destroy = function () {
            var combobox = $("#" + this.inputId).data("kendoDatePicker");
            combobox.destroy();

            var $container = this.getDomContainer();
            $container.parent().remove();
        };

        //this.bindEvents();
    };

    DateInputControl.options = {
        $parent: $({}),
        label: "Date input",
        name: "",
        placeholder: "",
        value: "",
        min: "1995-30-08",
        max: "2019-03-01",
        type: "datepicker",
        lastFormInput: false,
        type: "date",
        rules: {},
        style: {
            width: "100%",
            marginRight: "0px",
            marginTop: "10px",
            additionalLeftLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            additionalRightLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            icon: {
                leading: false,
                trailing: false,
                class: ""
            }
        },
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return DateInputControl;
});