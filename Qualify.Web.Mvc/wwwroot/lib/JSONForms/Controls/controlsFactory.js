define(["jsonForm/Utilities/controlTypes",
    "jsonForm/Controls/CheckboxInputControl",
    "jsonForm/Controls/DateInputControl",
    "jsonForm/Controls/KendoDropdown",
    "jsonForm/Controls/LeftLabelInputControl",
    "jsonForm/Controls/NumberInputControl",
    "jsonForm/Controls/KendoNumberInputControl",
    "jsonForm/Controls/RadioButtonInputControl",
    "jsonForm/Controls/TextareaInputControl",
    "jsonForm/Controls/TextInputControl",
],
    function (controlTypes,
        CheckboxInputControl,
        DateInputControl,
        KendoDropdown,
        LeftLabelInputControl,
        NumberInputControl,
        KendoNumberInputControl,
        RadioButtonInputControl,
        ShowHideButtonControl,
        TextInputControl) {
        var controlsFactory = {};

        controlsFactory.createControl = function (controlType, controlOptions, callback) {
            if (controlType === "Row")
                return;

            switch (controlType) {
                case controlTypes.TextInputControl:
                    {
                        var control = new TextInputControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.CheckboxInputControl:
                    {
                        var control = new CheckboxInputControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.DateInputControl:
                    {
                        var control = new DateInputControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.KendoDropdown:
                    {
                        var control = new KendoDropdown(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.LeftLabelInputControl:
                    {
                        var control = new LeftLabelInputControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.NumberInputControl:
                    {
                        var control = new NumberInputControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.RadioButtonInputControl:
                    {
                        var control = new RadioButtonInputControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.ShowHideButtonControl:
                    {
                        var control = new ShowHideButtonControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.KendoNumberInputControl:
                    {
                        var control = new KendoNumberInputControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }

                default:
                    alert("UNDEFINED CONTROL TYPE");
                    break;
            }

            // if (controlType !== "Row") {
            //     var control = eval("new " + controlType + "(controlOptions)");
            //     if (control) {
            //         callback(control, controlOptions);
            //     }

            //     //require(["jsonForm/Controls/" + controlType], function (controlType) {
            //     //    var control = new controlType(controlOptions);
            //     //    if (control) {
            //     //        callback(control, controlOptions);
            //     //    }
            //     //});
            // }
            // else {
            //     console.log("FUNCTION HAS BEEN CALLED FOR ROW TYPE");
            // }
        };

        return controlsFactory;
    });