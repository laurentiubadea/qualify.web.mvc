define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "material", "kendo.numerictextbox.min"], function ($, Control, templateTypes, mdc) {
    function LeftLabelInputControl(options) {
        var self = this;
        Control.call(this);
        this.options = $.extend(true, {}, LeftLabelInputControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.LeftLabelInputControl, {
            containerId: this.containerId,
            inputId: this.inputId,
            labelWrapperId: this.inputId + "-labelWrapperId",
            label: this.options.label,
            placeholder: this.options.placeholder,
            name: this.options.name,
            fieldType: this.options.fieldType,
            typeControl: this.options.typeControl,
            min: this.options.min,
            max: this.options.maximumValue,
            decimals: this.options.maximumDecimals,
            step: this.options.step,
            width: this.options.style.width,
            widthLabel: this.options.style.widthLabel,
            widthInput: this.options.style.widthInput,
            floatingLabel: this.options.floatingLabel,
            marginRight: this.options.style.marginRight,
            paddingRight: this.options.style.paddingRight,
            leftLabelVisible: this.options.style.additionalLeftLabel.visible,
            leftLabelText: this.options.style.additionalLeftLabel.text,
            leftLabelMargin: this.options.style.additionalLeftLabel.margin,
            leftLabelFontSize: this.options.style.additionalLeftLabel.fontSize,
            rightLabelVisible: this.options.style.additionalRightLabel.visible,
            rightLabelText: this.options.style.additionalRightLabel.text,
            rightLabelMargin: this.options.style.additionalRightLabel.margin,
            rightLabelFontSize: this.options.style.additionalRightLabel.fontSize,
            iconType: ((this.options.style.icon.leading === true) && (this.options.style.icon.trailing === false)) ? "mdc-text-field--with-leading-icon" : (((this.options.style.icon.leading === false) && (this.options.style.icon.trailing === true)) ? "mdc-text-field--with-trailing-icon" : ""),
            iconVisible: ((this.options.style.icon.leading === true) || (this.options.style.icon.trailing === true)) ? true : false,
            iconClass: this.options.style.icon.class,
            hasTooltip: this.options.tooltip.display,
            tooltipText: this.options.tooltip.text,
            tooltipPlacement: this.options.tooltip.dataPlacement,
            type: this.options.format.type,
            symbol: this.options.format.symbol,
            symbolPlacement: this.options.format.symbolPlacement,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        if (this.options.tooltip.display) {
            $("#" + self.inputId + "-labelWrapperId").tooltip({
                trigger: this.options.tooltip.trigger
            });
        }

        //$("#" + this.inputId).keydown(function (event) {
        //    if (self.options.lastFormInput) {
        //        if (e.keyCode === 9) {
        //            e.preventDefault();
        //            $(".close-modal-button").first().focus();
        //        }
        //    }

        //    if (event.shiftKey == true) {
        //        event.preventDefault();
        //    }

        //    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        //        (event.keyCode >= 96 && event.keyCode <= 105) ||
        //        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        //        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        //    } else {
        //        event.preventDefault();
        //    }

        //    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        //        event.preventDefault();
        //    //if a decimal has been added, disable the "."-button
        //});

        const textField = [].map.call($("#" + this.containerId), function (el) {
            mdc.textField.MDCTextField.attachTo(el);
        });

        var kendoFormat = "n2";
        var symbol = this.options.format.symbol;

        if (symbol && symbol !== "") {
            switch (symbol) {
                case "$":
                    kendoFormat = "c";
                    break;

                case "%":
                    kendoFormat = "##.00 \\%";
                    break;

                default:
                    break;
            }
        }

        $("#" + this.inputId).kendoNumericTextBox({
            format: kendoFormat,
            decimals: this.options.maximumDecimals,
            spinners: false,
            max: self.options.max || 999999999
        });

        $("#" + this.inputId).data("kendoNumericTextBox").wrapper.width("100%");

        this.setValue = function (value, isValueChangedEventAvoided) {
            var kendoControl = $("#" + this.inputId).data("kendoNumericTextBox");
            kendoControl.value(value);

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        this.setValue(this.options.value, true);
    };

    LeftLabelInputControl.options = {
        $parent: $({}),
        label: "Number input",
        floatingLabel:"",
        name: "",
        placeholder: "",
        value: "",
        fieldType: "leftLabelNumber",
        min: "-999999999",
        typeControl: "leftLabel",
        max: "999999999",
        step: "0",
        maximumDecimals: "0",
        lastFormInput: false,
        rules: {},
        tooltip: {
            display: true,
            trigger: "hover",
            dataPlacement: "bottom",
            text: ""
        },
        format: {
            type: "",
            symbol: "",
            symbolPlacement: ""
        },
        style: {
            width: "0px",
            widthLabel: "0px",
            widthInput: "0px",
            marginRight: "0px",
            paddingRight: "0px",
            marginTop: "10px",
            additionalLeftLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            additionalRightLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            icon: {
                leading: false,
                trailing: false,
                class: ""
            }
        },
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return LeftLabelInputControl;
});