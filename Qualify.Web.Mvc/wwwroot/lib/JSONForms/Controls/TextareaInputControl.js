define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes"], function ($, Control, templateTypes) {
    function TextareaInputControl(options) {
        Control.call(this);
        this.options = $.extend(true, {}, TextareaInputControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.TextareaInput, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            placeholder: this.options.placeholder,
            name: this.options.name,
            rows: this.options.rows,
            cols: this.options.cols,
            value: this.options.value,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        this.setValue = function (value, isValueChangedEventAvoided) {
            var $control = this.getDomInput();
            $control.val(value);

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        this.setValue(this.options.value, true);
    };

    TextareaInputControl.options = {
        $parent: $({}),
        label: "Textarea input",
        name: "",
        placeholder: "textarea...",
        value: "",
        rules: {},
        rows: "5",
        cols: "100",
        style: {
            marginTop: "10px"
        },
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return TextareaInputControl;
});