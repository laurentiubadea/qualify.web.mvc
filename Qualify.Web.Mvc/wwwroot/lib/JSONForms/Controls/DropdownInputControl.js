define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "jsonForm/Utilities/optionsHelpers"], function ($, Control, templateTypes, optionsHelpers) {
    function DropdownInputControl(options) {
        Control.call(this);
        this.options = $.extend(true, {}, DropdownInputControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.DropdownInput, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            placeholder: this.options.placeholder,
            name: this.options.name,
            options: (this.options.formattedReferencedOptions !== undefined) ? this.options.formattedReferencedOptions : optionsHelpers.format(this.options.options),
            multiple: this.options.multiple,
            width: this.options.style.width,
            marginRight: this.options.style.marginRight,
            leftLabelVisible: this.options.style.additionalLeftLabel.visible,
            leftLabelText: this.options.style.additionalLeftLabel.text,
            leftLabelMargin: this.options.style.additionalLeftLabel.margin,
            leftLabelFontSize: this.options.style.additionalLeftLabel.fontSize,
            rightLabelVisible: this.options.style.additionalRightLabel.visible,
            rightLabelText: this.options.style.additionalRightLabel.text,
            rightLabelMargin: this.options.style.additionalRightLabel.margin,
            rightLabelFontSize: this.options.style.additionalRightLabel.fontSize,
            iconType: ((this.options.style.icon.leading === true) && (this.options.style.icon.trailing === false)) ? "mdc-select--with-leading-icon" : (((this.options.style.icon.leading === false) && (this.options.style.icon.trailing === true)) ? "mdc-select--with-trailing-icon" : ""),
            iconVisible: ((this.options.style.icon.leading === true) || (this.options.style.icon.trailing === true)) ? true : false,
            iconClass: this.options.style.icon.class,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        this.setValue = function (value, isValueChangedEventAvoided) {
            var $control = this.getDomInput();
            $control.val(value);

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        this.setValue(this.options.value, true);

        if (this.options.lastFormInput) {
            $("#" + this.inputId).on("keydown", function (e) {
                if (e.keyCode === 9) {
                    e.preventDefault();
                    $(".close-modal-button").first().focus();
                }
            });
        }

        //this.bindEvents();
    };

    DropdownInputControl.options = {
        $parent: $({}),
        label: "Dropdown input",
        name: "",
        placeholder: "",
        value: "",
        type: "dropdown",
        options: [{
            text: "Default Text",
            value: "Default Value"
        }],
        multiple: false,
        type: "dropdown",
        lastFormInput: false,
        rules: {},
        style: {
            width: "100%",
            marginRight: "0px",
            marginTop: "10px",
            additionalLeftLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            additionalRightLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            icon: {
                leading: false,
                trailing: false,
                class: ""
            }
        },
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return DropdownInputControl;
});