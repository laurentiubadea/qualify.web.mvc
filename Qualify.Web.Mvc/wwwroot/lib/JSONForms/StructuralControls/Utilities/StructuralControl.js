﻿define(["jquery", "knockout"], function ($, ko) {
    var _counter = 0;
    function _generateContainerId(index) {
        return "structural-control-" + index;
    };

    //function _generateInputId(index) {
    //    return "control-input-" + index;
    //};

    function StructuralControl() {
        this.containerId = _generateContainerId(_counter);
        //this.inputId = _generateInputId(_counter);
        _counter++;

        this.renderCustomTemplate = function ($container, templateName, options) {
            ko.applyBindingsToNode($container[0], { template: { name: templateName, data: options } });

            $container.find("[data-bind]").removeAttr("data-bind");
        };

        this.getDomContainer = function () {
            var containerId = this.containerId;

            return $("#" + containerId);
        };

        //this.getDomInput = function () {
        //    var inputId = this.inputId;

        //    return $("#" + inputId);
        //};

        this.destroy = function (callback) {
            var $container = this.getDomContainer();
            $container.parent().remove();

            if (callback) {
                callback();
            }
        };
    }

    return StructuralControl;
})