define(["jquery", "jsonForm/StructuralControls/Utilities/StructuralControl", "jsonForm/Utilities/templateTypes", "jsonForm/Utilities/optionsHelpers"], function ($, StructuralControl, templateTypes, optionsHelpers) {
    function TabControl(options) {
        StructuralControl.call(this);
        this.options = $.extend(true, {}, TabControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.TabControl, {
            containerId: this.containerId,
            text: this.options.text,
            name: this.options.name,
            width: this.options.style.width,
            marginLeft: this.options.style.marginLeft,
            marginTop: this.options.style.marginTop,
            fontSize: this.options.style.fontSize,
            fontWeight: this.options.style.fontWeight,
            color: this.options.style.color,
            tabs: this.options.tabs
        });

        $(".tab-button").on(
            {
                keydown: function (e) {
                    if (e.keyCode === 39) {
                        $(this).next(".tab-button").focus();

                        return false;
                    }
                    if (e.keyCode === 37) {
                        $(this).prev(".tab-button").focus();

                        return false;
                    }
                },
                click: function () {
                    optionsHelpers.bindTabEvent($("#controlsContainer"));
                }
            });
    };


    TabControl.options = {
        $parent: $({}),
        text: "TabControl",
        type: "tab",
        name: "",
        rules: {},
        tabs: [
            {
                tabText: "",
                tabControls: []
            }
        ],
        additionalRightLabelText: null,
        onValueSet: function () { }
    };

    return TabControl;
});