define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes"], function ($, Control, templateTypes) {

    //NOT USED AT THE MOMENT

    function TitleControl(options) {
        StructuralControl.call(this);
        this.options = $.extend(true, {}, TitleControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.TitleControl, {
            containerId: this.containerId,
            text: this.options.text,
            name: this.options.name,
            width: this.options.style.width,
            marginLeft: this.options.style.marginLeft,
            marginTop: this.options.style.marginTop,
            fontSize: this.options.style.fontSize,
            fontWeight: this.options.style.fontWeight
        });
    };

    TitleControl.options = {
        $parent: $({}),
        text: "Modal Title",
        name: "",
        rules: {},
        type: "title",
        style: {
            width: "100%",
            marginLeft: "0px",
            marginTop: "10px",
            fontSize: "24px",
            fontWeight: "600"
        },
        additionalRightLabelText: null,
        onValueSet: function () { }
    };

    return TitleControl;
});