define(["jquery", "jsonForm/StructuralControls/Utilities/StructuralControl", "jsonForm/Utilities/templateTypes"], function ($, StructuralControl, templateTypes) {
    function SubtitleControl(options) {
        StructuralControl.call(this);
        this.options = $.extend(true, {}, SubtitleControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.SubtitleControl, {
            containerId: this.containerId,
            text: this.options.text,
            name: this.options.name,
            width: this.options.style.width,
            marginLeft: this.options.style.marginLeft,
            marginTop: this.options.style.marginTop,
            fontSize: this.options.style.fontSize,
            fontWeight: this.options.style.fontWeight,
            color: this.options.style.color,
            hasTooltip: this.options.tooltip.display,
            tooltipText: this.options.tooltip.text,
            tooltipPlacement: this.options.tooltip.dataPlacement
        });

        if (this.options.tooltip.display) {
            $("#" + this.containerId).tooltip({
                trigger: this.options.tooltip.trigger
            });
        }
    };

    SubtitleControl.options = {
        $parent: $({}),
        text: "Modal Title",
        name: "",
        rules: {},
        type: "subtitle",
        tooltip: {
            display: true,
            trigger: "hover",
            dataPlacement: "bottom",
            text: ""
        },
        style: {
            width: "100%",
            marginLeft: "0px",
            marginTop: "30px",
            fontSize: "24px",
            fontWeight: "",
            color: "#004165"
        },
        additionalRightLabelText: null,
        onValueSet: function() {}
    };

    return SubtitleControl;
});