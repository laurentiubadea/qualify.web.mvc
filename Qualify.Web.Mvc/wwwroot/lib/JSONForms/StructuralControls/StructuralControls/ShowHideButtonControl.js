define(["jquery", "jsonForm/StructuralControls/Utilities/StructuralControl", "jsonForm/Utilities/templateTypes"], function ($, Control, templateTypes) {
    function ShowHideButtonControl(options) {
        var self = this;
        StructuralControl.call(this);
        this.options = $.extend(true, {}, ShowHideButtonControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.ShowHideButtonControl, {
            domId: this.domId,
            inputId: this.inputId,
            domClass: this.options.class,
            name: this.options.name,
            text: this.options.text,
            type: this.options.type,
            width: this.options.style.width,
            textAlign: this.options.style.buttonPosition,
            marginTop: this.options.style.marginTop
        });

        this.options.$parent.on("click", "button:first", function () {
            for (var i = 0, len = self.options.toggleVisibilityTo.length; i < len; i++) {
                var currentInputName = self.options.toggleVisibilityTo[i];
                var isHidden = $("." + currentInputName + "-wrapper").hasClass("hidden");

                if (isHidden) {
                    $("." + currentInputName + "-wrapper").removeClass("hidden");
                } else {
                    $("." + currentInputName + "-wrapper").addClass("hidden");
                }
            }

            self.options.onClick();
        });

        if (this.options.lastFormInput) {
            $("#" + this.inputId).on("keydown", function (e) {
                if (e.keyCode === 9) {
                    e.preventDefault();
                    $(".close-modal-button").first().focus();
                }
            });
        }
    };

    ShowHideButtonControl.options = {
        template: "ButtonControl-template",
        name: "",
        type: "button",
        text: "button",
        lastFormInput: false,
        rules: {},
        style: {
            width: "100%",
            marginTop: "10px",
            buttonPosition: "left"
        },
        toggleVisibilityTo: [],
        $parent: $({}),
        onDestroy: function () { },
        onClick: function () { }
    };

    return ShowHideButtonControl;
});