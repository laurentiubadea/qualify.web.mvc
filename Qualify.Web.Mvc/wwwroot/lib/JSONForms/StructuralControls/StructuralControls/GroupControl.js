define(["jquery", "jsonForm/StructuralControls/Utilities/StructuralControl", "jsonForm/Utilities/templateTypes"], function ($, StructuralControl, templateTypes) {
    function GroupControl(options) {
        var self = this;
        StructuralControl.call(this);
        this.options = $.extend(true, {}, GroupControl.options, options);

        //no need for an extra container
        //this.renderCustomTemplate(this.options.$parent, templateTypes.GroupControl, {
        //    containerId: this.containerId,
        //    controls: this.controls
        //});
    };

    GroupControl.options = {
        template: "GroupControl-template",
        controls: [],
        $parent: $({})
    };

    return GroupControl;
});