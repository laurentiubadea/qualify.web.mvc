define(["jquery", "material", "jsonForm/Utilities/guidGenerator", "jsonForm/Controls/controlsFactory", "jsonForm/StructuralControls/Utilities/structuralControlsFactory", "jsonForm/Utilities/arrayHelpers", "jsonForm/Utilities/createControlHelpers", "jsonForm/Utilities/optionsHelpers", "expressionsEngine"],
    function ($, mdc, guidGenerator, controlsFactory, structuralControlsFactory, arrayHelpers, createControlHelpers, optionsHelpers, ExpressionsEngine) {
        function JsonForms(options) {
            this.resultData = {};
            this._controls = [];
            this._containers = [];
            this._toRepeatControls = [];

            this.options = $.extend(true, {}, JsonForms.options, options);

            if (!options.$container) {
                console.error("$container of the form is undefined.");
                return;

            } else if (!options.controls) {
                console.warn("No controls defined for this plugin.");
            }

            this.expressionsEngine = new ExpressionsEngine({
                recordData: this.options.record
            });

            this.createControls({
                onCompleteCallback: function () {
                    this.options.$container.show();

                    this.options.onFormCreated.apply(this, []);

                    for (var i = 0; i < this._controls.length; i++) {
                        this._bindControlEvents(this._controls[i]);
                    }

                    //self.collectFormErrors();
                    this.evaluateFormRules();
                    optionsHelpers.bindTabEvent(this.options.$container);

                    this._bindExternalRules();
                }
            });
        }

        JsonForms.prototype._getData = function () {
            var self = this;
            return new Promise((resolve, reject) => {
                if (self.options.getDataUrl === "") {
                    resolve("no url");
                    return;
                }

                $.ajax({
                    url: self.options.getDataUrl,
                    type: 'GET',
                    success: function (data) {
                        resolve(data)
                    },
                    error: function (error) {
                        reject(error)
                    },
                });
            });
        };

        //ON STRUCTURAL CREATED DEFAULTS
        JsonForms.prototype._onStructuralControlCreatedDefaults = function (controlType, controlOptions, control) {
            switch (controlType) {
                case "Row":
                    if (!controlOptions.rules ||
                        !controlOptions.rules.repeatability) {

                        _createRowContent.apply(this, [controlOptions])
                        //if there is no repeatability, we just create the rows
                    } else {
                        this._repeatabilityEventBinding(controlOptions, controlType);
                    }
                    break;

                case "GroupControl":
                    break;

                //for subtitle and tab
                default:
                    this._materializeControls(control);
                    break;
            }

            this._visibilityEventBinding(controlOptions);
        };

        //ON CONTROL CREATED DEFAULTS
        JsonForms.prototype._onControlCreatedDefaults = function (control) {
            this._controls.push(control);
            this._materializeControls(control);
            this.options.onControlCreated(control);

            control.bindEvents();
        };

        //bind all events for a control
        JsonForms.prototype._bindControlEvents = function (control) {
            this._visibilityEventBinding(control.options);
            this._valueCalculationBinding(control);
            this._availabilityEventBinding(control);
            this._restrictionEventBinding(control);
            this._calculatedRestrictionEventBinding(control);

            this._requiredEventBinding(control);

            if (control.options.dataSource === "External") {
                this._externalDataSourceEventBinding(control);
            }
        }

        JsonForms.prototype.createControls = function (params) {
            var self = this;
            var onCompleteCallback = params.onCompleteCallback || function () { };

            for (var i = 0, len = this.options.controls.length; i < len; i++) {
                var control = this.options.controls[i];

                this._addControl({
                    controlType: control.type,
                    controlOptions: control.options
                });
            }

            onCompleteCallback.apply(this, []);
        };  

        JsonForms.prototype._addControl = function (params) {
            var controlType = params.controlType,
                controlOptions = params.controlOptions,
                $container = params.$container,
                marginTop = params.marginTop,
                repeatableElement = params.repeatableElement,
                onControlCreatedCallback = params.onControlCreatedCallback;

            controlOptions.$parent = this._createControlWrapper({
                controlOptions: controlOptions,
                $container: $container,
                marginTop: marginTop
            });

            this.bindControlValueChangeEvent(controlOptions);

            if (controlOptions.referencedOptions !== undefined) {
                controlOptions.formattedReferencedOptions = optionsHelpers.formatReferenced(
                    controlOptions.referencedOptions,
                    this.options.record,
                    this.options.uniqueId);
            }

            switch (controlType) {
                case "GroupControl":
                    this.createStructuralControl(controlType, controlOptions, _onCreateGroupControlCallback);
                    break;

                case "Row":
                    this._onStructuralControlCreatedDefaults(controlType, controlOptions);
                    break;

                case "TabControl":
                    this.createStructuralControl(controlType, controlOptions, _onCreateTabControlCallback);
                    break;

                case "SubtitleControl":
                    this.createStructuralControl(controlType, controlOptions);
                    break;

                default:
                    createControlHelpers.setControlValue({
                        jsonForms: this,
                        controlType: controlType,
                        controlOptions: controlOptions,
                        repeatableElement: repeatableElement
                    });

                    this.createControl(controlType, controlOptions, onControlCreatedCallback);
                    break;
            }
        }

        var _onCreateGroupControlCallback = function (control, controlOptions) {
            for (var i = 0; i < controlOptions.controls.length; i++) {
                var currentControl = controlOptions.controls[i];

                this._addControl({
                    controlType: currentControl.type,
                    controlOptions: currentControl.options,
                    $container: controlOptions.$parent
                });
            }
        }

        var _createRowContent = function (controlOptions, repeatableItemIndex) {
            var self = this;
            var $innerDivContainer = $("<div/>");
            controlOptions.$parent.append($innerDivContainer);
            controlOptions.$parent.addClass("control-row");

            var isInsideRepeatableRow = controlOptions.rules && controlOptions.rules.repeatability;
            var repeatableElement = null;

            if (isInsideRepeatableRow) {
                repeatableElement = {
                    path: controlOptions.rules.repeatability.element,
                    index: repeatableItemIndex
                };
            }

            for (var i = 0, len = controlOptions.controls.length; i < len; i++) {
                var currentControl = controlOptions.controls[i];

                self._addControl({
                    controlType: currentControl.type,
                    controlOptions: currentControl.options,
                    $container: $innerDivContainer,
                    marginTop: "0px",
                    repeatableElement: repeatableElement,
                    onControlCreatedCallback: function (innerControl, innerControlOptions) {
                        innerControl.options.parentRules = controlOptions.rules;

                        //bind control events needs to appear because the row is crreated differently than the others and it's children fields don't get their events binded
                        this._bindControlEvents(innerControl);

                        $controlContainer = $("#" + innerControl.containerId);

                        $controlContainer.css("width", "100%");

                        if (innerControl.options.typeControl !== undefined) {
                            if (innerControl.options.typeControl === "leftLabel") {
                                $controlContainer.css("width", innerControl.options.style.widthInput);
                            }
                        }

                        if ($controlContainer.parents().attr("id")) {
                            var controlParentId = $controlContainer.parents().attr("id");
                            $("#" + controlParentId).css("float", "left");

                        } else {
                            var controlParentId = $controlContainer.parents().parents().attr("id");
                            $("#" + controlParentId).css("float", "left");
                        }
                    }
                });
            }
        };

        var _onCreateTabControlCallback = function (control, controlOptions) {
            var self = this;
            //this._onStructuralControlCreatedDefaults(control.type, controlOptions, control);

            var tabs = new mdc.tabBar.MDCTabBar(document.querySelector('#' + control.options.$parent.attr("id")));
            tabs.activateTab(0);

            for (var i = 0, len = controlOptions.tabs.length; i < len; i++) {
                var currentTab = controlOptions.tabs[i];
                var currentTabControls = currentTab.controls;

                var tabContainer = $("<div></div>", { id: 'tab-content-' + (i + 1) }).addClass("content");

                if (i === 0) {
                    tabContainer.addClass("content--active");
                }

                controlOptions.$parent.append(tabContainer);

                for (var j = 0, ctrlLen = currentTabControls.length; j < ctrlLen; j++) {
                    var currentControl = currentTabControls[j];

                    self._addControl({
                        controlType: currentControl.type,
                        controlOptions: currentControl.options,
                        $container: $("#tab-content-" + (i + 1)),
                        onControlCreatedCallback: function (innerControl, innerControlOptions) {
                            innerControl.options.parentRules = controlOptions.rules;
                        }
                    });

                    continue;
                }
            }
        };

        //FACTORY FOR CONTROLS
        JsonForms.prototype.createControl = function (controlType, controlOptions, callback) {
            var self = this;
            callback = callback || function () { };

            if (controlOptions.referencedOptions !== undefined) {
                controlOptions.formattedReferencedOptions = optionsHelpers.formatReferenced(
                    controlOptions.referencedOptions,
                    this.options.record,
                    this.options.uniqueId);
            }

            controlsFactory.createControl(
                controlType,
                controlOptions,
                function (control, controlOptions) {
                    self._onControlCreatedDefaults(control);
                    callback.apply(self, [control, controlOptions]);
                });
        };

        //FACTORY FOR STRUCTURAL-CONTROLS 
        JsonForms.prototype.createStructuralControl = function (controlType, controlOptions, callback) {
            var self = this;
            callback = callback || function () { };

            structuralControlsFactory.createStructuralControl(
                controlType,
                controlOptions,
                function (control, controlOptions) {
                    self._onStructuralControlCreatedDefaults(controlType, controlOptions, control);
                    callback.apply(self, [control, controlOptions]);
                });
        };

        JsonForms.prototype._createControlWrapper = function (params) {
            var controlOptions = params.controlOptions,
                marginTop = params.marginTop,
                $container = params.$container || this.options.$container;

            var width = "100%";
            if (controlOptions.style &&
                controlOptions.style.width) {
                width = controlOptions.style.width;
            }

            if (controlOptions.style &&
                controlOptions.style.marginTop) {
                marginTop = controlOptions.style.marginTop;
            }

            var wrapperId = "wrapper-" + guidGenerator.generateGuid();

            var $controlWrapper = $("<div></div>", { id: wrapperId }).addClass("control-wrapper");
            $controlWrapper.css("width", width);
            $controlWrapper.css("margin-top", marginTop);
            $controlWrapper.addClass(controlOptions.name + "-wrapper");

            $container.append($controlWrapper);

            return $controlWrapper;
        };

        JsonForms.prototype._materializeControls = function (control) {
            //sterge
            //if (control.options.format !== undefined) {
            //    if (control.options.format.type !== "") {
            //        control.getDomInput().number(true, control.options.decimals);
            //    }
            //}

            if (control.options.type === "dropdown") {
                const select = [].map.call(control.getDomInput(), function (el) {
                    mdc.select.MDCSelect.attachTo(el);
                });
            }

            if (control.options.type === "text" || control.options.type === "number") {

                var wrapper = control.getDomInput().parent().parent().attr("id");

                const textField = [].map.call($("#" + wrapper), function (el) {
                    mdc.textField.MDCTextField.attachTo(el);
                });
            }

            var $containerId = $("#" + control.containerId);
            if (control.options.type === "number") {
                $containerId.parent().css("width", control.options.style.width);
                $containerId.parent().css("padding-right", control.options.style.paddingRight);
            }

            if (control.options.type === "text") {
                $containerId.parent().css("padding-right", control.options.style.paddingRight);
            }

            //sterge
            //if (control.options.fieldType === "decimal") {
            //    if (control.options.fieldType !== undefined) {
            //        const textField = [].map.call($("#" + control.containerId), function (el) {
            //            mdc.textField.MDCTextField.attachTo(el);
            //        });
            //    }
            //}

            const ripple = [].map.call($(".mdc-line-ripple"), function (el) {
                mdc.ripple.MDCRipple.attachTo(el);
            });
            const ripple2 = [].map.call($(".mdc-button"), function (el) {
                mdc.ripple.MDCRipple.attachTo(el);
            });
            const floating = [].map.call($(".mdc-floating-label"), function (el) {
                mdc.floatingLabel.MDCFloatingLabel.attachTo(el);
            });
            const icon = [].map.call($(".mdc-text-field-icon"), function (el) {
                mdc.textFieldIcon.MDCTextFieldIcon.attachTo(el);
            });

            if (control.options.type === "tab") {
                var tabBar = new mdc.tabBar.MDCTabBar(document.querySelector('#' + control.options.$parent.attr("id")));


                tabBar.listen('MDCTabBar:activated', function (event) {
                    var contentEls = document.querySelectorAll('.content');

                    // Hide currently-active content
                    $('.content--active').removeClass('content--active');
                    // Show content for newly-activated tab
                    if (contentEls[event.detail.index] !== undefined) {
                        contentEls[event.detail.index].classList.add('content--active');
                    }
                });
            }
        };

        //TO BE DELETED - I COULDN"T FIND A SOLUTION TO DO THIS MORE EFFICIENTLY
        JsonForms.prototype.applyEventMappingBinding = function () {
            var self = this;

            for (var i = 0; i < this.eventsDictionary.length; i++) {
                var itemEventMapping = this.eventsDictionary[i];
                var triggererControl = this.getControlByName(itemEventMapping.triggerControlName);

                var $expressionTrigererControl = $(itemEventMapping.triggerItemIdentifier, this.$container);
                if (!$expressionTrigererControl.length || !triggererControl)
                    continue;

                var onChangeFunction = function () {
                    for (var j = 0; j < itemEventMapping.events.length; j++) {
                        var event = itemEventMapping.events[j];

                        event.callback.apply(self, [{
                            control: event.control,
                            controlOptions: event.controlOptions
                        }]);
                    }
                }

                switch (triggererControl.options.type) {
                    case "kendonumberinput":
                    case "leftLabel":
                        $expressionTrigererControl.data("kendoNumericTextBox").bind("change", onChangeFunction);
                        break;

                    case "kendodropdown":
                        $expressionTrigererControl.data("kendoComboBox").bind("change", onChangeFunction);
                        break;

                    case "date":
                        $expressionTrigererControl.data("kendoDatePicker").bind("change", onChangeFunction);
                        break;

                    default:
                        $expressionTrigererControl.on("change", onChangeFunction);
                }
            }
        };

        JsonForms.prototype._bindDependantFieldsToChangeEvent = function (params) {
            var self = this;

            var control = params.control,
                controlOptions = params.controlOptions,
                callback = params.callback,
                ruleType = params.ruleType,
                triggerFieldsForThisExpression = params.triggerFieldsForThisExpression,
                callbackAdditionalParams = params.callbackAdditionalParams;

            if (!triggerFieldsForThisExpression) {
                triggerFieldsForThisExpression = ExpressionsEngine.Utils.getTriggerFields({
                    expression: controlOptions.rules[ruleType].condition
                });
            }

            for (var i = 0; i < triggerFieldsForThisExpression.length; i++) {
                var triggererControl = this.getControlByName(triggerFieldsForThisExpression[i]);

                var itemIdentifier = "[data-name='" + triggerFieldsForThisExpression[i] + "']";
                if (controlOptions.repeatableElement) {
                    itemIdentifier = itemIdentifier + "[data-repeatableindex='" + controlOptions.repeatableElement.index + "']";
                }
                var $expressionTrigererControl = $(itemIdentifier, self.$container);

                if (!$expressionTrigererControl.length || !triggererControl)
                    continue;

                var triggerChangeEvent = function () {
                    callback.apply(self, [{
                        control: control,
                        controlOptions: controlOptions,
                        callbackAdditionalParams: callbackAdditionalParams
                    }]);
                };

                switch (triggererControl.options.type) {
                    case "kendonumberinput":
                    case "leftLabel":
                        $expressionTrigererControl.data("kendoNumericTextBox").bind("change", triggerChangeEvent);
                        break;

                    case "kendodropdown":
                        $expressionTrigererControl.data("kendoComboBox").bind("change", triggerChangeEvent);
                        break;

                    case "date":
                        $expressionTrigererControl.data("kendoDatePicker").bind("change", triggerChangeEvent);
                        break;

                    default:
                        $expressionTrigererControl.on("change", triggerChangeEvent);
                }
            }
        }

        //EXPRESSIONS

        JsonForms.prototype._valueCalculationBinding = function (control) {
            var controlOptions = control.options;

            if (!controlOptions.rules ||
                !controlOptions.rules.value) {
                return;
            }

            var self = this;

            switch (controlOptions.rules.value.type) {
                case "On Condition":
                    this._bindDependantFieldsToChangeEvent({
                        callback: self._evaluateValue,
                        control: control,
                        controlOptions: controlOptions,
                        ruleType: "value"
                    });
                    break;

                case "Default":
                    if (this.options.isDataCreated) {
                        this._evaluateValue({
                            control: control,
                            controlOptions: controlOptions
                        });
                    }
                    break;
            }

            //to be discussed when these expressions run -> for calculated they should not run again
            //this._evaluateValue(control, controlOptions);
        };

        JsonForms.prototype._evaluateValue = function (params) {
            var control = params.control;
            var resultValue = null;

            switch (control.options.rules.value.type) {
                case "On Condition":
                case "Default":
                    var targetObject = this.resultData || {};

                    //if controlOptions.rules.value.condition is array -> evaluate each expression in array and the result is the && between all expressions

                    resultValue = this.expressionsEngine.evaluateExpression({
                        expression: control.options.rules.value.condition,
                        expressionContextData: targetObject,
                        contextName: this.options.name,
                        repeatableElement: control.options.repeatableElement
                    });
                    break;
            }

            if (resultValue === null)
                return;

            control.setValue(resultValue);
            this._evaluateRequired(params);
        };

        JsonForms.prototype._visibilityEventBinding = function (controlOptions) {
            if (!controlOptions.rules ||
                !controlOptions.rules.visibility) {
                return;
            }

            var self = this;

            //bind event changes
            if (controlOptions.rules.visibility.type === "On Condition") {
                //var targetValue = this.options.record[this.options.name];
                //var targetObject = targetValue.find(obj => obj.UniqueID === this.options.uniqueId);
                //targetObject is the context for my expression

                this._bindDependantFieldsToChangeEvent({
                    callback: self._evaluateVisibility,
                    controlOptions: controlOptions,
                    ruleType: "visibility"
                });
            }

            this._evaluateVisibility({
                controlOptions: controlOptions
            });
        };

        JsonForms.prototype._evaluateVisibility = function (params) {
            var self = this;
            var controlOptions = params.controlOptions;

            var isVisible = true;

            switch (controlOptions.rules.visibility.type) {
                case "On Condition":
                    var targetObject = this.resultData || {};

                    isVisible = this.expressionsEngine.evaluateExpression({
                        expression: controlOptions.rules.visibility.condition,
                        expressionContextData: targetObject,
                        contextName: this.options.name,
                        repeatableElement: controlOptions.repeatableElement
                    });
                    break;

                case "Hidden":
                    isVisible = false;
                    break;
            }

            if (isVisible) {
                controlOptions.$parent.removeClass("hidden");
            } else {
                controlOptions.$parent.addClass("hidden");
            }

            controlOptions.isVisible = isVisible;
        };

        JsonForms.prototype._availabilityEventBinding = function (control) {
            var self = this;
            var controlOptions = control.options;

            if (this.options.lockedFields.indexOf(controlOptions.name) > -1) {
                control.setIsEnabled(false);
                return;
            }

            if (!controlOptions.rules ||
                !controlOptions.rules.availability) {
                return;
            }

            if (controlOptions.rules.availability.type === "On Condition") {
                this._bindDependantFieldsToChangeEvent({
                    callback: self._evaluateAvailability,
                    control: control,
                    controlOptions: controlOptions,
                    ruleType: "availability"
                });
            }

            this._evaluateAvailability({
                control: control,
                controlOptions: controlOptions
            });
        }

        JsonForms.prototype._evaluateAvailability = function (params) {
            var control = params.control,
                controlOptions = control.options;

            var isEnabled = true;

            switch (controlOptions.rules.availability.type) {
                case "On Condition":
                    var targetObject = this.resultData || {};

                    isEnabled = this.expressionsEngine.evaluateExpression({
                        expression: controlOptions.rules.availability.condition,
                        expressionContextData: targetObject,
                        contextName: this.options.name,
                        repeatableElement: controlOptions.repeatableElement
                    });
                    break;

                case "Disabled":
                    isEnabled = false;
                    break;
            }

            control.setIsEnabled(isEnabled);
            this._evaluateRequired(params);
        }

        JsonForms.prototype._restrictionEventBinding = function (control) {
            var self = this;
            var controlOptions = control.options;

            if (!controlOptions.rules ||
                !controlOptions.rules.restriction) {
                return;
            }

            if (controlOptions.rules.restriction.type === "On Condition") {
                this._bindDependantFieldsToChangeEvent({
                    callback: self._evaluateRestriction,
                    control: control,
                    controlOptions: controlOptions,
                    ruleType: "restriction"
                });
            }

            this._evaluateRestriction({
                control: control,
                controlOptions: controlOptions
            });
        };

        JsonForms.prototype._evaluateRestriction = function (params) {
            var control = params.control,
                controlOptions = control.options;

            var isValid = true;

            controlOptions.$parent.removeClass("restriction-validation-error");
            $(".restriction-error-message", controlOptions.$parent).empty();
            //controlOptions.$parent.removeAttr("data-validationError");

            if (control.getDomInput().is(":disabled")) {
                return;
            }

            switch (controlOptions.rules.restriction.type) {
                case "On Condition":
                    var targetObject = this.resultData || {};

                    isValid = this.expressionsEngine.evaluateExpression({
                        expression: controlOptions.rules.restriction.condition,
                        expressionContextData: targetObject,
                        contextName: this.options.name,
                        repeatableElement: controlOptions.repeatableElement
                    });
                    break;
            }

            if (isValid === false) {
                controlOptions.$parent.addClass("restriction-validation-error");
                $(".restriction-error-message", controlOptions.$parent).text(controlOptions.rules.restriction.errorMessage);

                //TOOLTIP
                this._setToolTipOnErrorMessage("restriction-error-message", controlOptions.$parent, controlOptions.rules.restriction.errorMessage);
                //controlOptions.$parent.attr("data-validationError", controlOptions.rules.restriction.errorMessage);
            }

            //this.collectFormErrors();
        };

        JsonForms.prototype._calculatedRestrictionEventBinding = function (control) {
            var self = this;
            var controlOptions = control.options;

            if (!controlOptions.rules ||
                !controlOptions.rules.calculatedRestrictions) {
                return;
            }

            var triggerFieldsForAllExpressions = [];
            for (var i = 0; i < controlOptions.rules.calculatedRestrictions.conditions.length; i++) {
                var triggerFieldsForExpression = ExpressionsEngine.Utils.getTriggerFields({
                    expression: controlOptions.rules.calculatedRestrictions.conditions[i].condition
                });

                for (var j = 0; j < triggerFieldsForExpression.length; j++) {
                    if (triggerFieldsForAllExpressions.indexOf(triggerFieldsForExpression[j]) > -1)
                        continue;

                    triggerFieldsForAllExpressions.push(triggerFieldsForExpression[j]);
                }
            }

            this._bindDependantFieldsToChangeEvent({
                callback: self._evaluateCalculatedRestriction,
                control: control,
                controlOptions: controlOptions,
                triggerFieldsForThisExpression: triggerFieldsForAllExpressions
                //no need to specify rule type
            });

            this._evaluateCalculatedRestriction({
                control: control,
                controlOptions: controlOptions
            });
        };

        JsonForms.prototype._evaluateCalculatedRestriction = function (params) {
            var control = params.control,
                controlOptions = control.options;

            var isValid = true;
            controlOptions.$parent.removeClass("calculated-validation-error");
            $(".calculated-error-message", controlOptions.$parent).empty();
            //controlOptions.$parent.removeAttr("data-validationError");

            if (control.getDomInput().is(":disabled")) {
                return;
            }

            var isValid = true;
            var errorMessage = "";

            for (var i = 0; i < controlOptions.rules.calculatedRestrictions.conditions.length; i++) {
                var expressionObject = controlOptions.rules.calculatedRestrictions.conditions[i];
                var targetObject = this.resultData || {};

                isValid = this.expressionsEngine.evaluateExpression({
                    expression: expressionObject.condition,
                    expressionContextData: targetObject,
                    contextName: this.options.name,
                    repeatableElement: controlOptions.repeatableElement
                });

                if (isValid === false) {
                    errorMessage = expressionObject.errorMessage;
                    break;
                }
            }

            if (isValid === false) {
                controlOptions.$parent.addClass("calculated-validation-error");
                $(".calculated-error-message", controlOptions.$parent).text(errorMessage);
                //controlOptions.$parent.attr("data-validationError", controlOptions.rules.restriction.errorMessage);

                //TOOLTIP
                this._setToolTipOnErrorMessage("calculated-error-message", controlOptions.$parent, errorMessage);
            }
        };

        JsonForms.prototype._requiredEventBinding = function (control) {
            var self = this;
            var controlOptions = control.options;

            if (controlOptions.rules &&
                controlOptions.rules.required &&
                controlOptions.rules.required.type === "Optional") {
                return;
            }

            switch (control.options.type) {
                case "subtitle":
                case "row":
                    break;

                default:
                    if (controlOptions.rules &&
                        controlOptions.rules.required &&
                        controlOptions.rules.required.type === "On Condition") {
                        this._bindDependantFieldsToChangeEvent({
                            callback: self._evaluateRequired,
                            control: control,
                            controlOptions: controlOptions,
                            ruleType: "required"
                        });
                    }

                    if (control.options.type === "radio") {
                        $("#" + control.containerId).on("click change focusout focus", function () {
                            self._evaluateRequired({
                                control: control,
                                controlOptions: controlOptions
                            });
                        })
                    } else {
                        control.getDomInput().on("change focusout focus", function () {
                            self._evaluateRequired({
                                control: control,
                                controlOptions: controlOptions
                            });
                        });

                        control.getDomInput().on("change", function () {
                            optionsHelpers.bindTabEvent(self.options.$container);
                        });
                    }

                    this._evaluateRequired({
                        control: control,
                        controlOptions: controlOptions
                    });
                    break;
            }
        };

        JsonForms.prototype._evaluateRequired = function (params) {
            var isRequired = true;
            var control = params.control,
                controlOptions = params.controlOptions;

            controlOptions.$parent.removeClass("required-validation-error");

            if (control.getDomInput().is(":disabled")) {
                return;
            }

            //this.collectFormErrors();

            if (controlOptions.rules &&
                controlOptions.rules.required &&
                controlOptions.rules.required.type === "On Condition") {
                var targetObject = this.resultData || {};

                isRequired = this.expressionsEngine.evaluateExpression({
                    expression: controlOptions.rules.required.condition,
                    expressionContextData: targetObject,
                    contextName: this.options.name,
                    repeatableElement: controlOptions.repeatableElement
                });
            }

            if (isRequired === false)
                return;

            var value = control.getValue();
            if (value === "" || !value) {
                controlOptions.$parent.addClass("required-validation-error");

                //TOOLTIP
                var errorMessage = controlOptions.label + " is required";
                this._setToolTipOnErrorMessage("required-error-message", controlOptions.$parent, errorMessage);
            }
        };

        JsonForms.prototype._repeatabilityEventBinding = function (controlOptions, controlType) {
            var self = this;

            if (!controlOptions.rules ||
                !controlOptions.rules.repeatability) {
                return;
            }

            if (controlOptions.rules.repeatability.type === "On Condition") {
                this._bindDependantFieldsToChangeEvent({
                    callback: self._evaluateRepeatability,
                    controlOptions: controlOptions,
                    ruleType: "repeatability"
                });
            }

            this._evaluateRepeatability({
                controlOptions: controlOptions,
                controlType: controlType
            });
        };

        JsonForms.prototype._evaluateRepeatability = function (params) {
            var self = this;
            var controlOptions = params.controlOptions,
                controlType = params.controlType;

            var repeatNumberOfTimes = 1;

            switch (controlOptions.rules.repeatability.type) {
                case "On Condition":
                    var targetObject = this.resultData || {};

                    repeatNumberOfTimes = this.expressionsEngine.evaluateExpression({
                        expression: controlOptions.rules.repeatability.condition,
                        expressionContextData: targetObject,
                        contextName: this.options.name
                    });
                    break;
            }

            if (!repeatNumberOfTimes)
                repeatNumberOfTimes = 0;

            for (var i = 0; i < controlOptions.controls.length; i++) {
                //function here goes through all controls names "name" and removes
                var controlName = controlOptions.controls[i].options.name;
                var controlToRemove = this.getControlByName(controlName);

                while (controlToRemove) {
                    this.removeControlByName(controlName);
                    controlToRemove.destroy();

                    //remove next controls with the same name
                    controlToRemove = this.getControlByName(controlName);
                }
            }

            controlOptions.$parent.empty();

            for (var i = 0; i < repeatNumberOfTimes; i++) {
                _createRowContent.apply(this, [controlOptions, i]);
            }

            //clean the controls, but the data is still there unfortunately - how to fix it
            var namePartToBeReplaced = "record.";
            if (this.options.name) {
                namePartToBeReplaced = namePartToBeReplaced + this.options.name + ".";
            }

            var repeatableItemProperty = controlOptions.rules.repeatability.element.replace(namePartToBeReplaced, "");

            if (this.resultData &&
                this.resultData[repeatableItemProperty]) {
                var currentRepeatableLength = this.resultData[repeatableItemProperty].length;

                if (currentRepeatableLength > repeatNumberOfTimes) {
                    this.resultData[repeatableItemProperty].splice(-1, currentRepeatableLength - repeatNumberOfTimes);
                } else if (currentRepeatableLength < repeatNumberOfTimes) {
                    for (var i = 0; i < repeatNumberOfTimes - currentRepeatableLength; i++) {
                        this.resultData[repeatableItemProperty].splice(-1, 0, {});
                    }
                }

                //trigger a save change if the array was changed
                setTimeout(function () {
                    self.evaluateFormRules();
                    self.options.onSaveResultData.apply(self, [self.resultData, self.options.name]);
                }, 100);
            }
        };

        JsonForms.prototype._externalDataSourceEventBinding = function (control) {
            if (control.options.dataSource !== "External") {
                return;
            }

            if (!control.options.externalDataSourceOptions ||
                !control.options.externalDataSourceOptions.isPrimaryTrigger) {
                return;
            }

            var self = this;            

            var updateDependantsData = function (value, areDependantsValuesCleared) {
                var fieldDataSource = self.options.externalDataSource[control.options.externalDataSourceOptions.dataSourceName];

                var dataSourceJson = fieldDataSource[control.options.externalDataSourceOptions.collectionName].find(function (item) {
                    return item[control.options.externalDataSourceOptions.fieldName] === value;
                });

                var dependantControlsMappingList = control.options.externalDataSourceOptions.mapping;

                for (var i = 0; i < dependantControlsMappingList.length; i++) {
                    var dependantControl = this.getControlByName(dependantControlsMappingList[i].name);

                    //dependantControlsMappingList[i].name
                    //dependantControlsMappingList[i].dataSourceName

                    switch (dependantControl.options.type) {
                        case "kendodropdown":
                            var kendoControl = $("#" + dependantControl.inputId).data("kendoComboBox");

                            var newDataSource = [];
                            if (dataSourceJson) {
                                newDataSource = optionsHelpers.format(dataSourceJson[dependantControlsMappingList[i].dataSourceName]);
                            }

                            kendoControl.setDataSource(newDataSource);

                            if (areDependantsValuesCleared) {
                                kendoControl.value("");
                                kendoControl.trigger("change");
                            }
                            break;

                        default:
                            break;
                    }
                }
            };

            switch (control.options.type) {
                case "kendodropdown":
                    var kendoControl = $("#" + control.inputId).data("kendoComboBox");

                    kendoControl.bind("change", function () {
                        var value = this.value();

                        updateDependantsData.apply(self, [value, true]);
                    });

                    updateDependantsData.apply(self, [kendoControl.value()]);
                    break;

                default:
                    break;
            }
        };

        JsonForms.prototype.evaluateFormRules = function () {
            var isValueValid = true;
            var errorMessages = [];

            if (this.options.rules &&
                this.options.rules.restriction) {
                switch (this.options.rules.restriction.type) {
                    case "On Condition":
                        for (var i = 0; i < this.options.rules.restriction.conditions.length; i++) {
                            var conditionObject = this.options.rules.restriction.conditions[i];

                            var isConditionValid = this.expressionsEngine.evaluateExpression({
                                expression: conditionObject.condition,
                                expressionContextData: this.resultData,
                                contextName: this.options.name
                            });

                            if (isConditionValid === false) {
                                errorMessages.push(conditionObject.errorMessage);
                                isValueValid = false;
                            }
                        }
                }
            }

            var areControlsValid =
                $(".required-validation-error:visible", this.options.$container).length === 0 &&
                $(".calculated-validation-error:visible", this.options.$container).length === 0 &&
                $(".restriction-validation-error:visible", this.options.$container).length === 0;

            if (areControlsValid === false) {
                errorMessages.push("There are invalid controls in this form");
            }

            this.options.onFormValidationComplete.apply(this, [{
                isValid: isValueValid && areControlsValid,
                errorMessages: errorMessages
            }]);
        };

        //END EXPRESSIONS

        JsonForms.prototype.destroy = function () {
            for (var i = 0, len = this._controls.length; i < len; i++) {
                this._controls[i].destroy();
            }

            this._controls = [];
            this.options.onFormDestroyed(this);
        };

        JsonForms.prototype.getControlByName = function (controlName) {
            if ((controlName === "") || (controlName === undefined)) {
                return null;
            }
            var control = this._controls.find(c => c.options.name === controlName);

            if ((control !== null) && (control !== undefined)) {
                return control;
            }

            return null;
        };

        JsonForms.prototype.removeControlByName = function (controlName) {
            if ((controlName === "") || (controlName === undefined)) {
                return null;
            }

            //var control = this._controls.find(c => c.options.name === controlName);
            var index = this._controls.findIndex(x => x.options.name === controlName);

            if (index > -1) {
                this._controls.splice(index, 1);
            }
        };

        JsonForms.prototype.getControlByContainerId = function (containerId) {
            var index = arrayHelpers.findIndex(this._controls, function (item) {
                return item.containerId === containerId;
            });

            if (index === -1) {
                console.error(containerId + " not found in the controls.");

                return;
            }

            return this._controls[index];
        }

        JsonForms.prototype.removeControl = function (containerId) {
            var self = this;
            var control = this.getControlByContainerId(containerId);

            arrayHelpers.remove(this._controls, function (item) {
                return item.containerId === control.containerId;
            });

            control.destroy();
        };

        JsonForms.prototype.bindControlValueChangeEvent = function (controlOptions) {
            var self = this;

            controlOptions.onValueChanged = function (value, isValueChangedEventAvoided) {
                var namePartToBeReplaced = "record.";
                if (self.options.name) {
                    namePartToBeReplaced = namePartToBeReplaced + self.options.name + ".";
                }
                var controlName = this.options.name.replace(namePartToBeReplaced, "");

                optionsHelpers.recursivelyParseNestedElements(self.resultData, controlName, value, this.options.repeatableElement);

                //SET FLOATING LABEL CLASS-->it doesn't work with apply
                this.setFloatingLabelClass(value);

                //callback for save resultData
                if (isValueChangedEventAvoided)
                    return;

                self.options.onSaveResultData.apply(self, [self.resultData, self.options.name]);
                setTimeout(function () {
                    self.evaluateFormRules();
                });
            };
        }

        //TOOLTIP
        JsonForms.prototype._setToolTipOnErrorMessage = function (classMessage, context, errorMessage) {
            var $tooltipContainer = $("." + classMessage, context);

            $tooltipContainer.attr("data-original-title", errorMessage);
            $tooltipContainer.tooltip({
                trigger: "hover"
            });
        };

        JsonForms.prototype._bindExternalRules = function () {
            if (!this.options.externalRules) {
                return;
            }

            for (var externalRulesType in this.options.externalRules) {
                var externalRulesCollection = this.options.externalRules[externalRulesType];

                this._bindExternalRulesCollection(externalRulesCollection);
            }
        }

        JsonForms.prototype._bindExternalRulesCollection = function (externalRulesCollection) {
            var mappingDictionary = {};
            for (var i = 0; i < externalRulesCollection.mapping.length; i++) {
                mappingDictionary[externalRulesCollection.mapping[i].dataSourceName] = externalRulesCollection.mapping[i].name;
            }

            for (var j = 0; j < externalRulesCollection.ControlRules.length; j++) {
                var ruleItem = externalRulesCollection.ControlRules[j];

                for (var controlDataSourceName in ruleItem) {
                    var controlName = mappingDictionary[controlDataSourceName];
                    var control = this.getControlByName(controlName);

                    var triggerFieldsForAllExpressions = [];
                    for (var k = 0; k < ruleItem[controlDataSourceName].rules.value.conditions.length; k++) {
                        var conditionElement = ruleItem[controlDataSourceName].rules.value.conditions[k];

                        for (var dictionaryControlName in mappingDictionary) {
                            while (conditionElement.condition.indexOf(dictionaryControlName) > -1) {
                                conditionElement.condition =
                                    conditionElement.condition.replace(dictionaryControlName, "[#" + mappingDictionary[dictionaryControlName] + "#]");
                            }
                        }

                        var triggerFieldsForExpression = ExpressionsEngine.Utils.getTriggerFields({
                            expression: conditionElement.condition
                        });

                        for (var l = 0; l < triggerFieldsForExpression.length; l++) {
                            if (triggerFieldsForAllExpressions.indexOf(triggerFieldsForExpression[l]) > -1)
                                continue;

                            triggerFieldsForAllExpressions.push(triggerFieldsForExpression[l]);
                        }
                    }

                    this._bindDependantFieldsToChangeEvent({
                        callback: this._evaluateExternalRuleValue,
                        control: control,
                        controlOptions: control.options,
                        triggerFieldsForThisExpression: triggerFieldsForAllExpressions,
                        callbackAdditionalParams: {
                            conditions: ruleItem[controlDataSourceName].rules.value.conditions
                        }
                    });

                    this._evaluateExternalRuleValue({
                        control: control,
                        callbackAdditionalParams: {
                            conditions: ruleItem[controlDataSourceName].rules.value.conditions
                        }
                    });
                    //ruleItem[controlDataSourceName].rules.value.conditions
                }
            }
        }

        JsonForms.prototype._evaluateExternalRuleValue = function (params) {
            var control = params.control,
                conditions = params.callbackAdditionalParams.conditions;
            var resultValue = null;

            var targetObject = this.resultData || {};

            //if controlOptions.rules.value.condition is array -> evaluate each expression in array and the result is the && between all expressions

            for (var i = 0; i < conditions.length; i++) {
                resultValue = this.expressionsEngine.evaluateExpression({
                    expression: conditions[i].condition,
                    expressionContextData: targetObject,
                    contextName: this.options.name,
                    repeatableElement: control.options.repeatableElement
                });

                if (resultValue)
                    break;
            }

            if (resultValue === null)
                return;

            control.setValue(resultValue);
        };

        JsonForms.options = {
            $container: $({}),
            name: "",
            controls: {},
            lockedFields: [],
            getDataUrl: "",
            onFormCreated: function () { },
            onFormValidationComplete: function () { },
            onSaveResultData: function () { },
            onControlCreated: function () { },
            onFormDestroyed: function () { },
            controlCallbacks: {
                onValueSet: function () { },
                onControlDelete: function () { }
            },
            isDataCreated: false,
            externalDataSource: {}
        };

        return JsonForms;
    });