﻿define(["jquery", "knockout", "bootstrap"], function ($, ko) {
    function SlideModal(options) {

        this.options = $.extend(true, {}, SlideModal.defaultOptions, options);
        this.template = this.options.template;
        this.$container = this.options.$container;
        this.$modalContainer = this.options.$modalContainer;
        this.modalId = this.options.title.replace(" ", "");
        this.title = this.options.title;
        this.mainButtonText = this.options.mainButtonText;
        this.mainButtonHandler = this.options.mainButtonHandler;
        this.secondaryButtonText = this.options.secondaryButtonText;
        this.secondaryButtonHandler = this.options.secondaryButtonHandler;
        this.displaySecondaryButton = this.options.displaySecondaryButton;
        this.onSetContent = this.options.onSetContent;
        this.bodyHeight = this.options.footerDisplayed ? "small-height" : "large-height";
        this.footerDisplayed = this.options.footerDisplayed;
        this.onDestroyContent = this.options.onDestroyContent;

        this.buildBody();
    };

    SlideModal.prototype.buildBody = function () {
        var self = this;

        var secondaryButtonHandler = function () {
            self.options.secondaryButtonHandler.apply(self, []);
            self.destroy();
        }

        this.viewModel = {
            modalId: this.modalId,
            title: this.title,
            footerDisplayed: this.footerDisplayed,
            bodyHeight: this.bodyHeight,
            mainButtonText: ko.observable(this.mainButtonText),
            mainButtonHandler: this.mainButtonHandler,
            secondaryButtonHandler: secondaryButtonHandler,
            secondaryButtonText: ko.observable(this.options.secondaryButtonText),
            displaySecondaryButton: ko.observable(this.options.displaySecondaryButton),
            lightboxRgba: ko.observable(this.options.lightbox)
        };
        ko.applyBindingsToNode(self.$container[0], {
            template: {
                name: self.template,
                data: self.viewModel,
                bodyHeight: self.bodyHeight
            }
        });

        $("#closeButton", this.$container).on("click", function () {
            self.destroy();
        });
    };

    SlideModal.prototype.showModal = function () {
        var self = this;

        var $modal = $("#" + this.modalId);

        var $facebookLoading = $("#facebook-loading-animation-template").html();

        $modal.addClass(self.options.position);

        $modal.find(".modal-body").prepend($facebookLoading);

        $modal.modal({
            show: true,
            backdrop: self.options.backdrop,
            keyboard: false
        });

        $modal.on("shown.bs.modal", function () {
            self.onSetContent.apply(self, []);
        })
    };

    SlideModal.prototype.destroy = function () {
        this.onDestroyContent.apply(this, []);
        $(".modal-backdrop").remove(".show");
        $(".slide-modal-container").remove();
    };

    SlideModal.Position = {
        Right: "right",
        Center: "center",
        Left: "left",
        Botton: "bottom"
    };

    SlideModal.Lightbox = {
        Black: 'rgba(0,0,0,0.5)',
        White: 'rgba(255,255,255,0.7)'
    };

    SlideModal.defaultOptions = {
        $modalContainer: $({}),
        $container: $({}),
        footerDisplayed: false,
        $closeButton: $({}),
        template: "",
        modalId: "",
        title: "",
        onDestroyContent: function () { },
        onSetContent: function () { },
        position: SlideModal.Position.Right,
        secondaryButtonText: "Close",
        secondaryButtonHandler: function () { },
        displaySecondaryButton: true,
        lightboxRgba: 'rgba(0,0,0,0)',
        backdrop: false
    };

    return SlideModal;
});