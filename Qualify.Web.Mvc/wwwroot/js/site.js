﻿// Define global code in here
'use strict';
String.prototype.replaceAll = function (str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
};

define(["jquery"], function ($) {
    Object.defineProperty(String.prototype, "currency", {
        value: function currency() {
            if (isNaN(this)) {
                return this;
            } else {
                return "$" + parseFloat(this).toLocaleString('en-US', {
                    style: "decimal",
                    maximumFractionDigits: 2,
                    minimumFractionDigits: 2
                });
            }
        },
        writable: false,
        configurable: false
    });
    Object.defineProperty(String.prototype, "percentage", {
        value: function percentage() {
            var value = parseInt(this) / 100;

            return value.toLocaleString(undefined, { style: "percent" });
        },
        writable: false,
        configurable: false
    });

    function documentOnMousedown(e) {
        var container = $("#user-profile-collapse");
        if (!container.is(e.target) && container.has(e.target).length === 0 && !$(e.target).hasClass("user-details-button")) {
            $("#user-profile-collapse").removeClass("show");
        }
    }
    $(document).on("mousedown", documentOnMousedown);
});