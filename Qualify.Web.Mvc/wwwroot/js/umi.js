﻿var testVariableGlobal = null;
define(["jquery", "js/Plugins/ServiceListsGenerator", "js/main", "js/Plugins/LocalStorageRepository", "js/Plugins/ContentConfigurator", "js/Plugins/JsonStructureS3Handler"],
    function ($, ServiceListsGenerator, main, LocalStorageRepository, ContentConfigurator, JsonStructureS3Handler) {
        var jsonStructureS3Handler = new JsonStructureS3Handler({
            keyName: "Qualify-Resources-JSON-Forms/UMI/UMIStructureV",
            localStorageKey: "UMIStructureV",
            jsonVersionsStructure: "UMIStructure"
        }),
            contentConfigurator = new ContentConfigurator();

        var isLocalJsonTesting = false;

        function MainInit(viewModel) {
            var recordData = $.parseJSON(viewModel.dataJson);

            $(".downloadPackage").attr("href", $(".downloadPackage").attr("href") + "?recordId=" + recordData.UniqueID);

            $(".downloadPreparedFiles").attr("href", $(".downloadPreparedFiles").attr("href") + "?recordId=" + recordData.UniqueID);

            if (viewModel.isUmiSubmittedAtLeastOnce) {
                $(".downloadPreparedFiles").show();
            }
            if (viewModel.isUmiCalculationFinished) {
                umiResult(recordData);
            } else {
                //if this flag is true -> the JSON is loaded from static local resources - for testing purposes
                if (isLocalJsonTesting) {
                    umiLocalJSON(
                        recordData,
                        function (recordData, structuralJson) {
                            umi(recordData, structuralJson);
                        });
                }
                else {
                    jsonStructureS3Handler.GetJsonVersions(function (structuralJson) {
                        umi(recordData, structuralJson);
                    });
                }
            }
        }

        function umi(recordData, structuralJson) {
            testVariableGlobal = new ServiceListsGenerator({
                record: recordData,
                structuralJson: structuralJson,
                $container: $(".qualifyContainer"),
                submitUrl: "/Umi/SubmitRecord",
                onServiceListGenerated: function () {
                    contentConfigurator.hideMainLoader();
                    contentConfigurator.hideKendoLoader(function () {
                        contentConfigurator.enableSidebar();
                    });
                },
                submitSuccessCallback: function () {
                    DestroyScript();
                    main.RenderUmiPage.apply(this, [recordData.UniqueID]);
                }
            });
        }

        function umiLocalJSON(recordData, callback) {
            $.ajax({
                url: "/ServiceJsonFiles/UMIStructureV53.json",
                type: "GET",
                complete: function (data, status) {
                    if (status === "success") {
                        callback(recordData, data.responseJSON);
                    }
                    else {
                        console.log("Error on getting JSON from server: ", data);
                    }
                }
            });
        }

        function umiResult(recordData) {
            contentConfigurator.hideMainLoader();
            contentConfigurator.hideKendoLoader(function () {
                contentConfigurator.enableSidebar();
            });

            $("body").on("click", ".deleteUmiButton", function () {
                contentConfigurator.showKendoLoader();
                contentConfigurator.disableSidebar();

                $.ajax({
                    url: "/Umi/Delete",
                    type: "POST",
                    data: {
                        recordId: recordData.UniqueID
                    },
                    complete: function (data, status) {
                        if (status === "success") {
                            var responseJSON = data.responseJSON;
                            console.log("DATA SUBMITTED");
                            var updatedRecord = $.parseJSON(responseJSON.record);

                            console.log('record', updatedRecord);

                            var localStorageRepository = new LocalStorageRepository();
                            localStorageRepository.updateStorageRecord(updatedRecord.UniqueID, updatedRecord);

                            DestroyScript();

                            main.RenderUmiPage.apply(this, [recordData.UniqueID]);

                        }
                        else {
                            contentConfigurator.hideKendoLoader(function () {
                                contentConfigurator.enableSidebar();
                            });

                            console.log(data, status);
                            console.error("SUBMIT FAILED");
                        }
                    }
                });
            });
        }

        function DestroyScript() {
            if (testVariableGlobal) {
                testVariableGlobal.destroy();
                testVariableGlobal = null;
            }

            $("body").off("click", ".deleteUmiButton");
            $(".sidebar li").off("click", DestroyScript);
        }

        return {
            MainInit: MainInit,
            DestroyScript: DestroyScript
        };
    });