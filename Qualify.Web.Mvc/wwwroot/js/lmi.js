﻿define(["jquery", "js/Plugins/ServiceListsGenerator", "js/main", "js/Plugins/LocalStorageRepository", "js/Plugins/ContentConfigurator", "js/Plugins/JsonStructureS3Handler"],
    function ($, ServiceListsGenerator, main, LocalStorageRepository, ContentConfigurator, JsonStructureS3Handler) {
        var jsonStructureS3Handler = new JsonStructureS3Handler({
            keyName: "Qualify-Resources-JSON-Forms/LMI/LMIStructureV",
            localStorageKey: "LMIStructureV",
            jsonVersionsStructure: "LMIStructure"
        }),
            contentConfigurator = new ContentConfigurator();

        function MainInit(viewModel) {

            var recordData = $.parseJSON(viewModel.dataJson);

            $(".downloadPackage").attr("href", $(".downloadPackage").attr("href") + "?recordId=" + recordData.UniqueID);

            $(".downloadPreparedFiles").attr("href", $(".downloadPreparedFiles").attr("href") + "?recordId=" + recordData.UniqueID);

            if (viewModel.isCalculationFinished) {
                lmiResult(recordData);
            } else {
                jsonStructureS3Handler.GetJsonVersions(function (structuralJson) {
                    lmi(recordData, structuralJson);
                });
            }
        }

        function lmi(recordData, structuralJson) {
            testVariableGlobal = new ServiceListsGenerator({
                record: recordData,
                structuralJson: structuralJson,
                $container: $(".qualifyContainer"),
                submitUrl: "/Lmi/SubmitRecord",
                onServiceListGenerated: function () {
                    contentConfigurator.hideMainLoader();
                    contentConfigurator.hideKendoLoader(function () {
                        contentConfigurator.enableSidebar();
                    });
                },
                submitSuccessCallback: function () {
                    DestroyScript();
                    main.RenderLmiPage.apply(this, [recordData.UniqueID]);
                }
            });
        }

        function lmiResult(recordData) {
            contentConfigurator.hideMainLoader();
            contentConfigurator.hideKendoLoader(function () {
                contentConfigurator.enableSidebar();
            });

            $("body").on("click", ".deleteLmiButton", function () {
                contentConfigurator.showKendoLoader();
                contentConfigurator.disableSidebar();

                $.ajax({
                    url: "/Lmi/Delete",
                    type: "POST",
                    data: {
                        recordId: recordData.UniqueID
                    },
                    complete: function (data, status) {
                        if (status === "success") {
                            var responseJSON = data.responseJSON;
                            console.log("DATA SUBMITTED");
                            var updatedRecord = $.parseJSON(responseJSON.record);

                            var localStorageRepository = new LocalStorageRepository();
                            localStorageRepository.updateStorageRecord(updatedRecord.UniqueID, updatedRecord);

                            DestroyScript();

                            main.RenderLmiPage.apply(this, [recordData.UniqueID]);

                        }
                        else {
                            contentConfigurator.hideKendoLoader(function () {
                                contentConfigurator.enableSidebar();
                            });

                            console.log(data, status);
                            console.error("SUBMIT FAILED");
                        }
                    }
                });
            });
        }

        function DestroyScript() {
            console.log("destroy lmi script");

            if (testVariableGlobal) {
                testVariableGlobal.destroy();
                testVariableGlobal = null;
            }

            $("body").off("click", ".deleteLmiButton");
            $(".sidebar button").off("click", DestroyScript);
        }

        return {
            MainInit: MainInit,
            DestroyScript: DestroyScript
        };
    });