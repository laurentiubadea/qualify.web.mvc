﻿define([], function () {

    function LocalStorageRepository() {
    }

    LocalStorageRepository.prototype.updateStorageRecord = function (recordId, newRecordData) {
        var storageId = this.getRecordStorageId(recordId);
        localStorage.setItem(storageId, JSON.stringify(newRecordData));
    };


    LocalStorageRepository.prototype.getRecordStorageId = function (recordId) {
        var storageId = "record_" + recordId;
        return storageId;
    };

    return LocalStorageRepository;
   
});