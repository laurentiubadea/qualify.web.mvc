﻿'use strict';
define(["jquery"], function ($) {
    function ContentConfigurator(params) {

    }

    ContentConfigurator.prototype.showMainLoader = function () {
        $("#loader-wrapper").show();
    }

    ContentConfigurator.prototype.hideMainLoader = function () {
        $("#loader-wrapper").fadeOut();
    }

    ContentConfigurator.prototype.showKendoLoader = function (callback) {
        callback = callback || function () { };

        $(".k-loading-mask").fadeIn(callback);
    }

    ContentConfigurator.prototype.hideKendoLoader = function (callback) {
        callback = callback || function () { };

        $(".k-loading-mask").fadeOut(callback);
    }

    ContentConfigurator.prototype.enableSidebar = function () {
        $(".sidebar button").prop("disabled", false);
    }

    ContentConfigurator.prototype.disableSidebar = function () {
        $(".sidebar button").prop("disabled", true);
    }

    ContentConfigurator.prototype.selectSidebarItem = function (listItem) {
        $(listItem).removeClass("roboto-light").addClass("selected roboto-medium");
    }

    ContentConfigurator.prototype.deselectSidebarItems = function () {
        $(".sidebar li.available.selected").removeClass("roboto-medium selected").addClass("roboto-light");
    }

    ContentConfigurator.prototype.appendContent = function (partialView) {
        $(".rendered-content-container").append(partialView);
    }

    ContentConfigurator.prototype.deleteContent = function () {
        $(".rendered-content-container *").remove();
    }

    return ContentConfigurator;
});