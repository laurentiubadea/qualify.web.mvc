﻿define(["jquery"], function ($) {
    var MessageDisplay = function () {

    };

    MessageDisplay.DisplayAfter = function ($beforeElement, message, css) {
        var $div = $("<div>", {
            class: css,
            attr: {
                "role": "alert"
            },
            text: message,
            click: function () {
                $div.hide('slow', function () { $div.remove(); });
            }
        });

        $beforeElement.after($div);
    };

    return MessageDisplay;
});