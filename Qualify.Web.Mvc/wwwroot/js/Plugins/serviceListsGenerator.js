﻿'use strict';
define(["jquery", "knockout", "jsonForm", "slideModal", "expressionsEngine", "jsonForm/Utilities/guidGenerator", "js/Plugins/messageDisplay", "jsonForm/Utilities/optionsHelpers", "js/Plugins/ContentConfigurator", "knockout-mapping"], function ($, ko, JsonForm, SlideModal, ExpressionsEngine, guidGenerator, MessageDisplay, optionsHelpers, ContentConfigurator) {
    var contentConfigurator = new ContentConfigurator();

    function ServiceListsGenerator(params) {
        this.areFormResourcesLoaded = false;
        this.recordId = params.record.UniqueID;
        this.$container = params.$container;
        this.sectionsStructure = {};
        this.sectionsValidationCollection = {};
        this.eventListenersFunctions = {};
        this.onGeneratorLoaded = params.onGeneratorLoaded || function () { };
        this.submitUrl = params.submitUrl;
        this.onServiceListGenerated = params.onServiceListGenerated || function () { };
        this.submitSuccessCallback = params.submitSuccessCallback || function () { };
        this.sectionDependenciesDictionary = {};

        this.expressionsEngine = new ExpressionsEngine({ recordData: null });

        //get storage record from your current storage if exists
        var storageId = this.getRecordStorageId();
        if (localStorage.hasOwnProperty(storageId)) {
            params.record = this.getStorageRecord();
            this.expressionsEngine.updateRecordData(params.record);
        }
        else {
            this.updateStorageRecord(params.record);
        }

        bindData.apply(this, [params.structuralJson, params.record]);

        this.toggleSaveButton();
    }

    ServiceListsGenerator.prototype.setDataFlag = function (value) {
        localStorage.setItem(this.getRecordStorageFlagId(), value);
    };

    ServiceListsGenerator.prototype.toggleSaveButton = function () {
        var $saveRecordButton = $(".saveRecordButton");
        var storageFlagId = this.getRecordStorageFlagId();

        var isDirty = localStorage.getItem(storageFlagId) === "true";
        //storageFlagValue if true -> toggle enable
        if (isDirty) {
            $saveRecordButton.removeAttr("disabled");
        }
        else {
            $saveRecordButton.attr("disabled", "disabled");
        }
    };

    ServiceListsGenerator.prototype.getRecordStorageId = function () {
        var storageId = "record_" + this.recordId;
        return storageId;
    };

    ServiceListsGenerator.prototype.getRecordStorageFlagId = function () {
        var recordStorageId = this.getRecordStorageId();
        var storageId = recordStorageId + "_isRecordDirty";
        return storageId;
    };

    ServiceListsGenerator.prototype.getRecordStorageErrorsId = function () {
        var recordStorageId = this.getRecordStorageId();
        var storageId = recordStorageId + "_globalErrors";
        return storageId;
    };

    ServiceListsGenerator.prototype.updateStorageRecord = function (newRecordData) {
        var storageId = this.getRecordStorageId();

        localStorage.setItem(storageId, JSON.stringify(newRecordData));

        this.expressionsEngine.updateRecordData(newRecordData);
    };

    ServiceListsGenerator.prototype.updateStorageRecordErrors = function (newRecordErrors) {
        var storageId = this.getRecordStorageErrorsId();

        localStorage.setItem(storageId, JSON.stringify(newRecordErrors));
    };

    ServiceListsGenerator.prototype.getStorageRecord = function () {
        var storageId = this.getRecordStorageId();

        var recordData = JSON.parse(localStorage.getItem(storageId));
        return recordData;
    };

    ServiceListsGenerator.prototype.getStorageRecordErrors = function () {
        var storageId = this.getRecordStorageErrorsId();

        var recordDataErrors = JSON.parse(localStorage.getItem(storageId));
        return recordDataErrors;
    };

    var bindData = function (serviceData, recordData) {
        var self = this;
        this.sectionsStructure = serviceData.sections;
        this.rules = serviceData.rules;

        this.viewModel = {
            sections: ko.observableArray([]),
            getPropertyValue: function (sectionData, propertyName) {
                var properties = propertyName.split(".");

                var propertyValue = properties.reduce(function (prev, curr) {
                    return prev ? prev[curr] : null;
                }, sectionData || self);

                return typeof propertyValue === "function" ? propertyValue() : propertyValue;
            },
            toDataTypeString: function (value, dataType) {
                if (!value)
                    return "";

                return value.toString()[dataType]();
            },
            isVisible: function (sectionTemplate, contextData, item) {
                if (!item.rules ||
                    !item.rules.visibility) {
                    return true;
                }

                var contextName = sectionTemplate.name;
                var visibilityExpression = item.rules.visibility.condition;

                var params = {
                    contextName: contextName,
                    expression: visibilityExpression,
                    expressionContextData: contextData
                };
                return self.expressionsEngine.evaluateExpression(params);
            },
            getValue: function (sectionTemplate, contextData, item) {
                var value = null;

                if (!item.name) {
                    if (!item.rules ||
                        !item.rules.value) {
                        return '-';
                    }

                    var contextName = sectionTemplate.name;

                    var params = {
                        contextName: contextName,
                        expression: item.rules.value.condition,
                        expressionContextData: contextData
                    };
                    value = self.expressionsEngine.evaluateExpression(params);

                    if (item.dataType) {
                        return toDataTypeString(value, item.dataType);
                    }
                    return value;
                }

                if (item.name.constructor === Array) {
                    var values = [];

                    $.each(item.name, function () {
                        var propertyName = this;
                        values.push(getPropertyValue(contextData, propertyName, item.dataType));
                    });

                    value = values.join(" ");
                    if (value.trim() === "") {
                        return "-";
                    }
                    return value;
                }

                value = getPropertyValue(contextData, item.name, item.dataType, item.referencedOptions);

                if (!value && value !== 0) {
                    return "-";
                }

                return value;

                function getPropertyValue(data, propertyName, dataType, referencedOptions) {
                    var properties = propertyName.split("."),
                        propertyValue = properties.reduce(function (prev, curr) {
                            return prev ? prev[curr] : null;
                        }, data || self);

                    value = typeof propertyValue === "function" ? propertyValue() : propertyValue;

                    //if ref options -> guaranteed that value is an UniqueID
                    if (value && value !== "" && referencedOptions) {
                        //returns all the referenced items  - text: Household, value: HLD-sdfsdf
                        var referencedItems = optionsHelpers.formatReferenced(referencedOptions, self.getStorageRecord());

                        if (!referencedItems) {
                            return null;
                        }

                        var referenceResult = referencedItems.find(p => p.value === value);

                        if (referenceResult) {
                            value = referenceResult.text;
                        } else {
                            return null;
                        }
                    }

                    if (dataType) {
                        return toDataTypeString(value, dataType);
                    }
                    return value;
                }

                function toDataTypeString(value, dataType) {
                    if (!value)
                        return "";

                    return value.toString()[dataType]();
                }
            },
            onAccordionLoaded: function () {
                self.onGeneratorLoaded();
            },
            isValid: ko.observable(true),
            errorMessages: ko.observableArray([])
        };

        var sectionsList = [];

        $.each(this.sectionsStructure, function () {
            ExpressionsEngine.Utils.generateRestrictionRulesForMany(this.form.controls);

            ExpressionsEngine.Utils.getGlobalDependenciesDictionary({
                controls: this.form.controls,
                sectionName: this.name,
                resultDictionary: self.sectionDependenciesDictionary
            });

            ExpressionsEngine.Utils.getGlobalDependenciesDictionaryForRules({
                rules: this.rules,
                sectionName: this.name,
                resultDictionary: self.sectionDependenciesDictionary
            });

            ExpressionsEngine.Utils.getGlobalDependenciesDictionaryForRules({
                rules: this.form.rules,
                sectionName: this.name,
                resultDictionary: self.sectionDependenciesDictionary
            });

            var sectionDataAndStructure = {
                name: this.name,
                isValid: ko.observable(true),
                errorMessages: ko.observableArray([]),
                uniqueIDMask: this.uniqueIDMask,
                template: this,
                data: recordData[this.name] || []
            };

            sectionsList.push(sectionDataAndStructure);
        });

        for (var i = 0; i < sectionsList.length; i++) {
            sectionsList[i].data = ko.observable(sectionsList[i].data);

            this.validateSection({
                section: sectionsList[i],
                reevaluateDataValidation: true,
                preventReevaluateDependants: true
            });
        }
        this.viewModel.sections(sectionsList);

        ko.applyBindings(self.viewModel, $(".rendered-content-container")[0]);

        $.each($('[data-toggle="tooltip"]'), function () {
            var trigger = $(this).attr("tooltip-trigger");

            $(this).tooltip({
                trigger: trigger
            });
        });

        self.onServiceListGenerated();

        //Event listeners
        (function () {
            $(".saveRecordButton").on("click", function () {
                if ($(this).is(":disabled"))
                    return;

                self.saveRecord();
            });

            $(".submitButton").on("click", function () {
                if ($(this).is(":disabled"))
                    return;

                self.submit();
            });

            self.$container.on('hide.bs.collapse', '.collapse', function () {
                var $arrow = $(this).parent().find(".collapseTrigger");

                $arrow.removeClass("active");
            });

            self.$container.on('show.bs.collapse', '.collapse', function () {
                var $arrow = $(this).parent().find(".collapseTrigger");

                $arrow.addClass("active");
            });

            self.$container.on("click", ".addButton", function () {
                var $self = $(this);

                self.addNewItem({
                    name: $self.attr("data-section"),
                    idMask: $self.attr("data-idmask")
                });
            });

            self.$container.on("click", ".qualifyRow > table", function () {
                var $row = $(this).parent(),
                    uniqueId = $row.attr("data-id"),
                    sectionName = $row.attr("data-section");

                self.unselectAllItems(true);
                self.selectItem(uniqueId);

                self.openForm({
                    sectionName: sectionName,
                    uniqueId: uniqueId
                });
            });

            $(".body-content").on("click", ".clone-button", function () {
                var selectedItems = self.getSelectedItems();

                if (selectedItems.length > 1)
                    return;

                self.cloneItem(selectedItems[0]);

                // Uncheck select all and disable delete button
                $.each($(".selectAllCheckbox:checked"), function () {
                    $(this).prop("checked", false);
                });
                $(".main-delete-button").addClass("disabled");
                $(".main-clone-button").addClass("disabled");
            });

            $(".body-content").on("click", ".delete-button", function () {
                var selectedItems = self.getSelectedItems();

                for (var i = 0; i < selectedItems.length; i++) {
                    self.updateJsonItem(selectedItems[i]);
                    self.updateGridItem(selectedItems[i]);
                }

                // Uncheck select all and disable delete button
                $.each($(".selectAllCheckbox:checked"), function () {
                    $(this).prop("checked", false);
                });

                $(".main-delete-button").addClass("disabled");
                $(".main-clone-button").addClass("disabled");
            });

            self.$container.on("change", ".selectAllCheckbox", function () {
                var $selectAllCheckbox = $(this);
                var isChecked = $selectAllCheckbox.is(":checked");
                var sectionName = $selectAllCheckbox.attr("data-section");

                if (isChecked) {
                    self.selectSectionItems(sectionName, true);
                }
                else {
                    self.unselectSectionItems(sectionName, true);
                }
            });

            self.$container.on("click", ".qualifyRow .k-checkbox", function () {
                var isChecked = $(this).is(":checked");
                var id = $(this).attr("id");

                if (isChecked) {
                    self.selectItem(id, true);
                }
                else {
                    self.unselectItem(id, true);
                }

                var $cardElement = $(this).closest(".card");

                self.toogleSelectAll($cardElement);
            });

            // list (individual-more-info) listeners
            self.$container.on("shown.bs.dropdown", ".dropdown", function () {
                self.unselectAllItems(true);

                var rowId = $(this).parents(".qualifyRow").attr("data-id");
                self.selectItem(rowId);
            });

            $(".body-content").on("mouseleave", ".dropdown-menu.show", function () {
                if ($(".dropdown-toggle[aria-expanded='true']").is(":hover")) {
                    return;
                }

                $(this).parent().dropdown('toggle');
            });

            $(".body-content").on("mouseleave", ".dropdown-toggle[aria-expanded='true']", function () {
                var hoveredToElement = $(":hover").last();
                var $dropdownMenu = $(this).next();

                if ($dropdownMenu.is(hoveredToElement) || $dropdownMenu.has(hoveredToElement).length > 0) {
                    return;
                }

                $(this).dropdown('toggle');
            });

            // Listener is not 100% good (need a recheck)
            function serviceListDocumentOnMousedown(e) {
                var $modal = $(".modal-content");
                var $collapse = $(".collapse");

                if (!self.slideModal) {
                    var $accordionDropdownItem = $(".dropdown-item", ".qualifyRow");
                    var $accordionMoreInfoButton = $(".qualify-more-info-button", ".qualifyRow");
                    var $accordionMoreInfoIcon = $(".fa-ellipsis-v", ".qualifyRow");


                    if ($accordionDropdownItem.is(e.target) || $accordionMoreInfoButton.is(e.target) || $accordionMoreInfoIcon.is(e.target) || $modal.has(e.target).length > 0)
                        return;

                    $.each($(".qualifyRow"), function () {
                        var $row = $(this);

                        if ($row.hasClass("selected-row") && $row.find("input.k-checkbox:not(:checked)").length > 0) {
                            self.unselectItem($row.attr("data-id"));
                        }
                    });
                } else {
                    var dropdownEmptyContainer = $(".k-nodata");

                    if ($modal.has(e.target).length === 0
                        && $collapse.has(e.target).length === 0
                        && !$(e.target).hasClass("k-item")
                        && !$(e.target).hasClass("k-calendar-container")
                        && !$(e.target).hasClass("k-list-container")
                        && !$(e.target).hasClass("k-link")
                        && !$(e.target).hasClass("k-icon")
                        && !$(e.target).hasClass("k-header")
                        && !$(e.target).hasClass("k-content")
                        && !$(e.target).hasClass("k-month")
                        && !$(e.target).hasClass("tooltip-inner")
                        && !$(e.target).hasClass("tooltip fade")
                        && !$(e.target).hasClass("arrow")
                        && !$(e.target).hasClass("k-list-scroller")
                        && !$(e.target).hasClass("k-calendar-view")
                        && dropdownEmptyContainer.has(e.target).length === 0
                        && !dropdownEmptyContainer.is(e.target)) {

                        if (e.target.title !== undefined) {
                            var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                            var isDay = false;
                            var day = e.target.title;

                            for (var i = 0; i < days.length; i++) {
                                if (day === days[i]) {
                                    isDay = true;
                                    break;
                                }
                            }
                            if (isDay === false) {
                                self.slideModal.destroy();
                            }
                        }
                    }
                }
            }
            $(document).on("mouseup", serviceListDocumentOnMousedown);

            self.eventListenersFunctions["documentOnMousedown"] = serviceListDocumentOnMousedown;

            self.evaluateRecordValidation();
        })();
    };

    ServiceListsGenerator.prototype.evaluateDependantSections = function (params) {
        var modifiedSectionName = params.modifiedSectionName,
            evaluatedSections = params.evaluatedSections;

        if (!this.sectionDependenciesDictionary[modifiedSectionName])
            return;

        var allSections = this.viewModel.sections();

        for (var dependantSectionName in this.sectionDependenciesDictionary[modifiedSectionName]) {
            if (evaluatedSections.indexOf(dependantSectionName) > -1)
                continue;

            var foundSection = allSections.find(function (item) {
                return item.name === dependantSectionName;
            });

            this.validateSection({
                section: foundSection,
                evaluatedSections: evaluatedSections,
                reevaluateDataValidation: true,

                //to stop the evaluation going to deeper levels, set this flag to TRUE
                preventReevaluateDependants: true
            });

            //start easy - just validate the fucking sections

            //var sectionData = JSON.parse(JSON.stringify(foundSection.data()));

            ////update each item

            //this.updateGridItem({
            //    uniqueId: newUniqueId,
            //    name: name,
            //    data: newItem,
            //    isItemAddedIfNew: true
            //});

            //this.updateJsonItem({
            //    uniqueId: newUniqueId,
            //    name: name,
            //    //data: data
            //});
        }
    };

    ServiceListsGenerator.prototype.updateGridItem = function (params) {
        var uniqueId = params.uniqueId,
            name = params.name,
            data = params.data,
            isItemAddedIfNew = params.isItemAddedIfNew;

        //update the item found by Unique ID AND name in the View Model

        var allSections = this.viewModel.sections();
        var foundSection = allSections.find(function (item) {
            return item.name === name;
        });

        var sectionData = JSON.parse(JSON.stringify(foundSection.data()));
        if (!data) {
            var indexOfItemToDelete = 0;

            for (var index = 0; index < sectionData.length; index++) {
                if (sectionData[index].UniqueID === uniqueId) {
                    indexOfItemToDelete = index;
                    break;
                }
            }

            sectionData.splice(indexOfItemToDelete, 1);
            foundSection.data(sectionData);

            this.validateSection({
                section: foundSection
            });

            return;
        }

        if (isItemAddedIfNew) {
            var newDataObj = JSON.parse(JSON.stringify(data));
            this.validateItem({
                name: name,
                data: newDataObj,
                template: foundSection.template
            });

            sectionData.push(newDataObj);
            foundSection.data(sectionData);

            this.validateSection({
                section: foundSection
            });

            return;
        }

        var itemToUpdate = null;
        var itemToUpdateIndex = 0;

        for (var j = 0; j < sectionData.length; j++) {
            if (sectionData[j].UniqueID === uniqueId) {
                itemToUpdateIndex = j;
                itemToUpdate = sectionData[j];
                break;
            }
        }

        for (var property in data) {
            if (property === "Id" || property === "UniqueID")
                continue;

            var newDataObject = null;
            if (!data[property]) {
                newDataObject = null;
            }
            else {
                newDataObject = JSON.parse(JSON.stringify(data[property]));
            }

            itemToUpdate[property] = newDataObject;
        }

        this.validateItem({
            name: name,
            data: itemToUpdate,
            template: foundSection.template
        });

        sectionData.splice(itemToUpdateIndex, 1, itemToUpdate);
        foundSection.data(sectionData);

        this.validateSection({
            section: foundSection
        });
    };

    ServiceListsGenerator.prototype.updateJsonItem = function (params) {
        var uniqueId = params.uniqueId,
            name = params.name,
            data = params.data,
            isItemAddedIfNew = params.isItemAddedIfNew;

        //update the item found by Unique ID AND name in the LocalStorage
        var recordData = this.getStorageRecord();

        if (!recordData[name]) {
            recordData[name] = [];
        }

        var sectionData = recordData[name];

        this.setDataFlag(true);
        this.toggleSaveButton();

        //for delete
        if (!data) {
            var indexOfItemToDelete = 0;

            for (var index = 0; index < sectionData.length; index++) {
                if (sectionData[index].UniqueID === uniqueId) {
                    indexOfItemToDelete = index;
                    break;
                }
            }
            sectionData.splice(indexOfItemToDelete, 1);
            this.updateStorageRecord(recordData);
            return;
        }

        if (isItemAddedIfNew) {
            var newDataObj = JSON.parse(JSON.stringify(data));
            sectionData.push(newDataObj);
            this.updateStorageRecord(recordData);
            return;
        }

        var itemToEdit = null;
        var itemToEditIndex = 0;

        for (var i = 0; i < sectionData.length; i++) {
            if (sectionData[i].UniqueID === uniqueId) {
                itemToEditIndex = i;
                itemToEdit = sectionData[i];
                break;
            }
        }

        for (var property in data) {
            if (property === "Id" || property === "UniqueID")
                continue;

            var newDataObject = null;
            if (!data[property]) {
                newDataObject = null;
            }
            else {
                newDataObject = JSON.parse(JSON.stringify(data[property]));
            }

            itemToEdit[property] = newDataObject;
        }

        this.updateStorageRecord(recordData);
    };

    ServiceListsGenerator.prototype.cloneItem = function (params) {
        var name = params.name,
            uniqueId = params.uniqueId;

        var recordData = this.getStorageRecord();

        var sectionData = recordData[name];
        var itemToClone = null;

        for (var i = 0; i < sectionData.length; i++) {
            if (sectionData[i].UniqueID === uniqueId) {
                itemToClone = sectionData[i];
                break;
            }
        }

        this.addNewItem({
            name: name,
            data: itemToClone
        });
    };

    ServiceListsGenerator.prototype.addNewItem = function (params) {
        var name = params.name,
            mask = params.idMask;

        var newUniqueId = guidGenerator.generateUidFromMask(mask, name);
        var isDataCreated = false;

        var newItem = params.data;
        if (!newItem) {
            newItem = {
                UniqueID: newUniqueId
            };
            isDataCreated = true;
        }
        else {
            newItem.UniqueID = newUniqueId;
        }

        this.updateJsonItem({
            uniqueId: newUniqueId,
            name: name,
            data: newItem,
            isItemAddedIfNew: true
        });

        this.updateGridItem({
            uniqueId: newUniqueId,
            name: name,
            data: newItem,
            isItemAddedIfNew: true
        });

        this.openForm({
            sectionName: name,
            uniqueId: newUniqueId,
            isDataCreated: isDataCreated
        });
    };

    ServiceListsGenerator.prototype.validateSection = function (params) {  //(section, reevaluateDataValidation) {
        var section = params.section,
            reevaluateDataValidation = params.reevaluateDataValidation,
            evaluatedSections = params.evaluatedSections || [section.name],
            preventReevaluateDependants = params.preventReevaluateDependants;

        var sectionData = section.data();
        var sectionValidationResult = this.validateSectionRules(section.template);
        var allSectionItemsAreValid = true;

        //isSectionValid
        for (var i = 0; i < sectionData.length; i++) {
            //evaluate if this item is visible based on 

            if (section.template.list.rules &&
                section.template.list.rules.visibility) {
                var isVisible = this.expressionsEngine.evaluateExpression({
                    expression: section.template.list.rules.visibility.condition,
                    expressionContextData: sectionData[i],
                    contextName: section.template.name
                });

                if (isVisible === false)
                    continue;
            }

            if (reevaluateDataValidation) {
                this.validateItem({
                    //controls: section.template.form.controls,                    
                    name: section.name,
                    data: sectionData[i],
                    template: section.template
                });
            }
            allSectionItemsAreValid = allSectionItemsAreValid && sectionData[i].IsValid;

            //sectionValidationResult.errorMessages = sectionValidationResult.errorMessages.concat(sectionData[i].ErrorMessages);
            sectionValidationResult.isValid = sectionValidationResult.isValid && sectionData[i].IsValid;
        }

        if (allSectionItemsAreValid === false) {
            sectionValidationResult.errorMessages.push("There are invalid items in this section");
        }

        section.isValid(sectionValidationResult.isValid);
        section.errorMessages(sectionValidationResult.errorMessages);
        //section
        this.sectionsValidationCollection[section.name] = sectionValidationResult.isValid;

        if (preventReevaluateDependants)
            return;

        if (evaluatedSections.indexOf(section.name) === -1) {
            evaluatedSections.push(section.name);
        }

        this.evaluateDependantSections({
            modifiedSectionName: section.name,
            evaluatedSections: evaluatedSections
        });

        this.evaluateRecordValidation();
    };

    ServiceListsGenerator.prototype.evaluateRecordValidation = function () {
        var errorMessages = [];
        var isRecordValid = true;

        if (this.rules &&
            this.rules.restriction &&
            this.rules.restriction.type === "On Condition") {
            for (var i = 0; i < this.rules.restriction.conditions.length; i++) {
                var expressionObject = this.rules.restriction.conditions[i];

                var isConditionValid = this.expressionsEngine.evaluateExpression({
                    expression: expressionObject.condition
                });

                //console.log(expressionObject.condition, isConditionValid);

                if (isConditionValid === false) {
                    errorMessages.push(expressionObject.errorMessage);
                    isRecordValid = false;
                }
            }
        }

        for (var sectionNameKey in this.sectionsValidationCollection) {
            if (!this.sectionsValidationCollection[sectionNameKey]) {
                isRecordValid = false;
                errorMessages.push("There are invalid sections in this record");
                break;
            }
        }

        this.viewModel.isValid(isRecordValid);
        this.viewModel.errorMessages(errorMessages);
    };

    ServiceListsGenerator.prototype.validateSectionRules = function (sectionTemplate) {
        var result = {
            isValid: true,
            errorMessages: []
        };

        if (!sectionTemplate.rules ||
            !sectionTemplate.rules.restriction) {
            return result;
        }

        switch (sectionTemplate.rules.restriction.type) {
            case "On Condition":
                for (var i = 0; i < sectionTemplate.rules.restriction.conditions.length; i++) {
                    var isConditionValid = this.expressionsEngine.evaluateExpression({
                        expression: sectionTemplate.rules.restriction.conditions[i].condition
                    });

                    if (isConditionValid === false) {
                        result.errorMessages.push(sectionTemplate.rules.restriction.conditions[i].errorMessage);
                        result.isValid = false;
                    }
                }
        }

        if (result.isValid === false) {
            console.log("Section Validation Error: ", sectionTemplate.name, " -> ERROR: " + result.errorMessages);
        }

        return result;
    };

    ServiceListsGenerator.prototype.validateItem = function (params) {
        var name = params.name,
            data = params.data || {},
            template = params.template;

        var controlsValidationResult = validateDataItemControls.apply(this, [{
            sectionControls: template.form.controls,
            currentSectionName: name,
            currentSectionDataItem: data
        }]);

        var itemValidationResult = this.validateItemRules({
            currentSectionDataItem: data,
            template: template
        });

        data.IsValid = controlsValidationResult.isValid && itemValidationResult.isValid;
        data.ErrorMessages = controlsValidationResult.errorMessages.concat(itemValidationResult.errorMessages);

        return data.IsValid;
    };

    ServiceListsGenerator.prototype.validateItemRules = function (params) {
        var result = {
            isValid: true,
            errorMessages: []
        };

        var currentSectionDataItem = params.currentSectionDataItem,
            template = params.template;

        if (!template.form.rules ||
            !template.form.rules.restriction)
            return result;

        switch (template.form.rules.restriction.type) {
            case "On Condition":
                for (var i = 0; i < template.form.rules.restriction.conditions.length; i++) {
                    var conditionObject = template.form.rules.restriction.conditions[i];

                    var isConditionValid = this.expressionsEngine.evaluateExpression({
                        expression: conditionObject.condition,
                        expressionContextData: currentSectionDataItem,
                        contextName: template.name
                    });

                    if (isConditionValid === false) {
                        result.errorMessages.push(conditionObject.errorMessage);
                        result.isValid = false;
                    }
                }
        }

        if (result.isValid === false) {
            console.log("Item Validation Error: ", template.name, " -> ERROR: " + result.errorMessages);
        }

        return result;
    };

    function validateDataItemControls(params) {
        var result = {
            isValid: true,
            errorMessages: []
        };

        var sectionControls = params.sectionControls,
            currentSectionName = params.currentSectionName,
            currentSectionDataItem = params.currentSectionDataItem,
            repeatableElementSourceItem = params.repeatableElementSourceItem,
            parentContainerRules = params.parentContainerRules;

        //get parent visibility if any
        var isParentVisible = true;
        if (parentContainerRules &&
            parentContainerRules.visibility) {
            if (parentContainerRules.visibility.type === "On Condition") {
                isParentVisible = this.expressionsEngine.evaluateExpression({
                    expression: parentContainerRules.visibility.condition,
                    expressionContextData: currentSectionDataItem,
                    contextName: currentSectionName
                });
            }
            else if (parentContainerRules.visibility.type === "Hidden") {
                isParentVisible = false;
            }
        }

        if (isParentVisible === false) {
            return result;
        }

        //these controls are not the FORM CONTROLS -> they are only the structural controls used to fetch the settings and evaluate expressions
        for (var j = 0; j < sectionControls.length; j++) {
            var currentControl = sectionControls[j];
            if (currentControl.type === "SubtitleControl") {
                continue;
            }

            var namePartToBeReplaced = "record.";
            if (currentSectionName) {
                namePartToBeReplaced = namePartToBeReplaced + currentSectionName + ".";
            }

            if (currentControl.type === "Row" ||
                currentControl.type === "GroupControl") {

                var repeatableElementSource = null;
                if (currentControl.options.rules &&
                    currentControl.options.rules.repeatability) {

                    var repeatableElementPropertyName = currentControl.options.rules.repeatability.element.replace(namePartToBeReplaced, "");
                    repeatableElementSource = {
                        path: repeatableElementPropertyName,
                        index: 0
                    };

                    repeatableElementSource.repeatableLength = optionsHelpers.getNestedRepeatableItemLength(
                        currentSectionDataItem,
                        repeatableElementPropertyName,
                        repeatableElementSource);
                }

                var validateRowResult = validateDataItemControls.apply(this, [{
                    sectionControls: currentControl.options.controls,
                    currentSectionName: currentSectionName,
                    currentSectionDataItem: currentSectionDataItem,
                    repeatableElementSourceItem: repeatableElementSource,
                    parentContainerRules: currentControl.options.rules
                }]);

                result.isValid = result.isValid && validateRowResult.isValid;
                result.errorMessages = result.errorMessages.concat(validateRowResult.errorMessages);

                continue;
            }

            if (currentControl.type === "TabControl") {
                for (var m = 0; m < currentControl.options.tabs.length; m++) {
                    var currentTabControls = currentControl.options.tabs[m].controls;

                    var validateTabResult = validateDataItemControls.apply(this, [{
                        sectionControls: currentTabControls,
                        currentSectionName: currentSectionName,
                        currentSectionDataItem: currentSectionDataItem,
                        parentContainerRules: currentControl.options.rules
                    }]);

                    result.isValid = result.isValid && validateTabResult.isValid;
                    result.errorMessages = result.errorMessages.concat(validateTabResult.errorMessages);
                }

                continue;
            }

            var propertyName = currentControl.options.name.replace(namePartToBeReplaced, "");

            if (repeatableElementSourceItem) {
                var repeatableElementItem = JSON.parse(JSON.stringify(repeatableElementSourceItem));

                for (var i = 0; i < repeatableElementItem.repeatableLength; i++) {
                    repeatableElementItem.index = i;
                    controlValue = optionsHelpers.getNestedElementsPropertyValue(currentSectionDataItem, propertyName, repeatableElementItem);

                    var elementValidationItemResult = parseValidationItem.apply(this, [{
                        currentSectionName: currentSectionName,
                        currentSectionDataItem: currentSectionDataItem,
                        controlOptions: currentControl.options,
                        controlValue: controlValue, //.value,
                        repeatableElementItem: repeatableElementItem,
                        parentContainerRules: parentContainerRules
                    }]);

                    result.isValid = result.isValid && elementValidationItemResult.isValid;
                    result.errorMessages = result.errorMessages.concat(elementValidationItemResult.errorMessages);
                }
            }
            else {
                var controlValue = optionsHelpers.getNestedElementsPropertyValue(currentSectionDataItem, propertyName);

                var parseValidationItemResult = parseValidationItem.apply(this, [{
                    currentSectionName: currentSectionName,
                    currentSectionDataItem: currentSectionDataItem,
                    controlOptions: currentControl.options,
                    controlValue: controlValue, //.value,
                    parentContainerRules: parentContainerRules
                }]);

                result.isValid = result.isValid && parseValidationItemResult.isValid;
                result.errorMessages = result.errorMessages.concat(parseValidationItemResult.errorMessages);
            }
        }

        return result;
    }

    function parseValidationItem(params) {
        //var isValid = true;
        var result = {
            isValid: true,
            errorMessages: []
        };

        var currentSectionName = params.currentSectionName,
            currentSectionDataItem = params.currentSectionDataItem,
            controlOptions = params.controlOptions,
            controlValue = params.controlValue,
            repeatableElementItem = params.repeatableElementItem;

        var isRequired = true;
        var isVisible = true;

        //calculate item visibility
        if (isVisible &&
            controlOptions.rules &&
            controlOptions.rules.visibility) {
            if (controlOptions.rules.visibility.type === "On Condition") {
                isVisible = this.expressionsEngine.evaluateExpression({
                    expression: controlOptions.rules.visibility.condition,
                    expressionContextData: currentSectionDataItem,
                    contextName: currentSectionName,
                    repeatableElement: repeatableElementItem
                });
            }
            else if (controlOptions.rules.visibility.type === "Hidden") {
                isVisible = false;
            }
        }

        //calculate required
        if (isVisible &&
            controlOptions.rules &&
            controlOptions.rules.required) {
            if (controlOptions.rules.required.type === "Optional") {
                //remove required error
                isRequired = false;
            }
            else if (controlOptions.rules.required.type === "On Condition") {
                isRequired = this.expressionsEngine.evaluateExpression({
                    expression: controlOptions.rules.required.condition,
                    expressionContextData: currentSectionDataItem,
                    contextName: currentSectionName,
                    repeatableElement: repeatableElementItem
                });
            }
        }

        var isValueValid = true;

        //evaluate the auto-calculated expressions
        if (isVisible &&
            result.isValid &&
            controlOptions.rules &&
            controlOptions.rules.calculatedRestrictions) {
            //foreach expressions
            for (var i = 0; i < controlOptions.rules.calculatedRestrictions.conditions.length; i++) {
                var expressionObject = controlOptions.rules.calculatedRestrictions.conditions[i];

                isValueValid = this.expressionsEngine.evaluateExpression({
                    expression: expressionObject.condition,
                    expressionContextData: currentSectionDataItem,
                    contextName: currentSectionName,
                    repeatableElement: repeatableElementItem
                });

                if (isValueValid === false) {
                    result.isValid = false;
                    result.errorMessages.push(expressionObject.errorMessage);
                    break;
                }
            }
        }

        if (isVisible &&
            result.isValid &&
            controlOptions.rules &&
            controlOptions.rules.restriction &&
            controlOptions.rules.restriction.type === "On Condition") {
            isValueValid = this.expressionsEngine.evaluateExpression({
                expression: controlOptions.rules.restriction.condition,
                expressionContextData: currentSectionDataItem,
                contextName: currentSectionName,
                repeatableElement: repeatableElementItem
            });

            if (isValueValid === false) {
                result.isValid = false;
                result.errorMessages.push(controlOptions.rules.restriction.errorMessage);
            }
        }

        //final validation result
        if (isVisible &&
            isRequired) {
            if ((!controlValue && controlValue !== 0) ||
                controlValue === "") {
                result.isValid = false;
                result.errorMessages = [controlOptions.label + " is required"];
            }
        }

        return result;
    }

    var _bindForm = function (params) {
        var slideModal = this;

        this.validationResultObject = {
            isValid: ko.observable(true),
            errorMessages: ko.observableArray([])
        };

        var uniqueId = params.uniqueId,
            sectionStructure = params.sectionStructure,
            externalDataSource = params.externalDataSource,
            externalRules = params.externalRules,
            sectionName = params.sectionName,
            self = params.serviceListGenerator,
            isDataCreated = params.isDataCreated;

        this.jsonForms = new JsonForm({
            $container: $("#controlsContainer"),
            name: sectionName,
            controls: sectionStructure.form.controls,
            rules: sectionStructure.form.rules,
            externalDataSource: externalDataSource,
            externalRules: externalRules,
            uniqueId: uniqueId,
            onSaveResultData: function (resultData, name) {
                //toggle button
                self.setDataFlag(true);
                self.toggleSaveButton();

                self.updateJsonItem({
                    uniqueId: resultData.UniqueID,
                    name: name,
                    data: resultData
                });

                self.updateGridItem({
                    uniqueId: resultData.UniqueID,
                    name: name,
                    data: resultData
                });
            },
            onFormValidationComplete: function (formValidationResult) {
                slideModal.validationResultObject.isValid(formValidationResult.isValid);
                slideModal.validationResultObject.errorMessages(formValidationResult.errorMessages);
            },
            onFormCreated: function () {
                var formInstance = this;

                var $header = $(".modal-header", $("#" + slideModal.modalId));
                var $validationButtonContainer = $("<div class='validationButtonContainer' />");
                //$header.append($validationButtonContainer);
                $("h5", $header).after($validationButtonContainer);

                ko.applyBindingsToNode(
                    $validationButtonContainer[0],
                    {
                        template: {
                            name: "validation-feedback-button-template",
                            data: self.slideModal.validationResultObject
                        }
                    });

                setTimeout(function () {
                    formInstance.evaluateFormRules();
                    $("#controlsContainer input:visible").eq(0).focus();
                });

                $(".facebook-loading-animation-wrapper").hide();
                $("#controlsContainer").css("display", "block");
            },
            record: self.getStorageRecord(),
            isDataCreated: isDataCreated
        });
    };

    var _getDataSourceFromLocalStorage = function (dataSourceType, sectionStructure) {
        if (!sectionStructure.form[dataSourceType]) {
            return null;
        }

        var dataSource = {};
        var infoObject = JSON.parse(localStorage.getItem("JsonVersionsStructure"));

        for (var i = 0; i < sectionStructure.form[dataSourceType].length; i++) {
            var dataSourceKey = ''; 

            var dataSourceItem = sectionStructure.form[dataSourceType][i];
            if (dataSourceItem.constructor === Object) {
                dataSourceKey = dataSourceItem.localStorageName;
            }
            else {
                dataSourceKey = dataSourceItem;
            }

            var dataSourceVersion = infoObject[dataSourceKey];

            var storageKey = dataSourceKey + "V" + dataSourceVersion;
            dataSource[dataSourceKey] = JSON.parse(localStorage.getItem(storageKey));

            if (dataSourceItem.constructor === Object) {
                dataSource[dataSourceKey] = $.extend(true, {}, dataSource[dataSourceKey], dataSourceItem);
            }
        }

        return dataSource;
    };

    ServiceListsGenerator.prototype.openForm = function (params) {
        var self = this;
        var sectionName = params.sectionName,
            uniqueId = params.uniqueId,
            isDataCreated = params.isDataCreated;

        if (this.slideModal && this.slideModal.jsonForms) {
            //var jsonFormsOptions = this.slideModal.jsonForms.options;

            if (sectionName === this.slideModal.jsonForms.options.name &&
                uniqueId === this.slideModal.jsonForms.options.uniqueId) {
                //$("input:visible:first", this.slideModal.jsonForms.$container).focus();
                return;
            }
            this.slideModal.destroy();
        }

        //get the FORM structure for that section
        var allSections = this.sectionsStructure;

        var sectionStructure = allSections.find(function (item) {
            return item.name === sectionName;
        });

        var modalOptions = {
            $container: $(".plugin-container"),
            $modalContainer: $("#controlsContainer"),
            template: "rightSlideModalTemplate",
            footerDisplayed: sectionStructure.form.footerDisplayed,
            secondaryButtonHandler: function () { },
            secondaryButtonText: ko.observable("Close"),
            modalId: sectionStructure.form.title,
            title: sectionStructure.form.title,
            onSetContent: function () {   
                var externalDataSource = _getDataSourceFromLocalStorage("externalDataSource", sectionStructure);
                var externalRules = _getDataSourceFromLocalStorage("externalRules", sectionStructure);

                _bindForm.apply(this, [{
                    uniqueId: uniqueId,
                    externalDataSource: externalDataSource,
                    externalRules: externalRules,
                    sectionStructure: sectionStructure,
                    sectionName: sectionName,
                    serviceListGenerator: self,
                    isDataCreated: isDataCreated
                }]);
            },
            onDestroyContent: function () {
                var rowId = this.jsonForms.options.uniqueId;
                self.unselectItem(rowId);

                self.slideModal = null;

                var emptyFieldsCounter = 0;
                var uniqueIdControlIndex = 0;
                for (var i = 1, len = this.jsonForms._controls.length; i < len; i++) {
                    var currentValue = this.jsonForms._controls[i].getValue();
                    var currentName = this.jsonForms._controls[i].options.name;

                    if (currentValue === "" || !currentValue) {
                        emptyFieldsCounter++;
                    }

                    if (currentName.indexOf("UniqueID") >= 0) {
                        uniqueIdControlIndex = i;
                    }
                }

                var activeControlsCounter = this.jsonForms._controls.length - 1;
                if (emptyFieldsCounter === activeControlsCounter) {
                    var uid = this.jsonForms._controls[uniqueIdControlIndex].getValue();
                    var selectedItems = self.getItemFromList(uid);

                    for (var j = 0; j < selectedItems.length; j++) {
                        self.updateJsonItem(selectedItems[j]);
                        self.updateGridItem(selectedItems[j]);
                    }
                }

                this.jsonForms.destroy();
                //clean the KO binding for the errors button
            }
        };

        this.slideModal = new SlideModal(modalOptions);
        this.slideModal.showModal();
    };

    ServiceListsGenerator.prototype.selectItem = function (uniqueId, withCheckbox) {
        var self = this,
            $selectedItem = $(".qualifyRow[data-id='" + uniqueId + "']");

        $selectedItem.addClass("selected-row");

        if (withCheckbox) {
            $(".k-checkbox", $selectedItem).prop("checked", true);
            self.toogleMainDropdownActions();
        }
    };

    ServiceListsGenerator.prototype.unselectItem = function (uniqueId, withCheckbox) {
        var self = this,
            $selectedItem = $(".qualifyRow[data-id='" + uniqueId + "']");

        $selectedItem.removeClass("selected-row");

        if (withCheckbox) {
            $(".k-checkbox", $selectedItem).prop("checked", false);
            self.toogleMainDropdownActions();
        }
    };

    ServiceListsGenerator.prototype.selectSectionItems = function (sectionName, withCheckbox) {
        var self = this;

        var $sectionContainer = $(".qualifyTable[data-section='" + sectionName + "']");
        $(".qualifyRow", $sectionContainer).each(function () {
            var uniqueId = $(this).attr("data-id");
            self.selectItem(uniqueId, withCheckbox);
        });
    };

    ServiceListsGenerator.prototype.unselectSectionItems = function (sectionName, withCheckbox) {
        var self = this;

        var $sectionContainer = $(".qualifyTable[data-section='" + sectionName + "']");
        $(".qualifyRow", $sectionContainer).each(function () {
            var uniqueId = $(this).attr("data-id");
            self.unselectItem(uniqueId, withCheckbox);
        });
    };

    ServiceListsGenerator.prototype.unselectAllItems = function (withCheckbox) {
        var self = this,
            cards = $(".card", self.$container);

        $.each(cards, function () {
            var $card = $(this),
                sectionName = $(".qualifyTable", $card).attr("data-section");

            self.unselectSectionItems(sectionName, withCheckbox);

            self.toogleSelectAll($card);
        });
    };

    ServiceListsGenerator.prototype.toogleMainDropdownActions = function () {
        var selectedRowsCount = $(".qualifyRow .k-checkbox:checked").length;

        if (selectedRowsCount === 0) {
            $(".main-delete-button, .main-clone-button").addClass("disabled");
        } else if (selectedRowsCount === 1) {
            $(".main-delete-button, .main-clone-button").removeClass("disabled");
        } else if (selectedRowsCount > 0) {
            $(".main-delete-button").removeClass("disabled");
            $(".main-clone-button").addClass("disabled");
        }
    };

    ServiceListsGenerator.prototype.toogleSelectAll = function ($cardElement) {
        var $selectAllCheckbox = $cardElement.find(".selectAllCheckbox"),
            $section = $(".qualifyTable", $cardElement);

        // check if rows exists
        if ($section.children().length === 0) {
            $selectAllCheckbox.prop("checked", false);
            return;
        } else {
            var $sectionCheckboxesCount = $(".k-checkbox", $section).length,
                $sectionSelectedCheckboxesCount = $(".k-checkbox:checked", $section).length;

            if ($sectionSelectedCheckboxesCount === $sectionCheckboxesCount) {
                $selectAllCheckbox.prop("checked", true);
            } else {
                $selectAllCheckbox.prop("checked", false);
            }
        }
    };

    ServiceListsGenerator.prototype.getSelectedItems = function () {
        var selectedItems = [];
        $(".qualifyRow.selected-row").each(function () {
            var $self = $(this);

            selectedItems.push({
                name: $self.attr("data-section"),
                uniqueId: $self.attr("data-id")
            });
        });

        return selectedItems;
    };

    ServiceListsGenerator.prototype.getItemFromList = function (uniqueId) {
        var selectedItems = [];
        $(".qualifyRow").each(function () {
            var $self = $(this);
            if ($self.attr("data-id") === uniqueId) {
                selectedItems.push({
                    name: $self.attr("data-section"),
                    uniqueId: $self.attr("data-id")
                });
            }
        });

        return selectedItems;
    };

    ServiceListsGenerator.prototype.saveRecord = function (callback) {
        callback = callback || function () { };
        var recordData = this.getStorageRecord();
        var self = this;

        $.ajax({
            url: "/Umi/SaveRecord",
            type: "POST",
            data: {
                RecordDataJson: JSON.stringify(recordData)
            },
            //contentType: 'application/json',
            //dataType: "json",
            complete: function (data, status) {
                if (status === "success") {
                    //
                    console.log("DATA SAVED");
                    self.setDataFlag(false);
                    self.toggleSaveButton();
                    callback();
                }
                else {
                    console.log("ERROR ON SERVER ON SaveRecord ACTION");
                }
            }
        });
    };

    ServiceListsGenerator.prototype.submit = function () {
        var self = this;

        contentConfigurator.showMainLoader();

        self.saveRecord(function () {
            var recordData = self.getCleanRecord();

            $.ajax({
                url: self.submitUrl,
                type: "POST",
                data: {
                    recordId: recordData.UniqueID,
                    cleanRecordDataJson: JSON.stringify(recordData)
                },
                complete: function (data, status) {
                    var responseJSON = data.responseJSON;
                    if (status === "success") {
                        if (responseJSON.success === false) {
                            contentConfigurator.hideMainLoader();
                            MessageDisplay.DisplayAfter($(".functionalities-container"), responseJSON.errorMessage, "alert alert-danger");
                            $(".downloadPreparedFiles").show();
                            console.error("SUBMIT FAILED");
                        }
                        else {
                            console.log("DATA SUBMITTED");
                            var recordWithSubmittedData = responseJSON.Record;
                            self.updateStorageRecord(recordWithSubmittedData);
                            self.submitSuccessCallback.call(self);
                        }
                    } else {
                        console.log("Internal Server Error");
                    }
                }
            });
        });
    };

    ServiceListsGenerator.prototype.destroy = function () {
        var self = this;

        $(".saveRecordButton").off("click");
        $(".submitButton").off("click");

        $(".body-content").off("click", ".clone-button");
        $(".body-content").off("click", ".delete-button");

        $(".body-content").off("mouseleave", ".dropdown-menu.show");
        $(".body-content").off("mouseleave", ".dropdown-toggle[aria-expanded='true']");

        $(document).off("mousedown", self.eventListenersFunctions.documentOnMousedown);

        ko.cleanNode($(".rendered-content-container")[0]);
    };

    ServiceListsGenerator.prototype.getCleanRecord = function () {
        var recordData = this.getStorageRecord();

        //these are the properties we want to be kept in the JSON sent to the CALCULATE UMI function
        var allRecordProperties = [
            "UniqueID", "RecordName"
        ];

        for (var i = 0; i < this.sectionsStructure.length; i++) {
            var sectionStructure = this.sectionsStructure[i];
            //parsedSections.push(sectionStructure.name);
            allRecordProperties.push(sectionStructure.name);

            if (recordData[sectionStructure.name] === null)
                delete recordData[sectionStructure.name];

            var sectionDataList = recordData[sectionStructure.name];
            if (sectionDataList === null || (sectionDataList && sectionDataList.length === 0))
                delete recordData[sectionStructure.name];

            if (!sectionDataList) {
                continue; 
            }

            for (var j = 0; j < sectionDataList.length; j++) {
                if (sectionStructure.list.rules &&
                    sectionStructure.list.rules.visibility &&
                    sectionStructure.list.rules.visibility.type === "On Condition") {

                    var isItemVisible = this.expressionsEngine.evaluateExpression({
                        expression: sectionStructure.list.rules.visibility.condition,
                        expressionContextData: sectionDataList[j]
                    });

                    if (isItemVisible === false) {
                        console.log(sectionDataList[j], " is removed form section: " + sectionStructure.name);
                        sectionDataList[j] = null;
                        continue;
                    }
                }

                var itemSkeleton = ServiceListsGenerator.CleanupUtilities.cleanupDataItem({
                    currentSectionDataItem: sectionDataList[j],
                    controls: sectionStructure.form.controls,
                    currentSectionName: sectionStructure.name,
                    expressionsEngine: this.expressionsEngine
                });

                ServiceListsGenerator.CleanupUtilities.removeInexistentProperties(sectionDataList[j], itemSkeleton);

                sectionDataList[j] = _sortObjectProperties(sectionDataList[j]);
            }

            if (recordData[sectionStructure.name]) {
                recordData[sectionStructure.name] = recordData[sectionStructure.name].filter(function (item) { return item !== null && item !== undefined; });
            }
        }

        for (var recordProperty in recordData) {
            if (allRecordProperties.indexOf(recordProperty) > -1) {
                continue;
            }

            delete recordData[recordProperty];
        }

        //for (var k = 0; k < allSectionsNames.length; k++) {
        //    if (parsedSections.indexOf(allSectionsNames[i]) > -1) {
        //        continue;
        //    }

        //    if (recordData[allSectionsNames[i]] !== undefined) {
        //        delete recordData[allSectionsNames[i]];
        //    }
        //}

        return recordData;
    };

    var _sortObjectProperties = function (unordered) {
        var ordered = {};
        Object.keys(unordered).sort().forEach(function (key) {
            if (unordered[key].constructor === Object) {
                ordered[key] = _sortObjectProperties(unordered[key]);
            }
            else {
                ordered[key] = unordered[key];

                if (ordered[key].constructor === Array) {
                    for (var i = 0; i < ordered[key].length; i++) {
                        ordered[key][i] = _sortObjectProperties(ordered[key][i]);
                    }
                }
            }
        });

        return ordered;
    };

    ServiceListsGenerator.CleanupUtilities = {};

    ServiceListsGenerator.CleanupUtilities.removeInexistentProperties = function (element, skeleton) {
        for (var property in element) {
            if (skeleton[property] === undefined || (!element[property] && element[property] !== 0)) {
                delete element[property];
                continue;
            }

            if (element[property].constructor === Object) {
                ServiceListsGenerator.CleanupUtilities.removeInexistentProperties(element[property], skeleton[property]);
            }

            if (element[property].constructor === Array) {
                for (var i = 0; i < element[property].length; i++) {
                    ServiceListsGenerator.CleanupUtilities.removeInexistentProperties(element[property][i], skeleton[property][0]);
                }
            }
        }
    };

    ServiceListsGenerator.CleanupUtilities.cleanupDataItem = function (params) {
        var controls = params.controls,
            expressionsEngine = params.expressionsEngine,
            currentSectionName = params.currentSectionName,
            currentSectionDataItem = params.currentSectionDataItem,
            repeatableElementItem = params.repeatableElementItem,
            isParentContainerHidden = params.isParentContainerHidden,
            currentSectionDataItemSkeleton = params.currentSectionDataItemSkeleton || {};

        for (var j = 0; j < controls.length; j++) {
            var currentControl = controls[j];
            var isVisible = !isParentContainerHidden;

            if (currentControl.type === "SubtitleControl") {
                continue;
            }

            var namePartToBeReplaced = "record.";
            if (currentSectionName) {
                namePartToBeReplaced = namePartToBeReplaced + currentSectionName + ".";
            }

            if (isVisible &&
                currentControl.options.rules &&
                currentControl.options.rules.visibility) {
                if (currentControl.options.rules.visibility.type === "On Condition") {
                    isVisible = expressionsEngine.evaluateExpression({
                        expression: currentControl.options.rules.visibility.condition,
                        expressionContextData: currentSectionDataItem,
                        contextName: currentSectionName,
                        repeatableElement: repeatableElementItem
                    });
                }
                else if (currentControl.options.rules.visibility.type === "Hidden") {
                    isVisible = false;
                }
            }

            if (currentControl.type === "Row" ||
                currentControl.type === "GroupControl") {

                var repeatableElementSource = null;
                if (currentControl.options.rules &&
                    currentControl.options.rules.repeatability) {

                    var repeatableElementPropertyName = currentControl.options.rules.repeatability.element.replace(namePartToBeReplaced, "");
                    repeatableElementSource = {
                        path: repeatableElementPropertyName,
                        index: 0
                    };

                    repeatableElementSource.repeatableLength = optionsHelpers.getNestedRepeatableItemLength(
                        currentSectionDataItem,
                        repeatableElementPropertyName,
                        repeatableElementSource);
                }

                if (repeatableElementSource) {
                    for (var i = 0; i < repeatableElementSource.repeatableLength; i++) {
                        repeatableElementSource.index = i;
                        ServiceListsGenerator.CleanupUtilities.cleanupDataItem({
                            controls: currentControl.options.controls,
                            currentSectionName: currentSectionName,
                            currentSectionDataItem: currentSectionDataItem,
                            repeatableElementItem: repeatableElementSource,
                            isParentContainerHidden: isVisible === false,
                            currentSectionDataItemSkeleton: currentSectionDataItemSkeleton,
                            expressionsEngine: expressionsEngine
                        });
                    }
                }
                else {
                    ServiceListsGenerator.CleanupUtilities.cleanupDataItem({
                        controls: currentControl.options.controls,
                        currentSectionName: currentSectionName,
                        currentSectionDataItem: currentSectionDataItem,
                        isParentContainerHidden: isVisible === false,
                        currentSectionDataItemSkeleton: currentSectionDataItemSkeleton,
                        expressionsEngine: expressionsEngine
                    });
                }

                continue;
            }

            if (currentControl.type === "TabControl") {
                for (var m = 0; m < currentControl.options.tabs.length; m++) {
                    var currentTabControls = currentControl.options.tabs[m].controls;

                    ServiceListsGenerator.CleanupUtilities.cleanupDataItem({
                        controls: currentTabControls,
                        currentSectionName: currentSectionName,
                        currentSectionDataItem: currentSectionDataItem,
                        isParentContainerHidden: isVisible === false,
                        currentSectionDataItemSkeleton: currentSectionDataItemSkeleton,
                        expressionsEngine: expressionsEngine
                    });
                }

                continue;
            }

            var propertyName = currentControl.options.name.replace(namePartToBeReplaced, "");
            if ((isVisible && !isParentContainerHidden) || currentControl.options.outputIfHidden === true) {
                if (currentControl.type === "DateInputControl") {
                    ServiceListsGenerator.CleanupUtilities.cleanDateControlValue(
                        currentSectionDataItem,
                        propertyName,
                        repeatableElementItem);
                }

                ServiceListsGenerator.CleanupUtilities.addPropertyToElement(
                    currentSectionDataItemSkeleton,
                    propertyName,
                    repeatableElementItem);
                continue;
            }

            ServiceListsGenerator.CleanupUtilities.cleanNestedProperty(currentSectionDataItem, propertyName, repeatableElementItem);
        }

        ServiceListsGenerator.CleanupUtilities.cleanupNullProperties(currentSectionDataItem);

        return currentSectionDataItemSkeleton;
    };

    ServiceListsGenerator.CleanupUtilities.cleanDateControlValue = function (element, property, repeatableElement) {
        //if property has dots
        var propertyFirstPart = property;

        if (property.indexOf(".") > 0) {
            propertyFirstPart = property.substring(0, property.indexOf("."));
            var propertyLastPart = property.replace(propertyFirstPart + ".", "");

            if (repeatableElement &&
                propertyFirstPart === repeatableElement.path &&
                element[propertyFirstPart]) {

                if (element[propertyFirstPart][repeatableElement.index]) {
                    return ServiceListsGenerator.CleanupUtilities.cleanDateControlValue(element[propertyFirstPart][repeatableElement.index], propertyLastPart, repeatableElement);
                }

                return null;
            }

            if (element[propertyFirstPart]) {
                return ServiceListsGenerator.CleanupUtilities.cleanDateControlValue(element[propertyFirstPart], propertyLastPart, repeatableElement);
            }

            return null;
        }

        if (!element[propertyFirstPart])
            return null;

        var date = new Date(element[propertyFirstPart]);

        element[propertyFirstPart] = getFormattedDate(date);
        return null;
    };

    function getFormattedDate(date) {
        var year = date.getFullYear();

        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;

        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;

        return year + '-' + month + '-' + day;
    }

    ServiceListsGenerator.CleanupUtilities.addPropertyToElement = function (element, property, repeatableElement) {
        var propertyFirstPart = property;

        if (property.indexOf(".") > 0) {
            propertyFirstPart = property.substring(0, property.indexOf("."));
            var propertyLastPart = property.replace(propertyFirstPart + ".", "");

            if (repeatableElement &&
                propertyFirstPart === repeatableElement.path) {
                if (!element[propertyFirstPart])
                    element[propertyFirstPart] = [];

                if (element[propertyFirstPart].length === 0) {
                    var newSubItem = {};
                    element[propertyFirstPart].push(newSubItem);
                }

                ServiceListsGenerator.CleanupUtilities.addPropertyToElement(
                    element[propertyFirstPart][0],
                    propertyLastPart,
                    repeatableElement);
            }
            else {
                if (!element[propertyFirstPart])
                    element[propertyFirstPart] = {};
                
                ServiceListsGenerator.CleanupUtilities.addPropertyToElement(
                    element[propertyFirstPart],
                    propertyLastPart,
                    repeatableElement);
            }

            return;
        }

        element[propertyFirstPart] = "";
    };

    ServiceListsGenerator.CleanupUtilities.cleanupNullProperties = function (element) {
        if (!element || element.constructor !== Object)
            return;

        for (var key in element) {
            if (element[key] === null || element[key] === '') {
                delete element[key];
                continue;
            }

            if (element[key].constructor === Object)
                ServiceListsGenerator.CleanupUtilities.cleanupNullProperties(element[key]);
        }
    };

    ServiceListsGenerator.CleanupUtilities.cleanNestedProperty = function (element, property, repeatableElement) {
        var self = this;
        //if property has dots
        var propertyFirstPart = property;

        if (property.indexOf(".") > 0) {
            propertyFirstPart = property.substring(0, property.indexOf("."));
            var propertyLastPart = property.replace(propertyFirstPart + ".", "");

            if (repeatableElement &&
                propertyFirstPart === repeatableElement.path &&
                element[propertyFirstPart]) {

                if (element[propertyFirstPart][repeatableElement.index]) {
                    ServiceListsGenerator.CleanupUtilities.cleanNestedProperty(element[propertyFirstPart][repeatableElement.index], propertyLastPart, repeatableElement);
                }
            }

            if (element[propertyFirstPart]) {
                ServiceListsGenerator.CleanupUtilities.cleanNestedProperty(element[propertyFirstPart], propertyLastPart, repeatableElement);

                if (Object.keys(element[propertyFirstPart]).length === 0) {
                    delete element[propertyFirstPart];
                }
            }

            return;
        }

        if (element[propertyFirstPart] !== undefined) {
            //console.log("CLEANING ", propertyFirstPart, " from ", element);
            delete element[propertyFirstPart];
        }        
    };

    return ServiceListsGenerator;
});