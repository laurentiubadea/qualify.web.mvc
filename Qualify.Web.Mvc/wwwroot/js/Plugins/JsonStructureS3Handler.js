﻿define([], function () {
    function JsonStructureS3Handler(params) {
        params = params || {};

        this.keyName = params.keyName;
        this.localStorageKey = params.localStorageKey;
        this.jsonVersionsStructure = params.jsonVersionsStructure;
    }

    JsonStructureS3Handler.prototype.GetUmiJsonS3 = function (jsonVersion, onSuccessCallback) {
        var self = this,
            obj = {
                KeyName: self.keyName + jsonVersion + ".json"
            };

        $.ajax({
            url: "/JsonAwsConfiguration/GenerateJsonPresignedUrl",
            type: "GET",
            data: obj,
            success: function (response) {
                $.ajax({
                    url: response,
                    type: "GET",
                    success: function (data) {
                        if (data.constructor !== Object) {
                            data = JSON.parse(data);
                        }

                        for (var i = 0; i < localStorage.length; i++) {
                            if (localStorage.key(i).includes(self.localStorageKey)) {
                                localStorage.removeItem(localStorage.key(i));
                            }
                        }

                        localStorage.setItem(self.localStorageKey + jsonVersion, JSON.stringify(data));
                        onSuccessCallback(data);
                    },
                    error: function (jqxhr, status, exception) {
                        console.error('Exception:', exception);
                    }
                });
            }
        });
    };

    JsonStructureS3Handler.prototype.GetJsonVersions = function (onSuccessCallback) {
        var self = this;

        $.ajax({
            url: "/JsonAwsConfiguration/GetJsonEnvSettingsForS3",
            type: "GET",
            success: function (data) {
                $.ajax({
                    url: data.urlPublicJsonVersions,
                    type: "GET",
                    success: function (response) {
                        if (response.constructor !== Object) {
                            response = JSON.parse(response);
                        }

                        localStorage.setItem("JsonVersionsStructure", JSON.stringify(response));

                        var localJson = JSON.parse(localStorage.getItem(self.localStorageKey + response[self.jsonVersionsStructure]));
                        if (!localJson) {
                            self.GetUmiJsonS3(
                                response[self.jsonVersionsStructure],
                                function (structuralJson) {
                                    onSuccessCallback(structuralJson);
                                });
                        } else {
                            onSuccessCallback(localJson);
                        }
                    }
                });
            }
        });
    };

    return JsonStructureS3Handler;
});