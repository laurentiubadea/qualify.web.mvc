﻿require.config({
    baseUrl: "/",
    paths: {
        "jquery": "./vendor/jquery/jquery.min",
        "bootstrap": "./vendor/bootstrap/bootstrap.bundle.min",
        "material": "./vendor/material-components-web/material-components-web.min",
        "knockout": "./vendor/knockout/knockout-latest",
        "knockout-mapping": "./vendor/knockout-mapping/knockout.mapping.min",
        "slideModal": "./lib/JSONModals/SlideModal",
        "expressionsEngine": "./lib/ExpressionsEngine/ExpressionsEngine",
        "formatNumber": "./vendor/jquery-number/jquery.number",
        "controlTypes": "./lib/JSONForms/Utilities/controlTypes"
    },
    urlArgs: 'v=8.7.0',
    config: {
        text: {
            useXhr: function () {
                // allow cross-domain requests
                // remote server allows CORS
                return false;
            }
        }
    },
    packages: [
        {
            name: "jsonForm",
            location: "lib/JSONForms",
            main: "JsonForms"
        }
    ]
});

(function () {
    var load = requirejs.load;
    requirejs.load = function (context, moduleId, url) {
        if (moduleId && moduleId.startsWith("kendo.")) {
            // if moduleId starts with "kendo.", we overwrite the url path to the kendo library location

            url = url.substring(url.lastIndexOf("/") + 1);
            url = "/vendor/KendoUI/js/" + url;
        }

        load.apply(this, arguments);
    };
}());