﻿"use strict";
define(["jquery", "js/Plugins/ContentConfigurator", "js/Plugins/JsonStructureS3Handler", "slideModal", "knockout"], function ($, ContentConfigurator, JsonStructureS3Handler, SlideModal, ko) {
    var contentConfigurator = new ContentConfigurator();
    LoanJsonProductDataSource();
    LoanJsonProductRulesDataSource();

    function MainInit(recordData) {
        $(".sidebar .user").text(recordData.RecordName);

        // Event listeners
        (function () {
            $(".umi-list-button").on("click", renderUmiPage);

            $(".pricing-list-button").on("click", renderPricingPage);

            $(".lmi-list-button").on("click", renderLmiPage);

            $(".repayment-list-button").on("click", function () {
                openRedirectSlideModal($(this).attr("data-href"), "ANZ Home Loan Repayment calculator");
            });
            $(".home-loan-list-button").on("click", function () {
                openRedirectSlideModal($(this).attr("data-href"), "ANZ Home Loan Fee calculator");
            });
            $(".valuation-list-button").on("click", function () {
                openRedirectSlideModal($(this).attr("data-href"), "RPData Property Valuations");
            });
            $(".branch-list-button").on("click", function () {
                openRedirectSlideModal($(this).attr("data-href"), "ANZ Branch Locator");
            });
        }());

        function renderUmiPage() {
            if (isItemSelected($(this).parent())) {
                console.log("prevent render");
                return;
            }

            contentConfigurator.disableSidebar();
            contentConfigurator.showKendoLoader();
            contentConfigurator.deselectSidebarItems();
            contentConfigurator.selectSidebarItem($(this).parent());

            RenderUmiPage.apply(this, [recordData.UniqueID]);
        }

        function renderPricingPage() {
            if (isItemSelected($(this).parent())) {
                console.log("prevent render");
                return;
            }

            contentConfigurator.disableSidebar();
            contentConfigurator.showKendoLoader();
            contentConfigurator.deselectSidebarItems();
            contentConfigurator.selectSidebarItem($(this).parent());

            RenderPricingPage.apply(this, [recordData.UniqueID]);
        }

        function renderLmiPage() {
            if (isItemSelected($(this).parent())) {
                console.log("prevent render");
                return;
            }

            contentConfigurator.disableSidebar();
            contentConfigurator.showKendoLoader();
            contentConfigurator.deselectSidebarItems();
            contentConfigurator.selectSidebarItem($(this).parent());

            RenderLmiPage.apply(this, [recordData.UniqueID]);
        }

        // Event listeners functions

        // Main Page trigger
        $(".umi-list-button").trigger("click");
    }

    function RenderUmiPage(recordDataUniqueId) {
        $.ajax({
            type: "POST",
            url: "/Web/RenderUmiPartialView",
            data: {
                id: recordDataUniqueId
            },
            beforeSend: function () {
                contentConfigurator.deleteContent();
            },
            success: function (data) {
                var partialView = data.partialView,
                    viewModel = data.model;

                contentConfigurator.appendContent(partialView);

                require(["js/umi"], function (umiJs) {
                    umiJs.MainInit(viewModel);

                    $(".sidebar li.available:not(.selected) button").on("click", umiJs.DestroyScript);
                });
            }
        });
    }

    function RenderPricingPage(recordDataUniqueId) {
        $.ajax({
            type: "POST",
            url: "/Web/RenderPricingPartialView",
            data: {
                id: recordDataUniqueId
            },
            beforeSend: function () {
                contentConfigurator.deleteContent();
            },
            success: function (data) {
                var partialView = data.partialView,
                    viewModel = data.model;

                contentConfigurator.appendContent(partialView);

                require(["js/pricing"], function (pricingJs) {
                    pricingJs.MainInit(viewModel);

                    $(".sidebar li.available:not(.selected) button").on("click", pricingJs.DestroyScript);
                });
            }
        });
    }

    function RenderLmiPage(recordDataUniqueId) {
        $.ajax({
            type: "POST",
            url: "/Web/RenderLmiPartialView",
            data: {
                id: recordDataUniqueId
            },
            beforeSend: function () {
                contentConfigurator.deleteContent();
            },
            success: function (data) {
                var partialView = data.partialView,
                    viewModel = data.model;

                contentConfigurator.appendContent(partialView);

                require(["js/lmi"], function (lmiJs) {
                    lmiJs.MainInit(viewModel);

                    $(".sidebar li.available:not(.selected) button").on("click", lmiJs.DestroyScript);
                });
            }
        });
    }

    function openRedirectSlideModal(redirectUrl, siteName) {
        var slideModal = null;
        var modalOptions = {
            $container: $(".plugin-container"),
            $modalContainer: $("#controlsContainer"),
            template: "slideModalTemplate",
            title: "Open window",
            footerDisplayed: true,
            mainButtonText: "Continue",
            secondaryButtonText: "Cancel",
            mainButtonHandler: function () {
                openNewTab(redirectUrl);
                slideModal.destroy();
            },
            onSetContent: function () {
                _displayModalRecordsMessage(siteName, onModalContentLoaded);
            },
            onDestroyContent: function () {
                //this.jsonForms.destroy();
            },
            position: SlideModal.Position.Center,
            backdrop: "static"
        };

        slideModal = new SlideModal(modalOptions);

        slideModal.showModal();
    }

    function openNewTab(url) {
        window.open(url, '_blank');
    }

    function _displayModalRecordsMessage(siteName, callback) {
        var $messageContainer = $("<div></div>", { id: "messageId" });
        $("#controlsContainer").append($messageContainer);

        var viewModel = {
            mainMessage: `You are about to open a third party site in a new browser tab or window`,
            siteName: siteName,
            policyMessage: `ANZ Qualify is not responsible for the content or policies of linked third party sites so please read
                            those policies closely, including privacy and security policies.
                            If you have any questions about the products and services offered on linked third party sites,
                            please contact the third party directly.`
        };

        ko.applyBindingsToNode($messageContainer[0], {
            template: {
                name: "redirect-template",
                data: viewModel
            }
        });

        callback();
    }

    function onModalContentLoaded() {
        $(".facebook-loading-animation-wrapper").css("display", "none");
        $("#controlsContainer").css("display", "block");
    }

    function isItemSelected(item) {
        if ($(item).hasClass("selected")) {
            return true;
        }
        return false;
    }

    function LoanJsonProductDataSource() {
        let jsonStructureS3Handler = new JsonStructureS3Handler({
            keyName: "Qualify-Resources-JSON-Forms/EXTERNAL-DATASOURCES/ProductsDataSourceV",
            localStorageKey: "ProductsDataSourceV",
            jsonVersionsStructure: "ProductsDataSource"
        });

        jsonStructureS3Handler.GetJsonVersions(function () {

        });
    }

    function LoanJsonProductRulesDataSource() {
        //uncomment this later
        let jsonStructureS3Handler = new JsonStructureS3Handler({
            keyName: "Qualify-Resources-JSON-Forms/EXTERNAL-RULES/ProductRulesV",
            localStorageKey: "ProductRulesV",
            jsonVersionsStructure: "ProductRules"
        });

        jsonStructureS3Handler.GetJsonVersions(function () {

        });
    }

    return {
        MainInit: MainInit,
        RenderUmiPage: RenderUmiPage,
        RenderPricingPage: RenderPricingPage,
        RenderLmiPage: RenderLmiPage
    };
});