﻿namespace Qualify.Web.Mvc
{
    public class SendJwtResult
    {
        public string Url { get; set; }
        public string JWT { get; set; }
    }
}
