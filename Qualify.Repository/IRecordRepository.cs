﻿using MongoDB.Driver;
using Qualify.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qualify.Repository
{
    public interface IRecordRepository
    {
        IMongoCollection<Record> BaseQuery();
        Task CreateAsync(Record record);
        Task<IEnumerable<Record>> GetAllAsync();
        Task<Record> GetByIdAsync(string id);
        Task<Record> GetByUniqueIdAsync(string uniqueId);
        //Task<IEnumerable<Record>> GetByUserIdAsync(string userId);
        Task<Record> UpdateByIdAsync(string id, Record record);
        Task DeleteByIdAsync(string id);
        Task DeleteManyAsync(IEnumerable<string> ids);
    }
}
