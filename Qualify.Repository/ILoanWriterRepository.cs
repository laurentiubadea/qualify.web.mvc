﻿using Qualify.Domain.Models.Identity;
using System.Threading.Tasks;

namespace Qualify.Repository
{
    public interface ILoanWriterRepository
    {
        Task CreateAsync(LoanWriter loanWriter);
        Task<LoanWriter> GetByUniqueIdAsync(string id);
        Task<LoanWriter> GetFirstUserAsync();
        Task<LoanWriter> GetByEmailAsync(string email);
        Task<LoanWriter> UpdateByIdAsync(string uniqueId, LoanWriter loanWriter);
        Task DeleteByIdAsync(string uniqueId);
        Task<LoanWriter> GetUserByOneTimeUseTokenAsync(string oneTimeUseUserToken);
    }
}
