﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qualify.Domain
{
    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
        public string ANZDB { get; set; }
    }
}
