﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qualify.Domain.Models
{
    public class Section
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UniqueId { get; set; }

        public IEnumerable<Field> Fields { get; set; }

        public IEnumerable<Section> Sections { get; set; }

        public Section()
        {
            Fields = new List<Field>
            {
                new Field
                {
                    Name = "Name",
                    Value = "Ahmad"
                },
                new Field
                {
                    Name = "NumberOfDependants",
                    Value = "6"
                }
            };
        }
    }
}
