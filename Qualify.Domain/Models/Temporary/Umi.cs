﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Qualify.Domain.Models.Temporary
{
    public class Umi : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();
        public IEnumerable<Indicator> Indicators { get; set; }
        public string MaxBorrowingCapacity { get; set; }
        public DateTime? RequestDate { get; set; }
        public string UmiResult { get; set; }
    }
}
