﻿using System.Collections.Generic;
using System.Linq;

namespace Qualify.Domain.Models.Temporary
{
    public class CleanRecord
    {
        public IEnumerable<dynamic> Expenses { get; set; }
        public IEnumerable<dynamic> Household { get; set; }
        public IEnumerable<dynamic> Income { get; set; }
        public IEnumerable<dynamic> Liability { get; set; }
        public IEnumerable<dynamic> NewLoan { get; set; }
        public IEnumerable<dynamic> Person { get; set; }
        public IEnumerable<dynamic> PricingDetails { get; set; }
        public IEnumerable<dynamic> PropertyAsset { get; set; }
        public string RecordName { get; set; }
        public string UniqueID { get; set; }

        public void CleanEmptyArrays() {
            if (Expenses != null && Expenses.Count() == 0)
                Expenses = null;

            if (Household != null && Household.Count() == 0)
                Household = null;

            if (Income != null && Income.Count() == 0)
                Income = null;

            if (Liability != null && Liability.Count() == 0)
                Liability = null;

            if (NewLoan != null && NewLoan.Count() == 0)
                NewLoan = null;

            if (Person != null && Person.Count() == 0)
                Person = null;

            if (PricingDetails != null && PricingDetails.Count() == 0)
                PricingDetails = null;

            if (PropertyAsset != null && PropertyAsset.Count() == 0)
                PropertyAsset = null;
        }
    }
}
