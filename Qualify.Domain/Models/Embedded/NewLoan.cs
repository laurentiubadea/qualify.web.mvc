﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Qualify.Domain.HelperModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Qualify.Domain.Models.Embedded
{
    public class NewLoan : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();
        public string UniqueID { get; set; }
        [Required]
        public string OriginatorReferenceID { get; set; }
        [Required]
        public string LoanType { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string ProductCode { get; set; }
        [Required]
        public float ProposedAnnualInterestRate { get; set; }
        [Required]
        public float AmountRequested { get; set; }
        [Required]
        public string TotalTermType { get; set; }
        [Required]
        public int TotalTermDuration { get; set; }
        [Required]
        public string TotalTermUnits { get; set; }
        [Required]
        public string PaymentType { get; set; }
        [Required]
        public int PaymentTypeDuration { get; set; }
        [Required]
        public string PaymentTypeUnits { get; set; }
        [Required]
        public string InterestType { get; set; }
        [Required]
        public int InterestTypeDuration { get; set; }
        [Required]
        public string InterestTypeUnits { get; set; }
        [Required]
        public float RepaymentAmount { get; set; }
        [Required]
        public string RepaymentFrequency { get; set; }
        public string DiscountType { get; set; }
        public string DiscountRate { get; set; }
        public string PackageName { get; set; }
        public IEnumerable<Owner> Owner { get; set; }
    }
}
