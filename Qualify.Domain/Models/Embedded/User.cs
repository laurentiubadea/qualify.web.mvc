﻿
using Qualify.Domain.HelperModels;
using Qualify.Domain.HelperModels.AddressTypes;
using System.ComponentModel.DataAnnotations;

namespace Qualify.Domain.Models.Embedded
{
    public class User
    {
        [Required]
        public string LoanWriterFirstName { get; set; }
        [Required]
        public string LoanWriterSurname { get; set; }
        [Required]
        public string LoanWriterEmail { get; set; }
        [Required]
        public string LoanWriterAccreditationNumber { get; set; }
        [Required]
        public MobilePhone LoanWriterMobile { get; set; }
        public Address Address { get; set; }
        public string BDMFirstName { get; set; }
        public string BDMSurname { get; set; }
    }
}
