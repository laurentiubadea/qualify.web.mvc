﻿using System;
using System.Collections.Generic;
using Qualify.Domain.HelperModels;
using Qualify.Domain.HelperModels.IncomeTypes;

namespace Qualify.Domain.Models.Embedded
{
    public class Income : IMongoPropertiesInterface
    {
        public string UniqueID { get; set; }
        public string IncomeType { get; set; }
        public PAYG PAYG { get; set; }
        public SelfEmployed SelfEmployed { get; set; }
        public NotEmployed NotEmployed { get; set; }
        public OtherIncome OtherIncome { get; set; }
        public RentalIncome RentalIncome { get; set; }
        public IEnumerable<Owner> Owner { get; set; }

    }
}
