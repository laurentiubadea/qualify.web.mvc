﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Qualify.Domain.Models.Embedded
{
    public class Expense : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();
        public string UniqueID { get; set; }
        [Required]
        public string x_Household { get; set; }
        public float ChildcareAmount { get; set; }
        public string ChildcareFrequency { get; set; }
        public float ClothingAmount { get; set; }
        public string ClothingFrequency { get; set; }
        public float EducationAmount { get; set; }
        public string EducationFrequency { get; set; }
        public float GroceriesAmount { get; set; }
        public string GroceriesFrequency { get; set; }
        public float InsuranceAmount { get; set; }
        public string InsuranceFrequency { get; set; }
        public float InvestmentPropertyUtilitiesAndRatesAmount { get; set; }
        public string InvestmentPropertyUtilitiesAndRatesFrequency { get; set; }
        public float MedicalAndHealthAmount { get; set; }
        public string MedicalAndHealthFrequency { get; set; }
        public float OtherAmount { get; set; }
        public string OtherFrequency { get; set; }
        public float OwnerOccupiedPropertyUtilitiesAndRatesAmount { get; set; }
        public string OwnerOccupiedPropertyUtilitiesAndRatesFrequency { get; set; }
        public float RecreationAndEntertainmentAmount { get; set; }
        public string RecreationAndEntertainmentFrequency { get; set; }
        public float TelephoneInternetPayTVMediaStramingAmount { get; set; }
        public string TelephoneInternetPayTVMediaStramingFrequency { get; set; }
        public float TransportAmount { get; set; }
        public string TransportFrequency { get; set; }
        public float BoardAmount { get; set; }
        public string BoardFrequency { get; set; }
        public float ChildMaintenanceAmount { get; set; }
        public string ChildMaintenanceFrequency { get; set; }
        public float RentAmount { get; set; }
        public string RentFrequency { get; set; }
    }
}
