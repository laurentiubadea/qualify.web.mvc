﻿using MongoDB.Bson;
using Qualify.Domain.HelperModels;
using Qualify.Domain.HelperModels.AddressTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Qualify.Domain.Models.Embedded
{
    public class PropertyAsset : IMongoPropertiesInterface
    {
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();
        public string UniqueID { get; set; }
        [Required]
        public string Transaction { get; set; }
        [Required]
        public string PrimaryUsage { get; set; }
        [Required]
        public string PrimaryPurpose { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string EstimatedValue { get; set; }
        [Required]
        public string ApprovalInPrinciple { get; set; }

        public Address Address { get; set; }
        public IEnumerable<Owner> Owner { get; set; }
    }
}
