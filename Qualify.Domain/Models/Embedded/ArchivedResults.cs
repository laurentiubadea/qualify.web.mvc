﻿using Qualify.Domain.Models.Temporary;
using System.Collections.Generic;

namespace Qualify.Domain.Models.Embedded
{
    public class ArchivedResults : IMongoPropertiesInterface
    {
        public IEnumerable<Pricing> Pricing { get; set; }
        public IEnumerable<Umi> Umi { get; set; }
        public string UniqueID { get; set; }
    }
}
