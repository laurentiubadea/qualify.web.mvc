﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Qualify.Domain.HelperModels;
using System.Collections.Generic;

namespace Qualify.Domain.Models.Embedded
{
    public class Liability : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();
        public string UniqueID { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string FinancialInstitution { get; set; }
        public string OtherFIName { get; set; }
        public string AccountNumber { get; set; }
        public float OutstandingBalance { get; set; }
        public float CreditLimit { get; set; }
        public string OriginalLoanPurpose { get; set; }
        public float AnnualInterestRate { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public IEnumerable<Security> Security { get; set; }
        public float RepaymentAmount { get; set; }
        public string RepaymentFrequency { get; set; }
        public string InterestType { get; set; }
        public float InterestTypeDuration { get; set; }
        public string InterestUnits { get; set; }
        public string PaymentType { get; set; }
        public float PaymentTypeDuration { get; set; }
        public string PaymentUnits { get; set; }
        public string InterestOnlyEndDate { get; set; }
        public float TotalTermDuration { get; set; }
        public string TotalTermUnits { get; set; }
        public float RemainingTermDuration { get; set; }
        public string RemainingTermUnits { get; set; }
        public IEnumerable<Owner> Owner { get; set; }
    }
}
