﻿using MongoDB.Bson;
using Qualify.Domain.HelperModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Qualify.Domain.Models.Embedded
{
    public class Household : IMongoPropertiesInterface
    {
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        public string UniqueID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public int NumberOfAdults { get; set; }
        [Required]
        public int NumberOfDependants { get; set; }
        [Required]
        public IEnumerable<Dependant> Dependant { get; set; }
    }
}
