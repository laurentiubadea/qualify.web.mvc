﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Qualify.Domain.HelperModels;
using Qualify.Domain.HelperModels.AddressTypes;
using System.ComponentModel.DataAnnotations;

namespace Qualify.Domain.Models.Embedded
{
    public class Person : IMongoPropertiesInterface
    {
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();
        public string UniqueID { get; set; }
        [Required]
        public string ApplicantType { get; set; }
        [Required]
        public string PrimaryApplicant { get; set; }
        [Required]
        public string NameTitle { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string Surname { get; set; }
        public string ResidencyStatus { get; set; }
        public string MaritalStatus { get; set; }
        public Spouse Spouse { get; set; }
        public string IsLenderStaff { get; set; }
        public string IsExistingCustomer { get; set; }
        [Required]
        public string ExistingCustomerFinancialInstitution { get; set; }
        [Required]
        public string ExistingCustomerOtherFIName { get; set; }
        [Required]
        public string PostSettlementHousingStatus { get; set; }
        [Required]
        public string ExistingCustomerAccountNumber { get; set; }
        [Required]
        public Address PostSettlementAddress { get; set; }
        [Required]
        public float YearsInCurrentProffesion { get; set; }
        [Required]
        public float MonthsInCurrentProffesion { get; set; }
    }
}
