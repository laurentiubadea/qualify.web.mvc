﻿using Qualify.Domain.HelperModels;
using Qualify.Domain.HelperModels.AddressTypes;
using System.Text;

namespace Qualify.Domain.Models.Identity
{
    public class LoanWriter : IMongoPropertiesInterface
    {
        public string AggregatorAccreditationNumber { get; set; }
        public string AO { get; set; }
        public string BDMFirstName { get; set; }
        public string BDMSurname { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public string LoanWriterAccreditationNumber { get; set; }
        public Address LoanWriterAddress { get; set; }
        public string LoanWriterEmail { get; set; }
        public string LoanWriterFirstName { get; set; }
        public MobilePhone LoanWriterMobile { get; set; }
        public string LoanWriterSurname { get; set; }
        public string ReturnUrl { get; set; }
        public string Role { get; set; }
        public string SAO { get; set; }
        public string Surname { get; set; }
        public string UniqueID { get; set; }
        public string OneTimeUseToken { get; set; }
        public string ProfileImageUrl { get; set; }
        public string FullName
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder();
                if (!string.IsNullOrEmpty(LoanWriterFirstName))
                {
                    stringBuilder.Append($"{LoanWriterFirstName} ");
                }
                if (!string.IsNullOrEmpty(LoanWriterSurname))
                {
                    stringBuilder.Append($"{LoanWriterSurname}");
                }
                return stringBuilder.ToString();
            }
        }
        public string BdmFullName
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder();
                if (!string.IsNullOrEmpty(BDMFirstName))
                {
                    stringBuilder.Append($"{BDMFirstName} ");
                }
                if (!string.IsNullOrEmpty(BDMSurname))
                {
                    stringBuilder.Append($"{BDMSurname}");
                }
                return stringBuilder.ToString();
            }
        }

    }
}