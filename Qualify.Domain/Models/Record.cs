﻿using Qualify.Domain.Models.Embedded;
using Qualify.Domain.Models.Temporary;
using System;
using System.Collections.Generic;

namespace Qualify.Domain.Models
{
    public class Record : IMongoPropertiesInterface
    {
        public bool Archived { get; set; }
        public ArchivedResults ArchivedResults { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByUniqueID { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public IEnumerable<dynamic> Expenses { get; set; }
        public IEnumerable<dynamic> Household { get; set; }
        public string Id { get; set; }
        public IEnumerable<dynamic> Income { get; set; }
        public IEnumerable<dynamic> Liability { get; set; }
        public IEnumerable<dynamic> NewLoan { get; set; }
        public IEnumerable<dynamic> Person { get; set; }
        public Pricing Pricing { get; set; }
        public IEnumerable<dynamic> PropertyAsset { get; set; }
        public IEnumerable<dynamic> PricingDetails { get; set; }
        public string RecordName { get; set; }
        public IEnumerable<string> Status { get; set; }
        public Umi Umi { get; set; }
        public Lmi Lmi { get; set; }
        public string UniqueID { get; set; }
        public string UpdatedBy { get; set; }
    }
}
