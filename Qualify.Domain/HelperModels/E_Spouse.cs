﻿

namespace Qualify.Domain.HelperModels
{
    public class E_Spouse : IMongoPropertiesInterface
    {
        
        public string NameTitle { get; set; }
        
        public string FirstName { get; set; }
        
        public string Surname { get; set; }
    }
}
