﻿namespace Qualify.Domain.HelperModels
{
    public class Owner : IMongoPropertiesInterface
    {
        public string x_Party { get; set; }
        public float Percent { get; set; }
    }
}
