﻿using System.Collections.Generic;

namespace Qualify.Domain.HelperModels
{
    public class UserSession : IMongoPropertiesInterface
    {
        public string Id { get; set; }
        
        public List<SessionData> SessionData { get; set; }
        
        public string UserId { get; set; }
        public string Email { get; set; }
    }
}
