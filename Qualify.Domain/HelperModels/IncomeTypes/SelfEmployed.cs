﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Qualify.Domain.HelperModels.IncomeTypes
{
    public class SelfEmployed : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        public string Status { get; set; }
        
        public string Basis { get; set; }
        
        public string IndustryCode { get; set; }
        
        public int DurationLength { get; set; }
        
        public string DurationUnits { get; set; }
        public string OccupationCode { get; set; }
        public string StartDate { get; set; }
        public Employer Employer { get; set; }
        public BusinessIncome BusinessIncomeYearToDate { get; set; }
        public BusinessIncome BusinessIncomeRecent { get; set; }
        public BusinessIncome BusinessIncomePrevious { get; set; }
        public BusinessIncome BusinessIncomePrior { get; set; }
        public Owner Owner { get; set; }
    }
}
