﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Qualify.Domain.HelperModels.IncomeTypes
{
    public class NotEmployed : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        public string Status { get; set; }
        
        public string StartDate { get; set; }
        public float GovernmentBenefitsAmount { get; set; }
        public string GovernmentBenefitsFrequency { get; set; }
        public float NewstartAllowanceAmount { get; set; }
        public string NewstartAllowanceFrequency { get; set; }
        public float PrivatePensionAmount { get; set; }
        public string PrivatePensionFrequency { get; set; }
        public float SuperannuationAmount { get; set; }
        public string SuperannuationFrequency { get; set; }
        public float OtherIncomeAmount { get; set; }
        public string OtherIncomeFrequency { get; set; }
        public Owner Owner { get; set; }
    }
}
