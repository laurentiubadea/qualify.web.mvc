﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Qualify.Domain.HelperModels.IncomeTypes
{
    public class PAYG : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        public string Status { get; set; }
        
        public string Basis { get; set; }
        
        public string OccupationCode { get; set; }
        
        public string StartDate { get; set; }
        public string OnProbation { get; set; }
        public string CompanyCar { get; set; }
        
        public string IndustryCode { get; set; }
        public int DurationLength { get; set; }
        public string DurationUnits { get; set; }
        
        public float GrossSalaryAmount { get; set; }
        
        public string GrossSalaryFrequency { get; set; }
        
        public float NetSalaryAmount { get; set; }
        
        public string NetSalaryFrequency { get; set; }
        public float GrossRegularOvertimeAmount { get; set; }
        public string GrossRegularOvertimeFrequency { get; set; }
        public float BonusAmount { get; set; }
        public string BonusFrequency { get; set; }
        public float CommissionAmount { get; set; }
        public string CommissionFrequency { get; set; }
        public float CarAllowanceAmount { get; set; }
        public string CarAllowanceFrequency { get; set; }
        public float WorkAllowanceAmount { get; set; }
        public string WorkAllowanceFrequency { get; set; }
        public float WorkersCompensationAmount { get; set; }
        public string WorkersCompensationFrequency { get; set; }
        public bool FullyMaintainedCompanyCar { get; set; }
        public Owner Owner { get; set; }
    }
}
