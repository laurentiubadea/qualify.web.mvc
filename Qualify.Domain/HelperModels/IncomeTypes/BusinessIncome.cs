﻿namespace Qualify.Domain.HelperModels.IncomeTypes
{
    public class BusinessIncome : IMongoPropertiesInterface
    {
        
        public string StartDate { get; set; }
        
        public string EndDate { get; set; }
        
        public float ProfitBeforeTax { get; set; }
        
        public float Deprecation { get; set; }
        public string Interest { get; set; }
        public float Lease { get; set; }
        public float NonCashBenefits { get; set; }
        public float NonRecurringExpenses { get; set; }
        public float SuperannuationExcess { get; set; }
        public float CarryForwardLosses { get; set; }
        public float AmortisationOfGoodwill { get; set; }
        public float Salary { get; set; }
        public float Allowances { get; set; }
        public float Bonus { get; set; }
        public float CarExpense { get; set; }
    }
}
