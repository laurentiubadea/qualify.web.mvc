﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Qualify.Domain.HelperModels.IncomeTypes
{
    public class OtherIncome : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        public string Type { get; set; }
        public string Description { get; set; }
        public string GovernmentBenefitsType { get; set; }
        public string BenefitsDescription { get; set; }
        public float TotalAssetValue { get; set; }
        public float Amount { get; set; }
        public string Frequency { get; set; }
        public string IsTaxable { get; set; }
        public string StartDate { get; set; }
        public Owner Owner { get; set; }
    }
}
