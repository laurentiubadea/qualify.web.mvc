﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Qualify.Domain.HelperModels.AddressTypes;

namespace Qualify.Domain.HelperModels.IncomeTypes
{
    public class RentalIncome : IMongoPropertiesInterface
    {
        [BsonId]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        public string Transaction { get; set; }
        
        public string PrimaryUsage { get; set; }
        
        public string PrimaryPurpose { get; set; }
        public string Status { get; set; }
        public string ApprovalInPrinciple { get; set; }
        public float RentalAmount { get; set; }
        public string RentalFrequency { get; set; }
        public Address Address { get; set; }
        public Owner Owner { get; set; }
    }
}
