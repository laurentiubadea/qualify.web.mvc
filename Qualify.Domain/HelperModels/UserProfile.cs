﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qualify.Domain.HelperModels
{
    class UserProfile
    {
        public string Id { get; set; }
        public string UserId { get; set; }
    }
}
