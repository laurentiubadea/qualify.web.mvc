﻿namespace Qualify.Domain.HelperModels
{
    public class Dependant : IMongoPropertiesInterface
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string DateOfBirth { get; set; }
    }
}
