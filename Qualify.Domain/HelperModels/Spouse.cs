﻿namespace Qualify.Domain.HelperModels
{
    public class Spouse : IMongoPropertiesInterface
    {
        public string x_Spouse { get; set; }
        public E_Spouse e_Spouse { get; set; }
    }
}
