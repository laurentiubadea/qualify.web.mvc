﻿namespace Qualify.Domain.HelperModels.AddressTypes
{
    public class NonStandardAddress : IMongoPropertiesInterface
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
    }
}
