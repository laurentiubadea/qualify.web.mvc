﻿namespace Qualify.Domain.HelperModels.AddressTypes
{
    public class Address : IMongoPropertiesInterface
    {
        public string AustralianPostCode { get; set; }
        public string AustralianState { get; set; }
        public string Country { get; set; }
        public NonStandardAddress NonStandard { get; set; }
        public StandardAddress Standard { get; set; }
        public string Suburb { get; set; }
        public string Type { get; set; }
    }
}
