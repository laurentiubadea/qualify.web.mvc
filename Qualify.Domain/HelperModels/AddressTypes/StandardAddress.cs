﻿namespace Qualify.Domain.HelperModels.AddressTypes
{
    public class StandardAddress : IMongoPropertiesInterface
    {

        public string BuildingName { get; set; }
        public string Level { get; set; }
        public string LevelType { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string StreetType { get; set; }
        public string Unit { get; set; }
        public string UnitType { get; set; }
    }
}
