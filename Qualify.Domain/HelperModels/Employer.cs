﻿

namespace Qualify.Domain.HelperModels
{
    public class Employer
    {
        
        public string CompanyName { get; set; }
        
        public string BusinessStructure { get; set; }
        public Owner Partner { get; set; }
    }
}
