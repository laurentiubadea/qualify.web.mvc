﻿using System;

namespace Qualify.Domain.HelperModels
{
    public class SessionData : IMongoPropertiesInterface
    {
        public string SessionId { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
